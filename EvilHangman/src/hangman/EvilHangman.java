package hangman;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class EvilHangman implements EvilHangmanGame
{

	private int length;
	private int guesses;
	private ArrayList<String> lettersUsed;
	private StringBuilder displayedWord;
	private Scanner input = new Scanner(System.in);
	private Set<String> validWords;
	private Map<String, String> used_letters;
	private boolean playingGame = false;

	@Override
	public void startGame(File dictionary, int wordLength)
	{
		length = wordLength;
		lettersUsed = new ArrayList<>();
		displayedWord = new StringBuilder();
		createValidWordSet(dictionary);
		used_letters = new HashMap<String, String>();
		for (int i = 0; i < wordLength; i++)
		{
			displayedWord.append("-");
		}
	}

	private void createValidWordSet(File dictionary)
	{
		try
		{
			validWords = new TreeSet<String>();
			Scanner read = new Scanner(dictionary);
			while (read.hasNextLine())
			{
				String word = read.nextLine();
				if (word.length() == length)
				{
					validWords.add(word);
				}
			}
			read.close();

		}
		catch (FileNotFoundException e)
		{

			e.printStackTrace();
		}
	}

	public void run()
	{
		playingGame = true;
		while (isGameOver() == false)
		{
			System.out.println("You have " + guesses + " guesses left");
			lettersUsed();
			System.out.println("Word: " + displayedWord.toString());
			guessInput();

		}
		input.close();
	}

	private boolean isGameOver()
	{
		if (guesses == 0)
		{
			String word = "";
			for (String i : validWords)
			{
				word = i;
				break;
			}
			System.out.println("You Lose!");
			System.out.println("The Word was: " + word);

			return true;
		}
		else if (displayedWord.toString().indexOf('-') == -1)
		{
			System.out.println("You Win!");
			System.out.println(displayedWord.toString());
			return true;
		}
		System.out.println();
		return false;
	}

	private void guessInput()
	{
		System.out.print("Enter guess: ");
		String test = input.next();
		char guess = test.charAt(0);
		guess = Character.toLowerCase(guess);
		if (Character.isAlphabetic(guess) == false || test.length() > 1)
		{
			System.out.println("Invalid input");
			guessInput();
		}
		else if (used_letters.get(Character.toString(guess)) != null)
		{
			System.out.println("You already used that letter");
			guessInput();
		}
		else
		{
			try
			{
				makeGuess(guess);
			}
			catch (GuessAlreadyMadeException e)
			{
				guessInput();
			}
		}
	}

	public int getGuesses()
	{
		return guesses;
	}

	public void setGuesses(int guesses)
	{
		this.guesses = guesses;
	}

	private void lettersUsed()
	{
		System.out.print("Used letters: ");
		for (String i : lettersUsed)
		{
			System.out.print(i + " ");
		}
		System.out.println();
	}

	@Override
	public Set<String> makeGuess(char guess) throws GuessAlreadyMadeException
	{
		//check exception
		if (used_letters.get(Character.toString(guess)) != null)
		{
			throw new GuessAlreadyMadeException();
		}
		used_letters.put(Character.toString(guess), Character.toString(guess));
		lettersUsed.add(Character.toString(guess));

		//partition that stores groups of words
		Map<String, ArrayList<String>> partition = new HashMap<String, ArrayList<String>>();

		// go over each word that is currently valid and create a key composed of indexes of where char matches guess
		// and add group of words to map under the key made
		for (String i : validWords)
		{
			StringBuilder key = new StringBuilder();
			//default key value for all words that do not have letter
			key.append("@,");

			for (int a = 0; a < i.length(); a++)
			{
				if (i.charAt(a) == guess)
				{
					key.append(a + ",");
				}
			}

			ArrayList<String> replace = new ArrayList<String>();

			// get arraylist already in the map for this key (if there is any) and copy into the replace arraylist
			if (partition.get(key.toString()) != null)
			{
				replace.addAll(partition.get(key.toString()));

				// remove arraylist associated to key from map
				partition.remove(key.toString());
			}
			// add current word to arrayist  
			replace.add(i);

			//reinsert arraylist with the new word into the map under same key
			partition.put(key.toString(), replace);
		}

		//return the arraylist with the greatest number of words
		int size = 0;
		String key = null;
		for (Map.Entry<String, ArrayList<String>> i : partition.entrySet())
		{
			int listSize = i.getValue().size();
			//if list is of equal size get the one with none of the letter chosen or fewer of them
			if (listSize == size)
			{
				//add the current key if it the guessed letter is not found in words
				if (i.getKey().equals("@,"))
				{
					size = listSize;
					key = i.getKey();
				}
				//if number of commas (number of indexes matched) in map key is smaller then make it current key and size
				else if (countCommas(i.getKey()) < countCommas(key))
				{
					size = listSize;
					key = i.getKey();
				}
			}
			//if current list is greater than one stored in size, replace size and key with the current list's
			else if (listSize > size)
			{
				size = listSize;
				key = i.getKey();
			}
		}
		if (playingGame)
		{
			//parse key and add guessed char to current word displayed
			String[] numbers = key.split(",");
			//start at 1 because @ is the default for words with no match
			for (int i = 1; i < numbers.length; i++)
			{
				int index = Integer.parseInt(numbers[i]);
				displayedWord.replace(index, index + 1, Character.toString(guess));
			}
			if (numbers.length > 1)
			{
				if (numbers.length < 3)
					System.out.println("Yes, there is " + (numbers.length - 1) + " " + guess);
				else
					System.out.println("Yes, there are " + (numbers.length - 1) + " " + guess + "'s");
			}
			else
			{
				System.out.println("Sorry, there are no " + guess + "'s");
				guesses--;
			}
		}
		//make the set of available words the words of the arraylist (largest list) associated to "key"
		validWords = new TreeSet<>();
		validWords.addAll(partition.get(key));
		//System.out.println(validWords.toString());

		return validWords;
	}

	private int countCommas(String input)
	{
		int result = 0;
		for (int i = 0; i < input.length(); i++)
		{
			if (input.charAt(i) == ',')
				result++;
		}
		return result;
	}
}
