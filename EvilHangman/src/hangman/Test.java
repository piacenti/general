package hangman;

import java.io.File;

public class Test
{
	public static void main(String[] args)
	{
		File dictionaryPath = new File(args[0]);
		int wordLength = Integer.parseInt(args[1]);
		int guesses = Integer.parseInt(args[2]);
		
		EvilHangman newGame = new EvilHangman();
		newGame.setGuesses(guesses);
		newGame.startGame(dictionaryPath, wordLength);
		newGame.run();
			
		 
	}
	
}
