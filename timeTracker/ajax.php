<?php
require('../includes/includeMeBlank.php');
//The code below is activated when you submit an entry using the Clock IN and Clock OUT buttons.
if(isset($_GET['inOutSubmit']) && isset($_GET['entry']))
{
	$inOut=$_GET['inOutSubmit'];
	$groupSelection=$_GET['groupSelection'];
	$groupSelectionId=$_GET['groupSelectionId'];
	$noSubmit=$_GET['noSubmit'];
	$projectEntry=$_GET['entry'];
	$employeeName=$_GET['employeeName'];
	if($noSubmit!="true")
	{
	$query="INSERT INTO timeTrack (`startTime`, `time`, `inOut`, `project`, `employee`, `entryForTeam`, `teamId`) VALUES (CURDATE(), CURTIME(), '".$inOut."', '".addslashes($projectEntry)."', '".$employeeName."', '".addslashes($groupSelection)."', '".addslashes($groupSelectionId)."')";
	$submitQuery=mysql_query($query);
	}
	else
	{
		echo "alert";
	}
}
if(isset($_GET['selectEmployee']))
{
	$employeeId=$_GET['employeeId'];
	$groupSelection=(int) $_GET['groupSelection'];
	$queryTeam= mysql_query("SELECT * FROM `teamMembers` WHERE teamID = ".$groupSelection." ORDER BY netID ASC");

	$alreadyAddedEmployees=array();
	echo "<option value=''>Any</option>";
		while($cur = mysql_fetch_array($queryTeam))
		{
			if(!in_array(nameByNetId($cur['netID']), $alreadyAddedEmployees))
			{
			echo "<option value='".nameByNetId($cur['netID'])."'>".nameByNetId($cur['netID'])."</option>";
			$alreadyAddedEmployees[]=nameByNetId($cur['netID']);
			}
			
		}


}
//The code below is activated when you submit an edition which sends a json to this page. $numEdits contains the number of rows you chose to edit.
if(isset($_GET['json']))
{
	$numEdits=$_GET['numEdits'];
	$json=$_GET['json'];
	$editValues=json_decode($json);
	$employee=$_GET['employee'];
	$date= date("Y-m-d h:i:s A");
	$rowNum=0;

//This while is used in combination with the foreach to test and update all the information that was submitted. The while is used to specify the row information you are trying to get with foreach. Foreach runs through the specified row's information. $field takes the following values in order: id, startTime, time, inOut and project. $edit contains the actual information that were in the input boxes when the Submit Changes button was pressed and they are run through in the same order as $field. So when $field=id $edit has the id number, when $field=project, $edit has the project info that was in the imput box. Also, unless you use the while loop, the variable $field would contain just numbers instead of the actual field names listed above and this would not work with the foreach loop below. 
while($rowNum<$numEdits)
	{
		foreach ($editValues[$rowNum] as $field=>$edit)
		{

		$verifyEdit="SELECT * FROM `timeTrack` WHERE `id`='".$editValues[$rowNum]->id."' ";
		$query=mysql_query($verifyEdit);
		$dbEntry=mysql_fetch_array($query);
		
		$oldModifiedField=$dbEntry['modify'];
		$newModifiedField=$dbEntry['modify'];
//If the information in the $edit differs from the information found in its respective field in the database, the code below runs updating the database on whatever was changed. 
		if ($dbEntry[$field] != $edit)
			{
//The if statements below are just used to change the text format of $field so that it may be used below in the text that contains detail about the modification. This text is what submited to the modify column of the database. This is also used to avoid writing longer if statements for each of the values that $field takes. $addComa is used to add comas between the changes when more than one field was changed. $addSlash is used to add the pipe "|" which is used to determine different editions so that they are separated when displayed. 
				if(!isset($addComa)){$addComa="";}
				if($field=='project'){$textUpdate="Project";}
				if($field=='time'){$textUpdate="Time";}
				if($field=='starTime'){$textUpdate="Date";}
				if($field=='employee'){$textUpdate="Employee";}
				if($field=='inOut'){$textUpdate="IN/OUT";}
//If it is the first time that the row is editted, the code in this if statement runs, and $addSlashes=| to keep if($addSlash!="|") from running. When you modify a row for the first time on multiple elements the first element it run it will run the if statement below because it will add the first bit of information but when it runs the next element it will see there is alaready something in the database so it will run the code under if($oldModifiedField!="") and so if there is a | ti will just keep running normally as it did in the beginning. However, if you are editing a row that has been edited before it will skip  if($oldModifiedField=="") and it will go to if($oldModifiedField!="") where if($addSlash!="|") will make sure to add | in order to separate the entries. |is also used a little ahead on the code to divide the entries in a specific way. 
				if($oldModifiedField=="")
				{
				$newModifiedField.=$addComa." ".$textUpdate." modified from \"".$dbEntry[$field]."\" to \"".$edit."\"";
				$addSlash="@|@";
				}
				if($oldModifiedField!="")
				{
				if($addSlash!="@|@")
				{
				$addSlash="@|@";
				$newModifiedField.=$addSlash." ".$textUpdate." modified from \"".$dbEntry[$field]."\" to \"".$edit."\"";
				}
				else
				{
				$newModifiedField.=$addComa." ".$textUpdate." modified from \"".$dbEntry[$field]."\" to \"".$edit."\"";
				}
				}
// after each change was made $addComa="," in case there are more. So if this part of the code runs again comas are added before the entry. 
				$addComa=",";
			}
// if there have been differences between the information contained in the old modify cell (database) and the new modify info (coming to replace the one on the database). The update below runs. 
		if ($oldModifiedField != $newModifiedField)
		{
			$modified=true;
			$updateColumn="UPDATE `timeTrack` SET `".$field."`='".addslashes($edit)."', `modify`='".addslashes($newModifiedField)."' WHERE `id`='".$editValues[$rowNum]->id."' ";
			mysql_query($updateColumn);
		}
		}
//if there were changes made, the if statement below runs after the foreach is done with the row and before the while loop starts the new row (in case of multiple row edition). It submits the string $newModifiedField which contains the details of all the fields changed and at the very end it adds the date and the name of the employee that made the changes to the row.
		if ($modified==true)
		{
			$newModifiedField.=" by ".$employee." on ".date('Y-m-d h:i:s A')."<br>";
			$updateColumn="UPDATE `timeTrack` SET  `modify`='".addslashes($newModifiedField)."' WHERE `id`='".$editValues[$rowNum]->id."' ";
			mysql_query($updateColumn);
		}
//since foreach is done with the row the variables $addSlash and $addComa are reset to default values and the row number, $rowNum, increase by one. 
$addSlash="";
$rowNum++;
$addComa="";
}

}
//The code below runs when you first get into the page, whenever you click on the Run Search button and when you submit editions to rows. It gets all the information it uses from the field in the search table. The $searchArra array is used to make it clear these are all based on the values from the search table. 
if(isset($_GET['printLog']))
{
	$searchArray=array('startDate'=>$_GET['startDate'], 'endDate'=>$_GET['endDate'],'descriptionSearch'=>$_GET['descriptionSearch'], 'mic'=>$_GET['mic'], 'employeeName'=>$_GET['employeeName'], 'inOutSearch'=>$_GET['inOutSearch'], 'employeeModify'=>$_GET['employeeIdModify'], 'groupSelection'=>$_GET['groupSelection'], 'groupSelectionId'=>$_GET['groupSelectionId']);
	if($searchArray['mic']!=="")
	{
		$query="SELECT * FROM timeTrack WHERE (`project` LIKE '%".$searchArray['mic']."%' OR  `startTime` LIKE '%".$searchArray['mic']."%'  OR `inOut` LIKE '%".$searchArray['mic']."%' OR  `employee` LIKE '%".$searchArray['mic']."%' OR `modify` LIKE '%".$searchArray['mic']."%') AND `entryForTeam`= '".addslashes($searchArray['groupSelection'])."' ORDER BY `startTime` DESC, `time` DESC";
	}
		else
	{
		$query="SELECT * FROM timeTrack WHERE `startTime` BETWEEN '".$searchArray['startDate']."' AND '".$searchArray['endDate']."'  AND `inOut` LIKE '%".$searchArray['inOutSearch']."%' AND `project` LIKE '%".$searchArray['descriptionSearch']."%'  AND `employee` LIKE '%".$searchArray['employeeName']."%'  AND  `modify` LIKE '%".$searchArray['employeeModify']."%' AND `entryForTeam`= '".addslashes($searchArray['groupSelection'])."' ORDER BY `startTime` DESC, `time` DESC";
	}

	$infoPull=mysql_query($query);
//The table below is what gets printed in the "log" div in index.php
	echo "<table id='logInfo' class='centered display' ><thead><tr><th>Time</th><th>IN/OUT</th><th>Working/Worked On</th><th>Employee</th><th>Modified</th><th>Edit</th></tr></thead><tbody>";
	while($row=mysql_fetch_array($infoPull))
	{
//The code below grabs "|" and uses it to separate the string into an array with the explode command. That is why "|" was not added at the end of the row string $newModifiedField used previously. $n is used to count the number of variables in the array which is the number of times that the row was modified (not the number of elements in the row but the number of times that any change was made to any part of the row) . The $remainingEntries contains all the entries except the last one which is the first one that is diplayed on the page. This is used because the JQuery slidetoggle option requires a separate div for the remaining information. The $remainingEntries is reversed so that what the last result is the first one shown. The implode command turns the remaining values back into a string.
		$lastModifyEntry=explode("@|@",$row['modify']);
		$n=count($lastModifyEntry);
		$remainingEntries=$lastModifyEntry;
		unset($remainingEntries[$n-1]);
		$remainingEntries=array_reverse($remainingEntries);
// The '&' before '$eachElement' is just used to allow you to modify the values of $remainingEntries outside the loop (real values of the vaiable) rather than just inside the loop. '&' before a variabl ein php acts as a reference that references the latest value of a variable. Example: if you first define $number=1, second define $new=&$number, third redefine $number=2, and lastly echo $new, you will echo '2' which is the last definition of $number. So in other words the & below is changing the values of the globa variable, if you wish.
		foreach($remainingEntries as &$eachElement){$eachElement='&#149;'.$eachElement;}
		$remainingEntries=implode(" ",$remainingEntries);
		echo "<tr'><td id='".$row['id']."startTime'>".$row['startTime']."  ";
		echo $row['time']."</td>";
		echo "<td  id='".$row['id']."inOut'>".$row['inOut']."</td>";
		echo "<td  id='".$row['id']."project'>".$row['project']."  ";
		echo "<td id='".$row['id']."employee'>".$row['employee']."</td>";
//This row contains the log of modifications for the row. On click it should toggle between $lastModifyEntry[$n-1], which only shows the very last result, and the remainig results in $remainingEntries which are stored in the div subsequent to it. The second div takes care of showing how many results there are besides the initial one which is shown when the printLog() function is executed. The code (($n>1)?($n-1).' more modifications':"") is a short if statment that, when there are more than one result, shows the remaining number of results not displayed by default and the string that follows it when there are more than one result. 
		echo "<td   id='".$row['id']."modify' OnClick='$(\"#".$row['id']."modify2\").slideToggle();$(\"#".$row['id']."modify3\").toggle();$(\"#".$row['id']."clickBack\").slideToggle()'>".$lastModifyEntry[$n-1]."<div class='modifyField' id='".$row['id']."modify2'>".$remainingEntries."</div><div class='centered clickable' id='".$row['id']."modify3' style='color:#000066;'>".(($n>1)?($n-1).' more modifications':"")."</div><div class='centered clickable' id='".$row['id']."clickBack' style='color:#000066;display:none;'>".(($n>1)?'Click to slide back':"")."</div></td>";
//The if statement below takes care of only showing the edit button to the employee responsible for the row or the team lead who has rights to edit any other employee's information. The values passed by the OnClick event are the variables generated by the query used in this part of the code. $row['project'] is not used due to problems with single and double quotes. JQuery is used to get the project information from the row itself by means of the row id which is generated above. 
		if( $_GET['curEmployee']==$row['employee'] OR checkPermission('leadTimeTracker'))
			{
			if(checkPermission('leadTimeTracker')==true){$teamLeadPermission=1;}
			else{$teamLeadPermission=0;}
		echo "<td id='".$row['id']."edit'><input type='button' id='".$row['id']."edit2' class='buttonTracker'   Value='Edit Row' OnClick='editLog(".$row['id'].",\"".$row['startTime']."\",\"".$row['time']."\",\"".$row['inOut']."\",".$teamLeadPermission.")' /></td></tr><script>$('.buttonTracker').button();</script>";
			}
		else
			{
			echo "<td id='".$row['id']."edit'></td></tr>";
			}
			
	}
echo "</tbody></table>";
}

?>
