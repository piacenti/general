//call printLog function when the page is first loaded in order to display the log table.
window.onload = function()
{
	$('.buttonTracker').button();
	printLog();

	$('.datepicker').datepicker({dateFormat:"yy-mm-dd", showOn: 'both', buttonImage: '../includes/libs/img/cal.gif', buttonImageOnly:true});
	getEmployeesFromGroup();
	$('#groupSelected').tooltip();
	
}
var getEmployeesFromGroupCounter=0;
function getEmployeesFromGroup()
{
	
	if(getEmployeesFromGroupCounter>0)
	{
		$('option').removeAttr("title");
	}
	var employeeId=$('#employeeId').html();
	var groupSelection=$('#groupSelected').val();
	$.ajax({
	url:"ajax.php",
	type:'GET',
	data:{selectEmployee:'selectEmployee', employeeId:employeeId, groupSelection:groupSelection},
	success:function(result)
	{
			$('.selectEmployee').html(result);
			var counter=0;
			
			$('.selectEmployee option').each(function()
				{
					counter++;
				}
				);
			if(counter<3)
			{
				$('.selectEmployee option').text('No members');
			}
			printLog();
		}
	});
	getEmployeesFromGroupCounter++;
}

//this function takes care of both the search and the initially displayed information. It gets all the information it needs to run from the search table.  
function printLog()
{

	var mic=document.getElementById('mic').value;
	var inOutSearch=document.getElementById('inOutSearch').value;
	var startDate=document.getElementById('startDate').value;
	var endDate=document.getElementById('endDate').value;
	var descriptionSearch=document.getElementById('descriptionSearch').value;
	var employeeIdModify=document.getElementById('selectModify').value;
	var employeeName=document.getElementById('selectEmployee').value;
	var groupSelection=$('#groupSelected option:selected').html();
	var groupSelectionId=$('#groupSelected option:selected').val();
	$curEmployee=$('#employeeName').html();
	var printLog="printLog";
	$.ajax({
	url:"ajax.php",
	type:'GET',
	data:{startDate:startDate, endDate:endDate, descriptionSearch:descriptionSearch, mic:mic, employeeName:employeeName, employeeIdModify:employeeIdModify, inOutSearch:inOutSearch, printLog:printLog, curEmployee:$curEmployee, groupSelection:groupSelection, groupSelectionId:groupSelectionId},
	success:function(resultLog){$('#log').html(resultLog);
	$('#logInfo').dataTable( {
		"sDom": 'R<"H"lfr>t<"F"ip>',
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",
		"bFilter": false,
		"bDestroy": true,
		"bRetrieve": true
	} );
		$('#timeTrackSearchTable').dataTable( 
		{
		"bJQueryUI": true,
		 "bSort":false,
		 "bDestroy": true,
		"bRetrieve": true
		} 
		);
	}
	});



}
//this function is called when the buttons Clock IN and Clock OUT are clicked. The inOutSubmit parameter contains the information about which button was pressed (IN or OUT). At the end it calls printLog again to update the log information displayed. 
function submitAjax(inOutSubmit)
{
	
	var employeeName=document.getElementById('employeeName').innerHTML;
	var groupSelection=$('#groupSelected option:selected').html();
	var groupSelectionId=$('#groupSelected option:selected').val();
	var noSubmit=$('#groupSelected option:selected').attr('noSubmit');
	var entry=$.trim($('#projectEntry').val());
	$('#projectEntry').val("");
	$.ajax({
	url:"ajax.php",
	type:'GET',
	data:{inOutSubmit:inOutSubmit, entry:entry, employeeName:employeeName, groupSelection:groupSelection, groupSelectionId:groupSelectionId, noSubmit:noSubmit},
	success:function(result)
		{ 
			if(result=="alert"){ alert("You cannot submit entries for the current group selection");} 
			else{printLog();}; 
		}
	});
	
}
//The function below takes care of making specific fields edtiable in the row selected. It also makes the Submit Changes button show up and the edit button you pressed disappear. numEdits trakcs the number of rows you chose to edit and every time you click on Edit Row it adds one to the variable. items is an array that keeps track of the id of the database row that contains all the information that you see in the log table for that row. Every time you click on Edit Row it pushes the row id to the array. 
var items=new Array();
var numEdits=0;
function editLog(id, startTime, time, inOut, teamLeadPermission)
	{
		$employee=$('#'+id+'employee').html();
		$curEmployee=$('#employeeName').html();
		if($employee==$curEmployee || teamLeadPermission==1)
		{
		var submit=$('#editSubmitButton').html();
		if (submit=="" && $employee==$curEmployee || teamLeadPermission==1)
			{
				document.getElementById('editSubmitButton').innerHTML="<input type='button' class='buttonTracker' Value='Submit Changes' OnClick='submitEdit()' Title='Click here to submit or close editting'/>";
				$('.buttonTracker').button();
				$('#editSubmitButton').tooltip();
			}
		if(inOut=='IN'){var alternative="OUT"}
		if(inOut=='OUT'){var alternative="IN"}

		document.getElementById(id+'startTime').innerHTML="<input type='text' size=8 id='"+id+'startTimeInput'+"' value='"+startTime+"'/>";
		document.getElementById(id+'startTime').innerHTML+="<input type='text' size=6 id='"+id+'timeInput'+"' value='"+time+"'/>";
		document.getElementById(id+'inOut').innerHTML="<select id='"+id+'inOutInput'+"' value='"+inOut+"'><option value='"+inOut+"'>"+inOut+"</option><option vlaue='"+alternative+"'>"+alternative+"</option></select>";
// The variable project is not passed through a parameter because double and single quotes were causing problems. The text() solves problems you would have with html() for this action.
		var project=$("#"+id+"project").text();
		document.getElementById(id+'project').innerHTML="<input type='text' id='"+id+"projectInput'>";
		$('#'+id+'projectInput').attr('value', project);
		document.getElementById(id+'edit').innerHTML="";
		items.push(id);
		window.numEdits++;
		}
	}
//This function submits the edtions made. Every change that was made is collected and put into the json string which is sent to ajax.php. At the end the items array is emptied and numEdits is set back to 0.
function submitEdit()
{
	var employeeName=document.getElementById('employeeName').innerHTML;
	var jsonString='[';
	var counter=0;
	$(items).each(function(items, id){
//The project is obtained through these two commands below because double and single quotes were causing problems when the information in the variable was being sent to ajax.php. Although (/\"/g, '\\"'); doesn't look right it works perfectly fine and even though the ; and , look like they appear as a string (color wise) they don't act like it in the program. (/\\/g,'\\\\') allows backslashes to be read on the databaase. The function(items, id) loops through items and get the element as id. Each id element is used to get all the information from a row and build each section of the json. &amp; is replaced because when this Jquery gets the values it makes a & into an &amp; before it sends the string to the database. 
	$project= $.trim($('#'+id+"projectInput").val());
	$project=$project.replace(/\\/g,'\\\\');	
	$project=$project.replace(/\"/g, '\\"');

	if (counter>0) jsonString+=',';
	jsonString+='{ "id" : "'+id+'", "startTime" : "'+$('#'+id+"startTimeInput").val()+'","time" : "'+$('#'+id+"timeInput").val()+'", "inOut" : "'+$('#'+id+"inOutInput").val()+'","project" : "'+$project+'"}';
	counter++;
	});
	jsonString+=']';
	$.ajax({
	url:"ajax.php",
	type:"GET",
	data:{json:jsonString, employee:employeeName, numEdits:numEdits},
	success:function(){printLog();}
	});
	document.getElementById('editSubmitButton').innerHTML="";
	window.items=[];
	window.numEdits=0;
}


