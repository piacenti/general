<?php 
//index.php
require ('../includes/includeme.php');

if(checkPermission('development') || checkPermission('problemManagement'))
{
	if(isset($_POST['employeeNetId']))
		{
			$employeeNetId = $_POST['employeeNetId'];
		} 
			else if( isset($_GET['employee']))
		{
			$employeeNetId = $_GET['employee'];
		}
	else
		{
		$employeeNetId = $netID;
		}
?>
<!-- javascript code specific for this app-->
<script type="text/javascript" src="javascript.js"></script>
<!--  javascript code for the calendar used in this code-->
<script type="text/javaScript" src="/includes/libs/tcal.js"></script>
<!--  css used for the calendar in this code-->
<link rel="stylesheet" type="text/css" href="/includes/libs/tcal.css" />
<!--  css specific for this app -->
<link rel="styleSheet" type="text/css" href="styleSheet.css" />

<br>
<div id='timeTrackTitle'  class="centered">Time and Work Tracker</div>
<br><br>


<!-- Text area where the imput entry is taken and submitted to the database when the buttons Clock In and Out are clicked. The butons themselves send the information about if this is a clock in or out-->
<textarea id="projectEntry" style="width:580px; height:100px;"></textarea>
<input class="buttonTracker" type="button"  Value="Clock IN" Onclick="submitAjax('IN');"/>
<input class="buttonTracker" type="button"  Value="Clock OUT" Onclick="submitAjax('OUT');"/> 

<!--This div is used by javascript to get the php variable containing employee name and send it to the database with other parameters-->
<div id="employeeName" style="visibility:hidden"><?php echo nameByNetId($employeeNetId);?></div>
<div id="employeeId" style="visibility:hidden"><?php echo $employeeNetId;?></div>
<span style="position:relative; left:600px; bottom:100px;"><b>Current Group</b><br>
<select id="groupSelected" onchange="getEmployeesFromGroup();" >
<?php
$query=mysql_query("SELECT * FROM `teamMembers` WHERE `netID` = '".$netID."' AND `area` = ".$area."");
$counter=0;
$addedTeams=array();
while($row=mysql_fetch_array($query))
{
	$queryTeam=mysql_query("SELECT * FROM `teams` WHERE `ID` = ".$row['teamID']." ");
	$teamInfo=mysql_fetch_array($queryTeam);
	if($counter==0){ echo "<option title='My group' selected = 'selected' value='".$teamInfo['ID']."'>".$teamInfo['name']."</option>";}
	else { echo "<option title='My group' value='".$teamInfo['ID']."'>".$teamInfo['name']."</option>";}
	$addedTeams[]=$teamInfo['ID'];
	$counter++;
	
}
$queryAllTeams=mysql_query("SELECT * FROM `teams` WHERE `area` = ".$area."");
while($row=mysql_fetch_array($queryAllTeams))
{
	if(in_array($row['ID'], $addedTeams)===false)
	echo "<option title='Not my group' style='background-color:rgba(255, 0, 0, 0.41);' value=".$row['ID']." noSubmit='true'>".$row['name']."</option>";
}

?>
</select>
</span>
<br>
<!--The Input below runs the search-->
<input class="buttonTracker" type="button" value="Run Search" OnClick="printLog()"/><br><br>

<!--This span is turned into the Submit Changes button when you click on Edit Row to make editions-->
<span id="editSubmitButton"class='alignRigth' style="margin-bottom:20px;"></span>

<!--The search table. Its default values are used to get the initial information from the database which is displayed on the next table-->
<table id="timeTrackSearchTable" class='centered display'><thead><tr><th style="border-top-left-radius:8px;">Search All</th><th>IN/OUT</th><th>Start Date</th><th>End Date</th><th>Entry Description</th><th>Employee</th><th style="border-top-right-radius:8px;">Modified By:</td></tr></thead><tbody><tr>
<td><input type="text" style="width:115px" id="mic" x-webkit-speech/></td>
<td><select id="inOutSearch"><option value="">Any</option><option value="in">IN</option><option value="out">OUT</option></td>
<!--The input fields below are responsible for the initial dates set for Start Date and End Date and the calendar option presented for each-->
<td><input type="text" value="<?php echo date('Y-m-d', strtotime('-1 week')); ?>" name="startDate" id="startDate" size=7 class='datepicker' style='float:left;'></td>
<td><input type="text" value="<?php echo date('Y-m-d'); ?>" name="endDate" id="endDate" size=7 class='datepicker' style='float:left;'></td>
<td><input type="text" id="descriptionSearch"/></td>
<td><select id='selectEmployee' class="selectEmployee" style="width:115px">
<!--Both PHP codes below are used to grab the employees which are in the dev team -->
</select></td>
<td><select id='selectModify' class="selectEmployee" style="width:115px">
</select>
</td></tr></tbody>
</table>

<!--This div contains the table which has all the log information that matches the information found in the search table-->
<div id="log"></div>

<?php
}
else
	{
	echo "You do not have permission to view this page";
	}
require ('../includes/includeAtEnd.php');
?>
