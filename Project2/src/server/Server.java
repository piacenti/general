package server;

import java.io.*;
import java.net.*;
import java.util.logging.*;

import com.sun.net.httpserver.*;

public class Server
{

	private static int SERVER_PORT_NUMBER = 8090;
	private static final int MAX_WAITING_CONNECTIONS = 10;
	private Facade facade;
	private static Logger logger;

	public Server(int port)
	{
		SERVER_PORT_NUMBER = port;
	}

	static
	{
		try
		{
			initLog();
		}
		catch (IOException e)
		{
			System.out.println("Could not initialize log: " + e.getMessage());
		}
	}

	private static void initLog() throws IOException
	{

		Level logLevel = Level.FINE;

		logger = Logger.getLogger("project");
		logger.setLevel(logLevel);
		logger.setUseParentHandlers(false);

		Handler consoleHandler = new ConsoleHandler();
		consoleHandler.setLevel(logLevel);
		consoleHandler.setFormatter(new SimpleFormatter());
		logger.addHandler(consoleHandler);

		FileHandler fileHandler = new FileHandler("log.txt", false);
		fileHandler.setLevel(logLevel);
		fileHandler.setFormatter(new SimpleFormatter());
		logger.addHandler(fileHandler);
	}

	private HttpServer server;

	private void run()
	{

		logger.info("Initializing Model");

		facade = new Facade();

		logger.info("Initializing HTTP Server");

		try
		{
			server = HttpServer.create(new InetSocketAddress(SERVER_PORT_NUMBER), MAX_WAITING_CONNECTIONS);
		}
		catch (IOException e)
		{
			logger.log(Level.SEVERE, e.getMessage(), e);
			return;
		}

		server.setExecutor(null); // use the default executor

		server.createContext("/validateUser", facade.validateUserHandler);
		server.createContext("/getProjects", facade.getProjectsHandler);
		server.createContext("/getSampleImage", facade.getSampleImageHandler);
		server.createContext("/downloadBatch", facade.downloadBatchHandler);

		server.createContext("/submitBatch", facade.submitBatchHandler);
		server.createContext("/getFields", facade.getFieldsHandler);
		server.createContext("/search", facade.searchHandler);
		server.createContext("/", facade.downloadFileHandler);

		logger.info("Starting HTTP Server");

		server.start();
	}

	public static void main(String[] args)
	{
		new Server(Integer.parseInt(args[0].trim())).run();
	}

}
