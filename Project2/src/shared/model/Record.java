/**
 * 
 */
package shared.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author gabriel_2
 * 
 */
@SuppressWarnings("serial")
public class Record implements Serializable
{
	private List<String> values = new ArrayList<>();
	private int id;
	private int parentId;
	private int row;

	/**
	 * @param strings
	 */
	public Record(List<String> strings)
	{
		values = strings;
	}
	
	public Record()
	{
	

	}

	/**
	 * @return the parentId
	 */
	public int getParentId()
	{
		return parentId;
	}

	/**
	 * @param parentId
	 *            the parentId to set
	 */
	public void setParentId(int parentId)
	{
		this.parentId = parentId;
	}

	/**
	 * @return the row
	 */
	public int getRow()
	{
		return row;
	}

	/**
	 * @param row
	 *            the row to set
	 */
	public void setRow(int row)
	{
		this.row = row;
	}

	/**
	 * @return
	 */
	public List<String> getValues()
	{
		return values;
	}

	/**
	 * @param values
	 */
	public void setValues(List<String> values)
	{
		this.values = values;
	}

	/**
	 * @return record id
	 */
	public int getId()
	{
		return id;
	}

	/**
	 * @param id
	 *            record id
	 */
	public void setId(int id)
	{
		this.id = id;
	}
	@Override
	public boolean equals(Object o)
	{
		if(this==o)
		{
			return true;
		}
		if(o==null)
		{
			return false;
		}
		if(this.getClass() !=  o.getClass())
		{
			return false;
		}
		Record other = (Record) o;
		for(int i=0; i<values.size();i++)
		{
			if(values.get(i).equals(other.getValues().get(i))==false)
					return false;
		}
		return true;
	}

}
