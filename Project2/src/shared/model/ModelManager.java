/**
 * 
 */
package shared.model;

import java.util.List;

/**Manages lists of database objects which can be summarized as users and projects
 * 
 *
 */
public class ModelManager
{
	private List<User> users;
	private List<Project> projects;
	/**
	 * @return the users
	 */
	public List<User> getUsers()
	{
		return users;
	}
	/**
	 * @param users the users to set
	 */
	public void setUsers(List<User> users)
	{
		this.users = users;
	}
	/**
	 * @return the projects
	 */
	public List<Project> getProjects()
	{
		return projects;
	}
	/**
	 * @param projects the projects to set
	 */
	public void setProjects(List<Project> projects)
	{
		this.projects = projects;
	}
}
