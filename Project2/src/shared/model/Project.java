package shared.model;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;

import server.database.Database;
import server.database.DatabaseException;

/**
 * Holds information about a project
 * 
 */
@SuppressWarnings("serial")
public class Project implements Serializable
{
	private int id;
	private String title;
	private int recordsPerImage;
	private int firstYCoord;
	private int recordHeight;
	private int assigned;
	private int completed;
	private List<Field> fields;
	private List<Image> images;
	public static int NOT_ASSIGNED = 0;
	public static int NOT_COMPLETED = 0;
	public static int COMPLETED = 1;
	private Database database;

	public Project()
	{

	}

	/**
	 * Adds a list of projects to the database
	 * 
	 * @param projects
	 * @return
	 * @throws IOException
	 * @throws SQLException
	 * @throws DatabaseException
	 */
	public static boolean databaseAddProjects(List<Project> projects) throws IOException, SQLException, DatabaseException
	{
		Project project = new Project();
		project.database = new Database();
		project.database.startTransaction();
		boolean result = project.database.getProjectDB().addProjects(projects);
		project.database.endTransaction();
		return result;
	}

	/**
	 * @param string
	 * @param i
	 * @param j
	 * @param k
	 * @param l
	 * @param m
	 */
	public Project(String string, int j, int k, int l, int m, int recPerImage)
	{
		title = string;
		firstYCoord = j;
		recordHeight = k;
		assigned = l;
		completed = m;
		recordsPerImage = recPerImage;
	}

	/**
	 * @return
	 */
	public int getId()
	{
		return id;
	}

	/**
	 * @param id
	 */
	public void setId(int id)
	{
		this.id = id;
	}

	/**
	 * @return
	 */
	public String getTitle()
	{
		return title;
	}

	/**
	 * @param title
	 */
	public void setTitle(String title)
	{
		this.title = title;
	}

	/**
	 * @return number of records per image
	 */
	public int getRecordsPerImage()
	{
		return recordsPerImage;
	}

	/**
	 * @param recordsPerImage
	 *            number of records per image
	 */
	public void setRecordsPerImage(int recordsPerImage)
	{
		this.recordsPerImage = recordsPerImage;
	}

	/**
	 * @return first y coordinate
	 */
	public int getFirstYCoord()
	{
		return firstYCoord;
	}

	/**
	 * @param firstYCoord
	 *            first y coordinate
	 */
	public void setFirstYCoord(int firstYCoord)
	{
		this.firstYCoord = firstYCoord;
	}

	/**
	 * @return record height
	 */
	public int getRecordHeight()
	{
		return recordHeight;
	}

	/**
	 * @param recordHeight
	 *            record height
	 */
	public void setRecordHeight(int recordHeight)
	{
		this.recordHeight = recordHeight;
	}

	/**
	 * @return true for project assigned and false for not assigned
	 */
	public int getAssigned()
	{
		return assigned;
	}

	/**
	 * @param i
	 *            true for project assigned and false for not assigned
	 */
	public void setAssigned(int i)
	{
		this.assigned = i;
	}

	/**
	 * @return true for project completed and false for not completed
	 */
	public int getCompleted()
	{
		return completed;
	}

	/**
	 * @param i
	 *            true for project completed and false for not completed
	 */
	public void setCompleted(int i)
	{
		this.completed = i;
	}

	/**
	 * @return
	 */
	public List<Field> getFields()
	{
		return fields;
	}

	/**
	 * @param fields
	 */
	public void setFields(List<Field> fields)
	{
		this.fields = fields;
	}

	/**
	 * @return
	 */
	public List<Image> getImages()
	{
		return images;
	}

	/**
	 * @param images
	 */
	public void setImages(List<Image> images)
	{
		this.images = images;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null)
		{
			return false;
		}
		if (this.getClass() != o.getClass())
		{
			return false;
		}
		Project other = (Project) o;
		if (title.equals(other.getTitle()) == false || firstYCoord != other.getFirstYCoord() || recordHeight != other.getRecordHeight()
				|| assigned != other.getAssigned() || completed != other.getCompleted() || images.size() != other.getImages().size()
				|| fields.size() != other.getFields().size())
		{
			return false;
		}

		List<Image> imageOther = other.getImages();
		List<Field> field = other.getFields();
		for (int i = 0; i < images.size(); i++)
		{
			if (images.get(i).equals(imageOther.get(i)) == false || field.get(i).equals(field.get(i)) == false)
				return false;
		}

		return true;

	}

	public boolean updateEquals(Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null)
		{
			return false;
		}
		if (this.getClass() != o.getClass())
		{
			return false;
		}
		Project other = (Project) o;
		if (assigned != other.getAssigned() || completed != other.getCompleted() || images.size() != other.getImages().size())
		{
			return false;
		}
		List<Image> imageOther = other.getImages();
		for (int i = 0; i < images.size(); i++)
		{
			if (images.get(i).updateEquals(imageOther.get(i)) == false)
				return false;
		}

		return true;
	}

	public void makeProjectValid()
	{
		//make sure that there are as many records per image as the recordsPerImage variable and make sure that there are as many values per record as fields
		for (Image image : images)
		{
			//
			List<Record> recordValues = image.getRecordValues();
			//make sure there are enough records
			for (int i = recordValues.size(); i < recordsPerImage; i++)
			{
				image.getRecordValues().add(new Record());
			}
			//fill up records to capacity and deletes anything that goes beyond limit
			recordValues = image.getRecordValues();
			for (int i = 0; i < recordsPerImage; i++)
			{
				assert recordValues.get(i) != null;
				if (recordValues.get(i).getValues().size() < fields.size())
				{
					int difference = fields.size() - recordValues.get(i).getValues().size();
					for (int a = 0; a < difference; a++)
					{
						recordValues.get(i).getValues().add(" ");
					}
				}
				else if (recordValues.get(i).getValues().size() < fields.size())
				{
					int difference = recordValues.get(i).getValues().size() - fields.size();
					for (int a = 0; a < difference; a++)
					{
						recordValues.get(i).getValues().remove((recordValues.get(i).getValues().size()) - 1);
					}
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "Project [id=" + id + ", title=" + title + ", recordsPerImage=" + recordsPerImage + ", firstYCoord=" + firstYCoord + ", recordHeight="
				+ recordHeight + ", assigned=" + assigned + ", completed=" + completed + ", fields=" + fields + ", images=" + images + ", database=" + database
				+ "]";
	}

}
