package shared.model;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;

import server.database.Database;
import server.database.DatabaseException;

/**
 * Holds information about users from the database
 * 
 */
@SuppressWarnings("serial")
public class User implements Serializable
{
	private int id;
	private String username;
	private String password;
	private String firstName;
	private String lastName;
	private String email;
	private int indexedRecords;
	private int hasAssignedImage;
	public static int NO_CURRENT_IMAGE=0;
	private Database database;

	public User()
	{

	}
	/**
	 * 
	 * @param userN
	 * @param pass
	 * @param fname
	 * @param lname
	 * @param em
	 * @param indexR
	 * @param currI
	 */
	public User(String userN, String pass, String fname, String lname, String em, int indexR, int currI)
	{
		username=userN;
		password = pass;
		firstName = fname;
		lastName = lname;
		email = em;
		indexedRecords = indexR;
		hasAssignedImage = currI;
	}
	/**
	 * 
	 * @param ID
	 * @param userN
	 * @param pass
	 * @param fname
	 * @param lname
	 * @param em
	 * @param indexR
	 * @param currI
	 */
	public User(int ID, String userN, String pass, String fname, String lname, String em, int indexR, int currI)
	{
		id=ID;
		username=userN;
		password = pass;
		firstName = fname;
		lastName = lname;
		email = em;
		indexedRecords = indexR;
		hasAssignedImage = currI;
	}
	/**
	 * Add a list of users to the database
	 * @param users
	 * @return
	 * @throws IOException
	 * @throws SQLException
	 * @throws DatabaseException
	 */
	public static boolean databaseAddUsers(List<User> users) throws IOException, SQLException, DatabaseException
	{
		User user= new User();
		user.database = new Database();
		user.database.startTransaction();
		boolean result = user.database.getUserDB().addUsers(users);
		user.database.endTransaction();
		return result;
	}
	/**
	 * @return the currentImage
	 */
	public int getCurrentImage()
	{
		return hasAssignedImage;
	}

	/**
	 * @param currentImage
	 *            the currentImage to set
	 */
	public void setCurrentImage(int currentImage)
	{
		this.hasAssignedImage = currentImage;
	}

	/**
	 * Get id
	 * 
	 * @return id
	 */
	public int getId()
	{
		return id;
	}

	/**
	 * Set id
	 * 
	 * @param id
	 */
	public void setId(int id)
	{
		this.id = id;
	}

	/**
	 * @return
	 */
	public String getUsername()
	{
		return username;
	}

	/**
	 * @param username
	 */
	public void setUsername(String username)
	{
		this.username = username;
	}

	/**
	 * @return
	 */
	public String getPassword()
	{
		return password;
	}

	/**
	 * @param password
	 */
	public void setPassword(String password)
	{
		this.password = password;
	}

	/**
	 * @return
	 */
	public String getFirstName()
	{
		return firstName;
	}

	/**
	 * @param firstName
	 */
	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	/**
	 * @return
	 */
	public String getLastName()
	{
		return lastName;
	}

	/**
	 * @param lastName
	 */
	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	/**
	 * @return l
	 */
	public String getEmail()
	{
		return email;
	}

	/**
	 * @param email
	 */
	public void setEmail(String email)
	{
		this.email = email;
	}

	/**
	 * @return number of records indexed by user
	 */
	public int getIndexedRecords()
	{
		return indexedRecords;
	}

	/**
	 * @param indexedRecords
	 *            number of records indexed by user
	 */
	public void setIndexedRecords(int indexedRecords)
	{
		this.indexedRecords = indexedRecords;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "User [id=" + id + ", username=" + username + ", password=" + password + ", firstName=" + firstName + ", lastName=" + lastName + ", email="
				+ email + ", indexedRecords=" + indexedRecords + ", hasAssignedImage=" + hasAssignedImage + ", database=" + database + "]";
	}
	

}
