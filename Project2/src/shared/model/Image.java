package shared.model;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Holds information about image as contained in the database
 * 
 */
@SuppressWarnings("serial")
public class Image implements Serializable
{
	private int id;
	private int parentId;
	private File path;
	private int height;
	private int width;
	private int assigned;
	private int completed;
	public static final int NOT_ASSIGNED = 0;
	public static final int NOT_COMPLETED = 0;
	public static final int NO_WIDTH = 0;
	public static final int NO_HEIGHT = 0;

	private List<Record> recordValues = new ArrayList<Record>();

	public Image()
	{

	}

	/**
	 * @param parId
	 * @param fielPath
	 * @param height_
	 * @param width_
	 * @param assigned_
	 * @param records
	 */
	public Image(int parId, String fielPath, int height_, int width_, int assigned_, int completed_, List<Record> records)
	{
		parentId = parId;
		path = new File(fielPath);
		height = height_;
		width = width_;
		assigned = assigned_;
		completed = completed_;
		recordValues = records;

	}

	/**
	 * @return the completed
	 */
	public int getCompleted()
	{
		return completed;
	}

	/**
	 * @param completed
	 *            the completed to set
	 */
	public void setCompleted(int completed)
	{
		this.completed = completed;
	}

	/**
	 * @return the parentId
	 */
	public int getParentId()
	{
		return parentId;
	}

	/**
	 * @param parentId
	 *            the parentId to set
	 */
	public void setParentId(int parentId)
	{
		this.parentId = parentId;
	}

	/**
	 * @return the assigned
	 */
	public int getAssigned()
	{
		return assigned;
	}

	/**
	 * @param assigned
	 *            the assigned to set
	 */
	public void setAssigned(int assigned)
	{
		this.assigned = assigned;
	}

	/**
	 * @param path
	 *            File object containing path where image is located
	 */
	public void getImage(File path)
	{

	}

	/**
	 * @return records associated with image
	 */
	public List<Record> getRecordValues()
	{
		return recordValues;
	}

	/**
	 * @param recordValues
	 *            records associated with image
	 */
	public void setRecordValues(List<Record> recordValues)
	{
		this.recordValues = recordValues;
	}

	/**
	 * @return image id
	 */
	public int getId()
	{
		return id;
	}

	/**
	 * @param id
	 *            image id
	 */
	public void setId(int id)
	{
		this.id = id;
	}

	/**
	 * @return image file object
	 */
	public File getPath()
	{
		return path;
	}

	/**
	 * @param path
	 *            image file object
	 */
	public void setPath(File path)
	{
		this.path = path;
	}

	/**
	 * @return image height
	 */
	public int getHeight()
	{
		return height;
	}

	/**
	 * @param height
	 *            image height
	 */
	public void setHeight(int height)
	{
		this.height = height;
	}

	/**
	 * @return image width
	 */
	public int getWidth()
	{
		return width;
	}

	/**
	 * @param width
	 *            image width
	 */
	public void setWidth(int width)
	{
		this.width = width;
	}

	/**
	 * Records of values, rows of values in image
	 * 
	 */
	@Override
	public boolean equals(Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null)
		{
			return false;
		}
		if (this.getClass() != o.getClass())
		{
			return false;
		}
		Image other = (Image) o;
		if (path.getPath().equals(other.getPath().getPath()) == false || height != other.getHeight() || width != other.getWidth()
				|| assigned != other.getAssigned() || completed != other.getCompleted() || recordValues.size() != other.getRecordValues().size()
				|| parentId != other.getParentId())
		{
			return false;
		}
		List<Record> recordOther = other.getRecordValues();
		for (int i = 0; i < recordValues.size(); i++)
		{
			if (recordValues.get(i).equals(recordOther.get(i)) == false)
				return false;
		}
		return true;

	}
/**
 * Equals used to compare on updatable fields in a database image
 * @param o
 * @return
 */
	public boolean updateEquals(Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null)
		{
			return false;
		}
		if (this.getClass() != o.getClass())
		{
			return false;
		}
		Image other = (Image) o;
		if (assigned != other.getAssigned() || completed != other.getCompleted() || recordValues.size() != other.getRecordValues().size())
		{
			return false;
		}

		List<Record> recordOther = other.getRecordValues();
		for (int i = 0; i < recordValues.size(); i++)
		{
			if (recordValues.get(i).equals(recordOther.get(i)) == false)
				return false;
		}

		return true;
	}

	/**
	 * Makes sure all the records in the image have the same number of values
	 * 
	 * @return
	 */
	public boolean validateImage()
	{
		//gets size of first record
		int size=0;
		if (recordValues.get(0) != null)
			size = recordValues.get(0).getValues().size();
		else
			return false;
		for (Record record : recordValues)
		{
			if (record.getValues().size() != size)
				return false;
		}
		// TODO Auto-generated method stub
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "Image [id=" + id + ", parentId=" + parentId + ", path=" + path + ", height=" + height + ", width=" + width + ", assigned=" + assigned
				+ ", completed=" + completed + ", recordValues=" + recordValues + "]";
	}

}
