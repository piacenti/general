package shared.communication.output;

import java.io.Serializable;

/**
 * Holds result information obtained after BatchSubmit request
 * 
 */
@SuppressWarnings("serial")
public class BatchSubmitOutput implements Serializable
{
	private boolean result;

	/**
	 * @param submitBatch
	 */
	public BatchSubmitOutput(boolean submitBatch)
	{
		result = submitBatch;
	}

	

	/**
	 * @return true for successful database update or false otherwise
	 */
	public boolean isResult()
	{
		return result;
	}

	/**
	 * @param result
	 *            true for successful database update or false otherwise
	 */
	public void setResult(boolean result)
	{
		this.result = result;
	}



	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return result+"\n";
	}

}
