package shared.communication.output;

import java.io.Serializable;
import java.util.List;

import shared.model.Field;

/**Holds result information obtained after a BatchGet request
 *
 */
@SuppressWarnings("serial")
public class BatchGetOutput implements Serializable
{
	private int batchId;
	private int projectId;
	private String imageUrl;
	private int firstYCoord;
	private int recordHeight;
	private int numRecords;
	private int numFields;
	private List<Field> fields;
	

	/**
	 * @param id
	 * @param parentId
	 * @param imaUrl
	 * @param firstYCoord_
	 * @param recordHeight_
	 * @param recordsPerImage
	 * @param numberOfFields
	 * @param fields_
	 */
	public BatchGetOutput(int id, int parentId, String imaUrl, int firstYCoord_, int recordHeight_, int recordsPerImage_, int numberOfFields, List<Field> fields_)
	{
		batchId=id;
		projectId=parentId;
		imageUrl=imaUrl;
		firstYCoord=firstYCoord_;
		recordHeight=recordHeight_;
		numRecords=recordsPerImage_;
		numFields=numberOfFields;
		fields=fields_;
	}


	/**
	 * @return the batchId
	 */
	public int getBatchId()
	{
		return batchId;
	}

	/**
	 * @param batchId the batchId to set
	 */
	public void setBatchId(int batchId)
	{
		this.batchId = batchId;
	}

	/**
	 * @return  id of project
	 */
	public int getProjectId()
	{
		return projectId;
	}

	/**
	 * @param projectId  id of project
	 */
	public void setProjectId(int projectId)
	{
		this.projectId = projectId;
	}


	/**
	 * @return  image url
	 */
	public String getImageUrl()
	{
		return imageUrl;
	}

	/**
	 * @param imageUrl  image url
	 */
	public void setImageUrl(String imageUrl)
	{
		this.imageUrl = imageUrl;
	}

	/**
	 * @return  first coordinate of Y
	 */
	public int getFirstYCoord()
	{
		return firstYCoord;
	}

	/**
	 * @param firstYCoord  first coordinate of Y
	 */
	public void setFirstYCoord(int firstYCoord)
	{
		this.firstYCoord = firstYCoord;
	}

	/**
	 * @return  record height
	 */
	public int getRecordHeight()
	{
		return recordHeight;
	}

	/**
	 * @param recordHeight  record height
	 */
	public void setRecordHeight(int recordHeight)
	{
		this.recordHeight = recordHeight;
	}

	/**
	 * @return  number of records
	 */
	public int getNumRecords()
	{
		return numRecords;
	}

	/**
	 * @param numRecords  number of records
	 */
	public void setNumRecords(int numRecords)
	{
		this.numRecords = numRecords;
	}

	/**
	 * @return  number of field
	 */
	public int getNumFields()
	{
		return numFields;
	}

	/**
	 * @param numFields  number of field
	 */
	public void setNumFields(int numFields)
	{
		this.numFields = numFields;
	}

	/**
	 * @return 
	 */
	public List<Field> getFields()
	{
		return fields;
	}

	/**
	 * @param fields 
	 */
	public void setFields(List<Field> fields)
	{
		this.fields = fields;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		StringBuilder string=new StringBuilder();
		string.append(batchId+"\n"+ projectId + "\n" + imageUrl + "\n" + firstYCoord
				+ "\n" + recordHeight + "\n" + numRecords + "\n" + numFields + "\n");
		for(Field field:fields)
		{
			string.append(field.toString());
		}
		return string.toString();
	}


	/**
	 * @return
	 */
	public String[] getFieldTitles()
	{
		String[] titles=new String[fields.size()];
		for(int i=0; i<fields.size();i++)
		{
			titles[i]=fields.get(i).getTitle();
		}
		return titles;
	}


	/**
	 * @return
	 */
	public int[] getXCoords()
	{
		int[] x=new int[fields.size()];
		for(int i=0; i<fields.size();i++)
		{
			x[i]=fields.get(i).getxCoord();
		}
		return x;
	}


	/**
	 * @return
	 */
	public int[] getFieldWidths()
	{
		int[] x=new int[fields.size()];
		for(int i=0; i<fields.size();i++)
		{
			x[i]=fields.get(i).getWidth();
		}
		return x;
	}


	/**
	 * @return
	 */
	public String[] getHelpPaths()
	{
		String[] x=new String[fields.size()];
		for(int i=0; i<fields.size();i++)
		{
			x[i]=fields.get(i).getHelpPath().getPath();
		}
		return x;
	}


	/**
	 * @return
	 */
	public String[] getKnownData()
	{
		String[] x=new String[fields.size()];
		for(int i=0; i<fields.size();i++)
		{
			x[i]=fields.get(i).getKnownDataPath().getPath();
		}
		return x;
	}
	

}
