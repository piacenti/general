package shared.communication.output;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import shared.model.Project;

/**
 * Holds result information acquired after a ProjectGet request
 * 
 */
@SuppressWarnings("serial")
public class ProjectGetOutput implements Serializable
{
	private StringBuilder result;
	private List<Project> projects;

	/**
	 * @param projects_
	 */
	public ProjectGetOutput(List<Project> projects_)
	{
		projects = projects_;
		result = new StringBuilder();
		for (Project project : projects_)
		{
			result.append(project.getId() + "\n");
			result.append(project.getTitle() + "\n");
		}
	}

	public String toString()
	{
		return result.toString();

	}

	/**
	 * @return the projects
	 */
	public List<Project> getProjects()
	{
		return projects;
	}

}
