/**
 * 
 */
package shared.communication.output;

import java.io.Serializable;

/**
 * @author gabriel_2
 * 
 */
@SuppressWarnings("serial")
public class SearchResult implements Serializable
{
	private int imageId;
	private String imageUrl;
	//value is >=1
	private int recNumber;
	private int fieldId;

	/**
	 * @return the imageId
	 */
	public int getImageId()
	{
		return imageId;
	}

	/**
	 * @param imageId
	 *            the imageId to set
	 */
	public void setImageId(int imageId)
	{
		this.imageId = imageId;
	}

	/**
	 * @return the imageUrl
	 */
	public String getImageUrl()
	{
		return imageUrl;
	}

	/**
	 * @param imageUrl
	 *            the imageUrl to set
	 */
	public void setImageUrl(String imageUrl)
	{
		this.imageUrl = imageUrl;
	}

	/**
	 * @return the recNumber
	 */
	public int getRecNumber()
	{
		return recNumber;
	}

	/**
	 * @param recNumber
	 *            the recNumber to set
	 */
	public void setRecNumber(int recNumber)
	{
		this.recNumber = recNumber;
	}

	/**
	 * @return the fieldId
	 */
	public int getFieldId()
	{
		return fieldId;
	}

	/**
	 * @param fieldId
	 *            the fieldId to set
	 */
	public void setFieldId(int fieldId)
	{
		this.fieldId = fieldId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return imageId + "\n" + imageUrl + "\n" + recNumber + "\n" + fieldId + "\n";
	}

}
