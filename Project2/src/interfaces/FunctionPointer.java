/**
 * 
 */
package interfaces;

/**
 * @author gabriel_2
 *
 */
public interface FunctionPointer
{
	public void Run(Object parameter);
}
