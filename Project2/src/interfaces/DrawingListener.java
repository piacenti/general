/**
 * 
 */
package interfaces;

/**
 * @author gabriel_2
 * 
 */
public interface DrawingListener
{

	void originChanged(int w_newOriginX, int w_newOriginY, double scale, int senderWidth, int senderHeight);
}
