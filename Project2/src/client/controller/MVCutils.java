/**
 * 
 */
package client.controller;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.RescaleOp;
import java.awt.image.WritableRaster;
import java.util.ArrayList;

import javax.swing.JComponent;

/**
 * @author gabriel_2
 * 
 */
public class MVCutils
{
	public static void notifyListeners(ActionEvent e, ArrayList<ActionListener> listeners)
	{
		for (ActionListener listener : listeners)
		{
			listener.actionPerformed(e);
		}
	}

	public static void centerWindow(java.awt.Window object)
	{
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		//centered
		object.setLocation(screenSize.width / 2 - object.getSize().width / 2, screenSize.height / 2 - object.getSize().height / 2);
	}
	public static void occupyFullWindow(Window invisible)
	{
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		//centered
		invisible.setLocation(0,0);
		invisible.setPreferredSize(screenSize);
	}

	public static BufferedImage imageCopy(BufferedImage bi)
	{
		ColorModel cm = bi.getColorModel();
		boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
		WritableRaster raster = bi.copyData(null);
		return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
	}
	public static void invertImage(BufferedImage image)
	{
		RescaleOp op = new RescaleOp(-1.0f, 255f, null);
		op.filter( image,  image);
	}
}
