/**
 * 
 */
package client.controller;

import interfaces.FunctionPointer;

import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import shared.communication.Validate;
import shared.communication.output.ValidateOutput;
import client.ClientCommunicator;
import client.ClientException;
import client.view.LoginDialog;

/**
 * @author gabriel_2
 * 
 */
public class LoginController
{
	public LoginController()
	{

	}

	public static void login(ClientCommunicator communicator, LoginDialog login, String command, FunctionPointer function, ActionEvent e)
	{
		@SuppressWarnings("unchecked")
		ArrayList<String> parameters = (ArrayList<String>) e.getSource();
		String username = parameters.get(0);
		String password = parameters.get(1);
		Validate userInput = new Validate(username, password);
		try
		{
			ValidateOutput userResult = communicator.validateUser(userInput);
			if (userResult != null && userResult.getResult() == true)
			{
				JOptionPane.showMessageDialog(login, "Welcome, " + userResult.getFname() + " " + userResult.getLname() + ".\n" + "You have indexed "
						+ userResult.getNumRecords() + " records", "Welcome to Indexer", JOptionPane.PLAIN_MESSAGE);
				function.Run(parameters);
			}
			else
			{
				JOptionPane.showMessageDialog(login, "Invalid username and/or password", "Login Failed", JOptionPane.ERROR_MESSAGE);
			}
		}
		catch (ClientException exception)
		{
			exception.printStackTrace();
		}
	}
}
