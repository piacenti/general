/**
 * 
 */
package client.controller;

import interfaces.DrawingListener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowListener;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JSplitPane;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import shared.communication.BatchGet;
import shared.communication.BatchSubmit;
import shared.communication.output.BatchGetOutput;
import client.ClientCommunicator;
import client.ClientException;
import client.model.BatchState;
import client.view.FormPanel;
import client.view.ImageNavigator;
import client.view.ImagePanel;
import client.view.Table;
import client.view.Window;

/**
 * @author gabriel_2
 * 
 */
public class MainFrameController
{

	private Window view = null;
	private BatchState state = null;

	public void loadState()
	{
		File saveFile = new File("users" + File.separator + view.getUserInfo().get(0) + File.separator + "save.xml");
		if (saveFile.exists())
		{
			XStream xml = new XStream(new DomDriver());
			try
			{
				FileReader file = new FileReader("users" + File.separator + view.getUserInfo().get(0) + File.separator + "save.xml");
				ObjectInputStream in = xml.createObjectInputStream(file);
				state = (BatchState) in.readObject();
				in.close();
				file.close();
				if (state.getImage() != null)
				{
					view.setEnableToolBar(true);
					view.setEnableMenuDownload(false);
					loadWindowSettings();
					setImage();
					createTable();
					createForm();
				}
				else
				{
					loadWindowSettings();
				}
			}
			catch (IOException | ClassNotFoundException e)
			{

			}
		}
		else
		{
			state = new BatchState();
			loadWindowSettings();
		}

	}

	/**
	 * 
	 */
	private void loadWindowSettings()
	{

		if (state.getWindowPosition() != null)
		{
			view.setSize(state.getWindowSize());
			view.setLocation(state.getWindowPosition());
			view.getSplitPane().setDividerLocation(state.getHorizontalSplit());
			view.getBottomPane().setDividerLocation(state.getVerticalSplit());
		}

		view.addComponentListener(view.windowSizeLoc);
		view.getBottomPane().addPropertyChangeListener(JSplitPane.DIVIDER_LOCATION_PROPERTY, view.splitterChanged);
		view.getSplitPane().addPropertyChangeListener(JSplitPane.DIVIDER_LOCATION_PROPERTY, view.splitterChanged);

	}

	/**
	 * @param event
	 * @param dialog
	 * 
	 */
	public void menuOperations(ActionEvent event)
	{

		String command = ((JMenuItem) event.getSource()).getText();
		if (command.equals("Download Batch"))
		{
			view.downloadBatchDialog();
		}
		else if (command.equals("Logout"))
		{
			saveState();
			MVCutils.notifyListeners(new ActionEvent(view, 1, "logout"), view.getListeners());
		}
		else if (command.equals("Exit"))
		{
			saveState();
			view.dispose();
		}

	}

	public void toolBarOperations(ActionEvent event)
	{
		String command = ((JButton) event.getSource()).getText();
		switch (command)
		{
			case "Zoom In":
				view.getBigImage().zoomIn();
				break;
			case "Zoom Out":
				view.getBigImage().zoomOut();
				break;
			case "Invert Image":
				MVCutils.invertImage((BufferedImage) view.getBigImage().getImage());
				view.getBigImage().setInverted(!view.getBigImage().isInverted());
				view.repaint();

				break;
			case "Toggle Highlight":
				view.getBigImage().setHighLenable(!view.getBigImage().isHighLenable());
				break;
			case "Save":
				saveState();
				break;
			case "Submit":
				view.setEnableToolBar(false);
				view.setEnableMenuDownload(true);
				submit();
				state.clear();
				saveState();
				clearWindow();
				break;
		}
	}


	/**
	 * 
	 */
	private void clearWindow()
	{
		ClientCommunicator communicator =view.getCommunicator();
		ArrayList<String> parameter= view.getUserInfo();
		ActionListener logout=view.getListeners().get(0);
		WindowListener onWindowClose=view.getWindowListeners()[0];
		view.dispose();
		view= new Window(communicator);
		view.addActionlistener(logout);
		this.setView(view);
		view.setController(this);
		view.setEnableToolBar(false);
		view.setEnableMenuDownload(true);
		view.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		view.setVisible(true);
		view.setUserInfo(parameter);
		view.getController().loadState();
		//set state to listen to window
		view.addWindowListener(onWindowClose);
		view.repaint();
	}

	public void downloadBOperations(ActionEvent event)
	{
		String command = ((JButton) event.getSource()).getText();

		switch (command)
		{
			case "Cancel":
				view.getDialog().dispose();
				break;
			case "View Sample":
				view.getSample();
				break;
			case "Download":
				downloadBatch();
				view.setEnableToolBar(true);
				view.setEnableMenuDownload(false);
				view.getDialog().dispose();
				break;
		}

	}

	/**
	 * 
	 */
	private void downloadBatch()
	{
		BatchGet input = new BatchGet(view.getUserInfo().get(0), view.getUserInfo().get(1), view.getProjectId());
		try
		{
			BatchGetOutput result = view.getCommunicator().downloadBatch(input);
			//updates BatchState
			state.setFields(result.getFieldTitles());
			state.setValues(new String[result.getNumRecords()][result.getNumFields() + 1]);

			state.setImage(result.getImageUrl());

			state.setFirstYCoord(result.getFirstYCoord());
			state.setXCoords(result.getXCoords());

			state.setHeight(result.getRecordHeight());
			state.setWidths(result.getFieldWidths());
			state.setNumFields(result.getNumFields());
			state.setNumRecords(result.getNumRecords());
			state.setHelpPath(result.getHelpPaths());
			state.setKnownData(result.getKnownData());
			state.setImageId(result.getBatchId());

			loadWindowSettings();
			setImage();
			createTable();
			createForm();
			saveState();
		}
		catch (ClientException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void setImage()
	{
		state.setFormListeners(new ArrayList<ActionListener>());
		state.setTableListeners(new ArrayList<ActionListener>());
		state.setImageListeners(new ArrayList<ActionListener>());
		ImagePanel big = view.getBigImage();
		big.setState(state);

		ImageNavigator nav = view.getBright().getImageNav();
		big.setImage(view.getImage(state.getImage()));
		big.addDrawingListener(drawingListener1);
		if (state.getZoomLevel() != 0)
		{
			big.setScale(state.getZoomLevel());
		}
		big.setW_centerX(state.getImageCenterX());
		big.setW_centerY(state.getImageCenterY());
		if (state.isImageInverted())
		{
			MVCutils.invertImage((BufferedImage) big.getImage());
		}
		big.setInverted(state.isImageInverted());

		if (state.getHighlight() != null)
		{
			int[] v = state.getHighlight();
			big.setHighlight(new Rectangle2D.Double(v[0], v[1], v[2], v[3]));
			big.setHighLenable(state.isHighlightEnable());
		}

		BufferedImage image2 = (BufferedImage) view.getImage(state.getImage());

		nav.setImage(image2, big);
		nav.setDragRect(big.getWidth(), big.getHeight());
		nav.setOrigin(big.getW_centerX(), big.getW_centerY(), big.getScale(), big.getWidth(), big.getHeight());
		nav.addDrawingListener(drawingListener2);
		big.addActionListener(state.imageEvent);
		view.repaint();
		view.setVisible(true);

	}

	private void createTable()
	{
		Table table = new Table(state, view);
		view.getBigImage().addHighlightListener(table.selectCell);
		view.repaint();

	}

	private void createForm()
	{

		FormPanel form = new FormPanel(state, view.getCommunicator());
		view.getBleft().getTabs().setComponentAt(1, form);

		//set the state to listen to changes in list or formFields
		form.addActionListener(state.formEvent);

		view.getBigImage().addHighlightListener(form.update);
		//set the form to listen to changes in the state
		state.addFormListeners(form.update);
	}

	public void saveState()
	{
		//create a folder for the user with his username
		File userFolder = new File("users" + File.separator + view.getUserInfo().get(0));
		// if the directory does not exist, create it
		if (!userFolder.exists())
		{
			userFolder.mkdir();
		}
		XStream xml = new XStream(new DomDriver());
		try
		{
			System.gc();
			File delete = new File("users" + File.separator + view.getUserInfo().get(0) + File.separator + "save.xml");
			if (delete.exists())
			{
				delete.delete();
			}
			FileWriter file = new FileWriter("users" + File.separator + view.getUserInfo().get(0) + File.separator + "save.xml");
			ObjectOutputStream out = xml.createObjectOutputStream(file);
			out.writeObject(state);
			out.close();

			file.close();
		}
		catch (IOException e)
		{

		}

	}

	private DrawingListener drawingListener1 = new DrawingListener()
	{

		@Override
		public void originChanged(int w_newOriginX, int w_newOriginY, double scale, int width, int height)
		{
			view.getBright().getImageNav().setOrigin(w_newOriginX, w_newOriginY, scale, width, height);
		}
	};

	private DrawingListener drawingListener2 = new DrawingListener()
	{

		@Override
		public void originChanged(int w_newOriginX, int w_newOriginY, double scale, int width, int height)
		{
			view.getBigImage().setOrigin(w_newOriginX, w_newOriginY, scale, width, height);
		}
	};

	/**
	 * @return the view
	 */
	public Window getView()
	{
		return view;
	}

	/**
	 * @param view
	 *            the view to set
	 */
	public void setView(Window view)
	{
		this.view = view;
	}

	/**
	 * @return the state
	 */
	public BatchState getState()
	{
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(BatchState state)
	{
		this.state = state;
	}

	public void submit()
	{
		//build BtachSubmit
		StringBuilder values = new StringBuilder();
		String[][] array = state.getValues();
		for (int a = 0; a < array.length; a++)
		{
			for (int b = 0; b < array[a].length - 1; b++)
			{
				if (array[a][b] != null)
				{
					values.append(array[a][b] + ",");
				}
				else
				{
					values.append(",");
				}
			}
			//remove last comma and add semi-colon
			values.deleteCharAt(values.lastIndexOf(","));
			values.append(";");
		}
		//remove last semi-colon
		values.deleteCharAt(values.lastIndexOf(";"));
		BatchSubmit submit = new BatchSubmit(view.getUserInfo().get(0), view.getUserInfo().get(1), state.getImageId(), values.toString());
		try
		{
			view.getCommunicator().submitBatch(submit);
		}
		catch (ClientException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
