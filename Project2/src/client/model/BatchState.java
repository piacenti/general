/**
 * 
 */
package client.model;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Rectangle2D.Double;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JTextField;

import client.controller.MVCutils;
import client.view.FormPanel;
import client.view.ImagePanel;

/**
 * @author gabriel_2
 * 
 */
@SuppressWarnings("serial")
public class BatchState implements Serializable
{
	private String[][] values;
	private String[] fields;
	private transient ArrayList<ActionListener> formListeners = new ArrayList<ActionListener>();
	private transient ArrayList<ActionListener> tableListeners = new ArrayList<ActionListener>();
	private transient ArrayList<ActionListener> imageListeners = new ArrayList<ActionListener>();
	private String image;
	private int imageId;
	private double zoomLevel = 0;
	private int imageCenterX = 0;
	private int imageCenterY = 0;
	private boolean imageInverted = false;
	private int[] highlight = null;
	private boolean highlightEnable = true;
	private int selectedRow = 0;
	private int selectedColumn = 1;
	private int firstYCoord = 0;
	private int[] xCoords = null;
	private int numRecords = 0;
	private int numFields = 0;
	private int height = 0;
	private int[] widths = null;
	private String[] helpPath = null;
	private String[] knownData = null;
	private HashMap<Integer, ArrayList<String>> knownDataMap;
	private int verticalSplit = 0;
	private int horizontalSplit = 0;
	private Dimension windowSize = null;
	private Point windowPosition = null;
	private HashMap<String, Boolean> checker = null;

	public ActionListener imageEvent = new ActionListener()
	{

		@Override
		public void actionPerformed(ActionEvent e)
		{
			ImagePanel im = (ImagePanel) e.getSource();
			if (e.getActionCommand().equals("set image"))
			{
				//image = MVCutils.imageCopy((BufferedImage) im.getImage());
			}
			zoomLevel = im.getScale();
			imageCenterX = im.getW_centerX();
			imageCenterY = im.getW_centerY();
			imageInverted = im.isInverted();
			if (e.getActionCommand().equals("highlighter"))
			{
				Rectangle2D rect = (Double) im.getHighlight();
				highlight = new int[] { (int) rect.getX(), (int) rect.getY(), (int) rect.getWidth(), (int) rect.getHeight() };
			}
			highlightEnable = im.isHighLenable();
		}
	};

	public ActionListener formEvent = new ActionListener()
	{

		@Override
		public void actionPerformed(ActionEvent e)
		{
			if (e.getSource() instanceof FormPanel)
			{
				FormPanel source = (FormPanel) e.getSource();
				//update row
				selectedRow = source.getList().getSelectedIndex();
				if (!e.getActionCommand().equals("focus"))
				{
					//update column values
					for (JTextField field : source.getFields())
					{
						//remove listeners
						field.getDocument().removeDocumentListener(source.notifyListeners);
						if (e.getActionCommand().equals("change text"))
						{
							String value = field.getText();
							setValues(value, selectedRow, source.getFields().indexOf(field));
						}
						else if (e.getActionCommand().equals("list"))
						{
							field.setText(getValue(selectedRow, source.getFields().indexOf(field)));
						}
						if (field.isFocusOwner())
						{
							selectedColumn = source.getFields().indexOf(field) + 1;
						}
						//re-add listeners
						field.getDocument().addDocumentListener(source.notifyListeners);

					}
				}
				else if (e.getActionCommand().equals("focus"))
				{
					focus(source);
				}
			}
			else if (e.getSource() instanceof Object[])
			{
				Object[] objects = (Object[]) e.getSource();
				FormPanel form = (FormPanel) objects[0];
				JTextField text = (JTextField) objects[1];
				for (JTextField field : form.getFields())
				{
					if (field == text)
					{
						selectedColumn = form.getFields().indexOf(field) + 1;
					}
				}
			}

			MVCutils.notifyListeners(new ActionEvent(BatchState.this, 1, "field update"), tableListeners);

		}
	};

	public void focus(FormPanel source)
	{
		//request focus by current field
		for (JTextField field : source.getFields())
		{
			//remove listeners
			field.getDocument().removeDocumentListener(source.notifyListeners);
			if (source.getFields().indexOf(field) == selectedColumn - 1)
			{
				field.requestFocusInWindow();
			}
			//re-add listeners
			field.getDocument().addDocumentListener(source.notifyListeners);

		}
	}

	public ActionListener tableEvent = new ActionListener()
	{

		@Override
		public void actionPerformed(ActionEvent e)
		{

			MVCutils.notifyListeners(new ActionEvent(e, 1, "update row"), formListeners);
		}
	};
	/**
	 * @return the values
	 */
	public String[][] getValues()
	{
		return values;
	}

	public String getValue(int row, int column)
	{
		return values[row][column];
	}

	/**
	 * @param values
	 *            the values to set
	 */
	public void setValues(String[][] values)
	{
		this.values = values;
	}

	public void setValues(String value, int row, int column)
	{
		values[row][column] = value;
	}

	/**
	 * @return the fields
	 */
	public String[] getFields()
	{
		return fields;
	}

	/**
	 * @param fields
	 *            the fields to set
	 */
	public void setFields(String[] fields)
	{
		this.fields = fields;
	}

	/**
	 * @return the tableFormListeners
	 */
	public ArrayList<ActionListener> getTableListeners()
	{
		return tableListeners;
	}

	/**
	 * @param tableFormListeners
	 *            the tableFormListeners to set
	 */
	public void setTableListeners(ArrayList<ActionListener> tableFormListeners)
	{
		this.tableListeners = tableFormListeners;
	}

	/**
	 * @return the windowListeners
	 */
	public ArrayList<ActionListener> getWindowListeners()
	{
		return imageListeners;
	}

	/**
	 * @param windowListeners
	 *            the windowListeners to set
	 */
	public void setWindowListeners(ArrayList<ActionListener> windowListeners)
	{
		this.imageListeners = windowListeners;
	}

	/**
	 * @return the image
	 */
	public String getImage()
	{
		return image;
	}

	/**
	 * @param image
	 *            the image to set
	 */
	public void setImage(String image)
	{
		this.image = image;
	}

	/**
	 * @return the zoomLevel
	 */
	public double getZoomLevel()
	{
		return zoomLevel;
	}

	/**
	 * @param zoomLevel
	 *            the zoomLevel to set
	 */
	public void setZoomLevel(double zoomLevel)
	{
		this.zoomLevel = zoomLevel;
	}

	/**
	 * @return the imageCenterX
	 */
	public int getImageCenterX()
	{
		return imageCenterX;
	}

	/**
	 * @param imageCenterX
	 *            the imageCenterX to set
	 */
	public void setImageCenterX(int imageCenterX)
	{
		this.imageCenterX = imageCenterX;
	}

	/**
	 * @return the imageCenterY
	 */
	public int getImageCenterY()
	{
		return imageCenterY;
	}

	/**
	 * @param imageCenterY
	 *            the imageCenterY to set
	 */
	public void setImageCenterY(int imageCenterY)
	{
		this.imageCenterY = imageCenterY;
	}

	/**
	 * @return the imageInverted
	 */
	public boolean isImageInverted()
	{
		return imageInverted;
	}

	/**
	 * @param imageInverted
	 *            the imageInverted to set
	 */
	public void setImageInverted(boolean imageInverted)
	{
		this.imageInverted = imageInverted;
	}

	/**
	 * @return the highlight
	 */
	public int[] getHighlight()
	{
		return highlight;
	}

	/**
	 * @param highlight
	 *            the highlight to set
	 */
	public void setHighlight(int[] highlight)
	{
		this.highlight = highlight;
	}

	/**
	 * @return the highlightEnable
	 */
	public boolean isHighlightEnable()
	{
		return highlightEnable;
	}

	/**
	 * @param highlightEnable
	 *            the highlightEnable to set
	 */
	public void setHighlightEnable(boolean highlightEnable)
	{
		this.highlightEnable = highlightEnable;
	}

	/**
	 * @return
	 */
	public int getFirstYCoord()
	{
		// TODO Auto-generated method stub
		return firstYCoord;
	}

	/**
	 * @param firstYCoord
	 *            the firstYCoord to set
	 */
	public void setFirstYCoord(int firstYCoord_)
	{
		firstYCoord = firstYCoord_;
	}

	/**
	 * @param xCoords
	 */
	public void setXCoords(int[] xCoords_)
	{
		xCoords = xCoords_;

	}

	/**
	 * @return the xCoords
	 */
	public int[] getxCoords()
	{
		return xCoords;
	}

	/**
	 * @param xCoords
	 *            the xCoords to set
	 */
	public void setxCoords(int[] xCoords_)
	{
		this.xCoords = xCoords_;
	}

	/**
	 * @return the numRecords
	 */
	public int getNumRecords()
	{
		return numRecords;
	}

	/**
	 * @param numRecords
	 *            the numRecords to set
	 */
	public void setNumRecords(int numRecords)
	{
		this.numRecords = numRecords;
	}

	/**
	 * @return the numFields
	 */
	public int getNumFields()
	{
		return numFields;
	}

	/**
	 * @param numFields
	 *            the numFields to set
	 */
	public void setNumFields(int numFields)
	{
		this.numFields = numFields;
	}

	/**
	 * @return the height
	 */
	public int getHeight()
	{
		return height;
	}

	/**
	 * @param height
	 *            the height to set
	 */
	public void setHeight(int height)
	{
		this.height = height;
	}

	/**
	 * @return the fieldWidth
	 */
	public int[] getWidths()
	{
		return widths;
	}

	/**
	 * @param fieldWidth
	 *            the fieldWidth to set
	 */
	public void setWidths(int[] fieldWidth)
	{
		this.widths = fieldWidth;
	}

	/**
	 * @return the helpPath
	 */
	public String[] getHelpPath()
	{
		return helpPath;
	}

	/**
	 * @param helpPath
	 *            the helpPath to set
	 */
	public void setHelpPath(String[] helpPath)
	{
		this.helpPath = helpPath;
	}

	/**
	 * @return the selectedRow
	 */
	public int getSelectedRow()
	{
		return selectedRow;
	}

	/**
	 * @param selectedRow
	 *            the selectedRow to set
	 */
	public void setSelectedRow(int selectedRow)
	{
		this.selectedRow = selectedRow;

	}

	/**
	 * @return the selectedColumn
	 */
	public int getSelectedColumn()
	{
		return selectedColumn;
	}

	/**
	 * @param selectedColumn
	 *            the selectedColumn to set
	 */
	public void setSelectedColumn(int selectedColumn)
	{
		this.selectedColumn = selectedColumn;
	}

	/**
	 * @return the imageListeners
	 */
	public ArrayList<ActionListener> getImageListeners()
	{
		return imageListeners;
	}

	/**
	 * @param imageListeners
	 *            the imageListeners to set
	 */
	public void setImageListeners(ArrayList<ActionListener> imageListeners)
	{
		this.imageListeners = imageListeners;
	}

	public void addTableListener(ActionListener e)
	{
		tableListeners.add(e);
	}

	/**
	 * @return the formListeners
	 */
	public ArrayList<ActionListener> getFormListeners()
	{
		return formListeners;
	}

	/**
	 * @param formListeners
	 *            the formListeners to set
	 */
	public void setFormListeners(ArrayList<ActionListener> formListeners)
	{
		this.formListeners = formListeners;
	}

	public void addFormListeners(ActionListener l)
	{
		formListeners.add(l);
	}

	/**
	 * @return the verticalPane
	 */
	public int getVerticalSplit()
	{
		return verticalSplit;
	}

	/**
	 * @param verticalPane
	 *            the verticalPane to set
	 */
	public void setVerticalSplit(int verticalPane)
	{
		this.verticalSplit = verticalPane;
	}

	/**
	 * @return the horizontalPane
	 */
	public int getHorizontalSplit()
	{
		return horizontalSplit;
	}

	/**
	 * @param horizontalPane
	 *            the horizontalPane to set
	 */
	public void setHorizontalSplit(int horizontalPane)
	{
		this.horizontalSplit = horizontalPane;
	}

	/**
	 * @return the windowSize
	 */
	public Dimension getWindowSize()
	{
		return windowSize;
	}

	/**
	 * @param windowSize
	 *            the windowSize to set
	 */
	public void setWindowSize(Dimension windowSize)
	{
		this.windowSize = windowSize;
	}

	/**
	 * @return the windowPosition
	 */
	public Point getWindowPosition()
	{
		return windowPosition;
	}

	/**
	 * @param windowPosition
	 *            the windowPosition to set
	 */
	public void setWindowPosition(Point windowPosition)
	{
		this.windowPosition = windowPosition;
	}

	/**
	 * @return the knownData
	 */
	public String[] getKnownData()
	{
		return knownData;
	}

	/**
	 * @param knownData
	 *            the knownData to set
	 */
	public void setKnownData(String[] knownData)
	{
		this.knownData = knownData;
	}

	/**
	 * @param checker
	 */
	public void setCorrectorSet(HashMap<String, Boolean> checker_)
	{
		checker = checker_;

	}

	/**
	 * @return the checker
	 */
	public HashMap<String, Boolean> getCorrectorSet()
	{
		return checker;
	}

	/**
	 * @param knownMap
	 */
	public void setKnownDataMap(HashMap<Integer, ArrayList<String>> knownMap)
	{
		knownDataMap=knownMap;
		
	}

	/**
	 * @return the knownDataMap
	 */
	public HashMap<Integer, ArrayList<String>> getKnownDataMap()
	{
		return knownDataMap;
	}

	/**
	 * 
	 */
	public void clear()
	{
		values=null;
		fields=null;
		/*
		 * formListeners = new ArrayList<ActionListener>(); tableListeners = new
		 * ArrayList<ActionListener>(); imageListeners = new
		 * ArrayList<ActionListener>();
		 */
		image=null;
		imageId=0;;
		zoomLevel = 0;
		imageCenterX = 0;
		imageCenterY = 0;
		imageInverted = false;
		highlight = null;
		highlightEnable = true;
		selectedRow = 0;
		selectedColumn = 1;
		firstYCoord = 0;
		xCoords = null;
		numRecords = 0;
		numFields = 0;
		height = 0;
		widths = null;
		helpPath = null;
		knownData = null;
		/*HashMap<Integer, ArrayList<String>> knownDataMap=null;
		HashMap<String, Boolean> checker = null;*/
		
	}

	/**
	 * @return the imageId
	 */
	public int getImageId()
	{
		return imageId;
	}

	/**
	 * @param imageId the imageId to set
	 */
	public void setImageId(int imageId)
	{
		this.imageId = imageId;
	}

}
