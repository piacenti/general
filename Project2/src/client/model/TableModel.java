/**
 * 
 */
package client.model;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

import client.controller.MVCutils;

/**
 * @author gabriel_2
 * 
 */
@SuppressWarnings("serial")
public class TableModel extends AbstractTableModel
{
	private String[][] values;
	private String[] fields;
	private ArrayList<ActionListener> listeners = new ArrayList<ActionListener>();
	public TableModel(String[][] values_, String[] strings)
	{
		fields = strings;
		values = values_;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	@Override
	public int getColumnCount()
	{
		return values[0].length;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	@Override
	public int getRowCount()
	{
		return values.length;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	@Override
	public Object getValueAt(int row, int column)
	{
		if (column == 0)
		{
			return row + 1;
		}
		else
		{
			column--;
			return values[row][column];
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.AbstractTableModel#setValueAt(java.lang.Object,
	 * int, int)
	 */
	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex)
	{
		columnIndex--;
		values[rowIndex][columnIndex]=(String) aValue;
		MVCutils.notifyListeners(new ActionEvent(new int[]{rowIndex,  columnIndex},  1,"values update"), listeners);
		//evaluate and highlight
		
				
				
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.AbstractTableModel#getColumnName(int)
	 */
	@Override
	public String getColumnName(int column)
	{
		if (column == 0)
			return "Record Number";
		else
			column--;
		return fields[column];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.AbstractTableModel#isCellEditable(int, int)
	 */
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex)
	{
		if (columnIndex == 0)
			return false;
		else
			return true;
	}
	public void addActionListener(ActionListener listener)
	{
		listeners.add(listener);
	}

}
