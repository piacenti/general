/**
 * 
 */
package client;

import interfaces.FunctionPointer;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import client.controller.LoginController;
import client.controller.MainFrameController;
import client.view.LoginDialog;
import client.view.Window;

/**
 * @author gabriel_2
 * 
 */
public class Gui
{
	private static ClientCommunicator communicator = null;
	private static Runnable runProgram = null;

	public static void main(String[] args)
	{
		communicator = new ClientCommunicator(args[0], args[1]);

		runProgram = new Runnable()
		{
			private LoginDialog login = new LoginDialog("Login to Indexer");
			private Window window = new Window(communicator);

			@Override
			public void run()
			{
				login.addActionListener(login_);
				login.setDefaultCloseOperation(LoginDialog.DISPOSE_ON_CLOSE);

				loginDialog(null);
			}

			private void loginDialog(ActionEvent e)
			{
				if (e != null)
				{
					Window win = (Window) e.getSource();
					win.dispose();
				}
				if (window != null)
				{
					window.dispose();
				}

				login.setVisible(true);
			}

			private ActionListener login_ = new ActionListener()
			{

				@Override
				public void actionPerformed(ActionEvent e)
				{
					LoginController.login(communicator, login, "validate", mainProgram, e);
				}
			};

			private ActionListener logout = new ActionListener()
			{
				@Override
				public void actionPerformed(ActionEvent e)
				{
					loginDialog(e);
				}
			};
			private WindowAdapter onWindowClose = new WindowAdapter()
			{
				/*
				 * (non-Javadoc)
				 * 
				 * @see
				 * java.awt.event.WindowAdapter#windowClosed(java.awt.event.
				 * WindowEvent)
				 */
				@Override
				public void windowClosing(WindowEvent arg0)
				{
					window.getController().saveState();
					super.windowClosed(arg0);
				}
			};
			private FunctionPointer mainProgram = new FunctionPointer()
			{
				@Override
				public void Run(Object parameter)
				{
					login.dispose();
					window = new Window(communicator);
					window.addActionlistener(logout);
					MainFrameController controller = new MainFrameController();
					controller.setView(window);
					window.setController(controller);
					window.setEnableToolBar(false);
					window.setEnableMenuDownload(true);
					window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					window.setVisible(true);
					window.setUserInfo(parameter);
					window.getController().loadState();
					//set state to listen to window
					window.addWindowListener(onWindowClose);
				}
			};

		};

		EventQueue.invokeLater(runProgram);

	}

}
