/**
 * 
 */
package client.test;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.*;

import server.database.Database;
import server.database.DatabaseException;
import shared.communication.*;
import shared.model.Field;
import shared.model.Image;
import shared.model.Project;
import shared.model.Record;
import shared.model.User;
import client.ClientCommunicator;
import client.ClientException;

/**
 * @author gabriel_2
 * 
 */
public class CommunicatorTest
{
	private ClientCommunicator communicator = new ClientCommunicator("localhost", "8090");
	private Database database;

	public CommunicatorTest() throws DatabaseException
	{
		database = new Database();
	}

	@Before
	public void setup() throws IOException, SQLException, DatabaseException
	{
		Database.recreateTables();
		database.startTransaction();
		List<Project> projects = new ArrayList<>();

		List<Record> records = new ArrayList<>();
		List<String> strings = new ArrayList<>();
		strings.add("value1");
		strings.add("value2");
		strings.add("value3");
		strings.add("value4");
		records.add(new Record(strings));
		strings = new ArrayList<>();
		strings.add("value5");
		strings.add("value6");
		strings.add("value7");
		strings.add("value8");
		records.add(new Record(strings));
		strings = new ArrayList<>();
		strings.add("value5");
		strings.add("value6");
		strings.add("value7");
		strings.add("value8");
		records.add(new Record(strings));
		List<Image> images = new ArrayList<>();
		images.add(new Image(1, "src" + File.separator + "sqlCommands" + File.separator + "sql.txt", 10, 10, 0, 0, records));

		//adds another image with three records where one repeats values
		records = new ArrayList<>();
		strings = new ArrayList<>();
		strings.add("value9");
		strings.add("value10");
		strings.add("value11");
		strings.add("value12");
		records.add(new Record(strings));
		strings = new ArrayList<>();
		strings.add("value13");
		strings.add("value14");
		strings.add("value15");
		strings.add("value16");
		records.add(new Record(strings));
		strings = new ArrayList<>();
		strings.add("value9");
		strings.add("value10");
		strings.add("value11");
		strings.add("value12");
		records.add(new Record(strings));
		images.add(new Image(1, "src" + File.separator + "sqlCommands" + File.separator + "sql.txt", 10, 10, 0, 0, records));

		List<Field> fields = new ArrayList<>();
		fields.add(new Field("name", 1, 1, "src" + File.separator + "sqlCommands" + File.separator + "sql.txt", "src" + File.separator + "sqlCommands"
				+ File.separator + "sql.txt"));
		fields.add(new Field("lastname", 1, 1, "src" + File.separator + "sqlCommands" + File.separator + "sql.txt", "src" + File.separator + "sqlCommands"
				+ File.separator + "sql.txt"));
		fields.add(new Field("mother", 1, 1, "src" + File.separator + "sqlCommands" + File.separator + "sql.txt", "src" + File.separator + "sqlCommands"
				+ File.separator + "sql.txt"));
		fields.add(new Field("father", 1, 1, "src" + File.separator + "sqlCommands" + File.separator + "sql.txt", "src" + File.separator + "sqlCommands"
				+ File.separator + "sql.txt"));

		Project project = new Project("1899", 1, 10, 0, 0, 3);
		project.setImages(images);
		project.setFields(fields);

		projects.add(project);
		//add slightly different project

		Project project2 = new Project("1900", 1, 10, 0, 0, 3);
		List<Record> records2 = new ArrayList<>();
		List<String> strings2 = new ArrayList<>();
		strings2.add("value1");
		strings2.add("value2");
		strings2.add("value3");
		records2.add(new Record(strings2));
		strings2 = new ArrayList<>();
		strings2.add("value5");
		strings2.add("value6");
		strings2.add("value7");
		records2.add(new Record(strings2));
		strings2 = new ArrayList<>();
		strings2.add("value5");
		strings2.add("value6");
		strings2.add("value7");
		records2.add(new Record(strings2));
		List<Image> images2 = new ArrayList<>();
		images2.add(new Image(1, "src" + File.separator + "sqlCommands" + File.separator + "sql.txt", 10, 10, 0, 0, records2));

		//adds another image with two records
		records2 = new ArrayList<>();
		strings2 = new ArrayList<>();
		strings2.add("value10");
		strings2.add("value12");
		strings2.add("value11");
		records2.add(new Record(strings2));
		strings2 = new ArrayList<>();
		strings2.add("value15");
		strings2.add("value16");
		strings2.add("value17");
		records2.add(new Record(strings2));
		strings2 = new ArrayList<>();
		strings2.add("value15");
		strings2.add("value16");
		strings2.add("value17");
		records2.add(new Record(strings2));
		images2.add(new Image(1, "src" + File.separator + "sqlCommands" + File.separator + "sql.txt", 10, 10, 0, 0, records2));

		List<Field> fields2 = new ArrayList<>();
		fields2.add(new Field("name", 1, 1, "src" + File.separator + "sqlCommands" + File.separator + "sql.txt", "src" + File.separator + "sqlCommands"
				+ File.separator + "sql.txt"));
		fields2.add(new Field("lastname", 1, 1, "src" + File.separator + "sqlCommands" + File.separator + "sql.txt", "src" + File.separator + "sqlCommands"
				+ File.separator + "sql.txt"));
		fields2.add(new Field("mother", 1, 1, "src" + File.separator + "sqlCommands" + File.separator + "sql.txt", "src" + File.separator + "sqlCommands"
				+ File.separator + "sql.txt"));
		project2.setImages(images2);
		project2.setFields(fields2);

		projects.add(project2);
		database.getProjectDB().addProjects(projects);

		User user = new User("pong", "thing", "john", "flemming", "test@test.com", 0, 0);
		database.getUserDB().addUser(user);

		//do the same as before with table already having entries
		user = new User("coisa", "feia", "blob", "flemming", "test2@test.com", 0, 0);
		database.getUserDB().addUser(user);

		//do the same as before with table already having entries
		user = new User("tetris", "feia", "cameo", "mateo", "test2@test.com", 0, 0);
		database.getUserDB().addUser(user);
		
		database.endTransaction();
	}

	@After
	public void tearDown()
	{
	}

	@Test
	public void testCommunicatorOperations() throws ClientException
	{
		assertEquals("TRUE\njohn\nflemming\n0\n",communicator.validateUser(new Validate("pong", "thing")));
		assertEquals("FALSE\n",communicator.validateUser(new Validate("ping", "thing")));
		assertEquals("1\n1899\n2\n1900\n",communicator.getProjects(new ProjectGet("pong", "thing")));
		assertEquals("http:"+File.separator+File.separator+"localhost:8090"+File.separator+"src"+File.separator+"sqlCommands"+File.separator+"sql.txt\n", communicator.getSampleImage(new SampleGet("pong", "thing", 1)));
		assertEquals("3\n2\nhttp:"+File.separator+File.separator+"localhost:8090"+File.separator+"src"+File.separator+"sqlCommands"+File.separator+"sql.txt\n"
				+ "1\n10\n3\n3\n"
				+ "5\n1\nname\n"
				+ "http:"+File.separator+File.separator+"localhost:8090"+File.separator+"src"+File.separator+"sqlCommands"+File.separator+"sql.txt\n"
				+ "1\n1\n"
				+ "http:"+File.separator+File.separator+"localhost:8090"+File.separator+"src"+File.separator+"sqlCommands"+File.separator+"sql.txt\n"
				+ "6\n2\nlastname\n"
				+ "http:"+File.separator+File.separator+"localhost:8090"+File.separator+"src"+File.separator+"sqlCommands"+File.separator+"sql.txt\n"
				+ "1\n1\n"
				+ "http:"+File.separator+File.separator+"localhost:8090"+File.separator+"src"+File.separator+"sqlCommands"+File.separator+"sql.txt\n"
				+ "7\n3\nmother\n"
				+ "http:"+File.separator+File.separator+"localhost:8090"+File.separator+"src"+File.separator+"sqlCommands"+File.separator+"sql.txt\n"
				+ "1\n1\n"
				+ "http:"+File.separator+File.separator+"localhost:8090"+File.separator+"src"+File.separator+"sqlCommands"+File.separator+"sql.txt\n"
				,communicator.downloadBatch(new BatchGet("pong", "thing", 2)));
		assertEquals("TRUE\n",communicator.submitBatch(new BatchSubmit("pong", "thing", 3, "lkjasd,laksdf,ljlkj;,,;,," )));
		
		communicator.downloadBatch(new BatchGet("pong", "thing", 1));
		communicator.submitBatch(new BatchSubmit("pong", "thing", 1, "value1, value2, value3, value4" ));
		
		communicator.downloadBatch(new BatchGet("pong", "thing", 1));
		communicator.submitBatch(new BatchSubmit("pong", "thing", 2, "value9,value10,value11,value12" ));
		
		assertEquals("1\n1\nname\n"
				+ "1\n2\nlastname\n"
				+ "1\n3\nmother\n"
				+ "1\n4\nfather\n",communicator.getFields(new FieldGet("pong", "thing", 1)));
		assertEquals("1\n"
				+ "http:"+File.separator+File.separator+"localhost:8090"+File.separator+"src"+File.separator+"sqlCommands"+File.separator+"sql.txt\n"
				+ "1\n1\n"
				+ "1\n"
				+ "http:"+File.separator+File.separator+"localhost:8090"+File.separator+"src"+File.separator+"sqlCommands"+File.separator+"sql.txt\n"
				+ "2\n1\n"
				+ "1\n"
				+ "http:"+File.separator+File.separator+"localhost:8090"+File.separator+"src"+File.separator+"sqlCommands"+File.separator+"sql.txt\n"
				+ "3\n1\n"
				+ "1\n"
				+ "http:"+File.separator+File.separator+"localhost:8090"+File.separator+"src"+File.separator+"sqlCommands"+File.separator+"sql.txt\n"
				+ "2\n2\n"
				+ "1\n"
				+ "http:"+File.separator+File.separator+"localhost:8090"+File.separator+"src"+File.separator+"sqlCommands"+File.separator+"sql.txt\n"
				+ "3\n2\n"
				,communicator.search(new Search("pong", "thing", "1,2", "value1, value5, value6")));
		assertEquals("DROP TABLE IF EXISTS fields;\nDROP TABLE IF EXISTS images;\nDROP TABLE IF EXISTS project;\nDROP TABLE IF EXISTS cell;\nDROP TABLE IF EXISTS users;\n"
				+ "\nCREATE TABLE IF NOT EXISTS users\n(\nid INTEGER not null primary key autoincrement,\nusername TEXT not null,\npassword TEXT not null,"
				+ "\nfirstName TEXT not null,\nlastName TEXT not null,\nemail TEXT not null,\ncurrentImage INTEGER,\nindexedRecords INTEGER not null\n);"
				+ "\nCREATE TABLE IF NOT EXISTS project\n(\nid INTEGER not null primary key autoincrement,\ntitle TEXT not null,\nrecordsPerImage INTEGER not null, \nfirstYCoord INTEGER not null,"
				+ "\nrecordHeight INTEGER not null,\nassigned INTEGER not null,\ncompleted INTEGER not null\n);\nCREATE TABLE IF NOT EXISTS fields\n("
				+ "\nid INTEGER not null primary key autoincrement,\nparentId INTEGER not null,\nnumber INTEGER not null,\ntitle TEXT not null,\nxCoord INTEGER not null,"
				+ "\nwidth INTEGER not null, \nhelp TEXT not null,\nknownData TEXT,\nFOREIGN KEY (parentId) REFERENCES project (id)\n);\nCREATE TABLE IF NOT EXISTS images"
				+ "\n(\nid INTEGER not null primary key autoincrement,\nparentId INTEGER not null,\nfile  TEXT not null,\nheight INTEGER not null,"
				+ "\nwidth INTEGER not null,\nassigned INTEGER,\ncompleted INTERGER,\nFOREIGN KEY (parentId) REFERENCES project (id)\n);"
				+ "\nCREATE TABLE IF NOT EXISTS cell\n(\nid INTEGER not null primary key autoincrement,\nparentId INTEGER not null,\nrow INTEGER not null,"
				+ "\ncolumn INTEGER not null,\nvalue TEXT,\nFOREIGN KEY (parentId) REFERENCES image (id)\n);\n"
				,communicator.downloadFile("http://localhost:8090/src/sqlCommands/sql.txt" ));
	}

}
