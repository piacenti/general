/**
 * 
 */
package client.view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;

import client.controller.MVCutils;

/**
 * @author gabriel_2
 * 
 */
@SuppressWarnings("serial")
public class SampleImageDialog extends JDialog
{
	private GridBagConstraints c = new GridBagConstraints();

	public SampleImageDialog(JImage image, java.awt.Window parent)
	{
		super(parent);
		setModal(true);
		setResizable(false);
		setLayout(new GridBagLayout());

		c.fill = GridBagConstraints.NONE;
		c.gridx = 0;
		c.gridy = 0;

		add(image, c);

		JButton close = new JButton("Close");
		close.addActionListener(exit);

		c.gridx = 0;
		c.gridy = 1;
		add(close, c);

		pack();

		MVCutils.centerWindow(this);
	}

	ActionListener exit = new ActionListener()
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			SampleImageDialog.this.dispose();

		}
	};
}
