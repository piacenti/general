/**
 * 
 */
package client.view;

import interfaces.DrawingListener;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

import javax.swing.JPanel;

/**
 * @author gabriel_2
 * 
 */
@SuppressWarnings("serial")
public class ImageNavigator extends JPanel
{
	private Image image = null;
	private Rectangle2D dragRect = null;
	private Rectangle2D rect = null;
	private double scale = 1;
	private Point lastPoint = null;
	private boolean dragging = false;
	private int w_centerX = 0;
	private int w_centerY = 0;
	private ArrayList<DrawingListener> listeners = new ArrayList<DrawingListener>();
	private Point trueCenter = null;

	public ImageNavigator()
	{
		setBackground(new Color(100, 100, 100, 255));
	}

	@Override
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		if (image != null)
		{
			g2.setColor(getBackground());
			g2.fillRect(0, 0, getWidth(), getHeight());

			scale = (double) (getHeight()) / (double) (image.getHeight(null));

			g2.translate(getWidth() / 2, getHeight() / 2);
			g2.scale(scale, scale);
			g2.translate(-w_centerX - image.getWidth(null) / 2, -w_centerY - image.getHeight(null) / 2);

			g2.drawImage(image, (int) rect.getMinX(), (int) rect.getMinY(), (int) rect.getMaxX(), (int) rect.getMaxY(), 0, 0, image.getWidth(null),
					image.getHeight(null), null);
			g2.setColor(new Color(0, 0, 255, 100));
			g2.fill(dragRect);
		}
	}

	/**
	 * @return the image
	 */
	public Image getImage()
	{
		return image;
	}

	/**
	 * @param image_
	 *            the image to set
	 */
	public void setImage(Image image_, ImagePanel sync)
	{
		image = image_;
		rect = new Rectangle2D.Double(0, 0, image.getWidth(null), image.getHeight(null));
		dragRect = new Rectangle2D.Double((sync.getImage().getWidth(null) - (sync.getWidth())) / 2, (sync.getImage().getHeight(null) - (sync.getHeight())) / 2,
				sync.getWidth(), sync.getHeight());
		w_centerX = 0;
		w_centerY = 0;

		addMouseListener(mouseEvent);
		addMouseMotionListener(mouseEvent);
	}

	MouseAdapter mouseEvent = new MouseAdapter()
	{
		@Override
		public void mousePressed(MouseEvent e)
		{
			int d_X = e.getX();
			int d_Y = e.getY();

			AffineTransform transform = new AffineTransform();
			transform.translate(getWidth() / 2, getHeight() / 2);
			transform.scale(scale, scale);
			transform.translate(-w_centerX - image.getWidth(null) / 2, -w_centerY - image.getHeight(null) / 2);

			Point2D d_Pt = new Point2D.Double(d_X, d_Y);
			Point2D w_Pt = new Point2D.Double();
			try
			{
				transform.inverseTransform(d_Pt, w_Pt);
			}
			catch (NoninvertibleTransformException ex)
			{
				return;
			}
			int w_X = (int) w_Pt.getX();
			int w_Y = (int) w_Pt.getY();

			System.out.println(d_X + "   " + w_X + "     " + d_Y + "    " + w_Y + "     " + dragRect.toString());
			boolean hitShape = false;
			if (dragRect.contains(w_X, w_Y) == true)
				hitShape = true;
			if (hitShape)
			{
				System.out.println("hit");
				lastPoint = new Point(w_X, w_Y);
				dragging = true;
			}

		};

		@Override
		public void mouseDragged(MouseEvent e)
		{
			if (dragging)
			{
				int d_X = e.getX();
				int d_Y = e.getY();

				AffineTransform transform = new AffineTransform();
				transform.translate(getWidth() / 2, getHeight() / 2);
				transform.scale(scale, scale);
				transform.translate(-w_centerX - image.getWidth(null) / 2, -w_centerY - image.getHeight(null) / 2);

				Point2D d_Pt = new Point2D.Double(d_X, d_Y);
				Point2D w_Pt = new Point2D.Double();
				try
				{
					transform.inverseTransform(d_Pt, w_Pt);
				}
				catch (NoninvertibleTransformException ex)
				{
					return;
				}
				int w_X = (int) w_Pt.getX();
				int w_Y = (int) w_Pt.getY();

				dragRect.setRect((w_X - lastPoint.getX()) + dragRect.getX(), (w_Y - lastPoint.getY()) + dragRect.getY(), dragRect.getWidth(),
						dragRect.getHeight());
				lastPoint = new Point(w_X, w_Y);
				notifyOriginChanged((int) dragRect.getX(), (int) dragRect.getY());
				repaint();
			}

		};

		@Override
		public void mouseReleased(MouseEvent e)
		{
			lastPoint = null;
		};
	};

	private void notifyOriginChanged(int w_newOriginX, int w_newOriginY)
	{
		//convert
		w_newOriginX = (int) (dragRect.getX()-image.getWidth(null)/2+dragRect.getWidth() / 2);
		w_newOriginY = (int) (dragRect.getY()-image.getHeight(null)/2+dragRect.getHeight() / 2);
		for (DrawingListener listener : listeners)
		{
			listener.originChanged(w_newOriginX, w_newOriginY, scale, getWidth(), getHeight());
		}
	}

	/**
	 * @param w_newOriginX
	 * @param w_newOriginY
	 * @param scale2
	 * @param height
	 * @param width
	 */
	public void setOrigin(int w_newOriginX, int w_newOriginY, double scale2, int width, int height)
	{

		if (trueCenter == null)
		{
			trueCenter = new Point(w_newOriginX, w_newOriginY);
		}

		//convert to the center relative to component from one to the other
		int centerX=w_newOriginX+image.getWidth(null)/2;
		int centerY=w_newOriginY+image.getHeight(null)/2;

		dragRect.setRect(centerX - (width / scale2) / 2, centerY - (height / scale2) / 2, width / scale2, height / scale2);
		repaint();
	}

	/**
	 * @param drawingListener1
	 */
	public void addDrawingListener(DrawingListener listener)
	{
		listeners.add(listener);
	}

	/**
	 * @return the rect
	 */
	public Rectangle2D getDragRect()
	{
		return dragRect;
	}

	/**
	 * @param rect
	 *            the rect to set
	 */
	public void setDragRect(int width, int height)
	{
		this.dragRect = new Rectangle2D.Double(dragRect.getX(), dragRect.getY(), width, height);
	}

}
