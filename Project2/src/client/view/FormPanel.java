/**
 * 
 */
package client.view;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import spell.Corrector;
import client.ClientCommunicator;
import client.ClientException;
import client.controller.MVCutils;
import client.model.BatchState;

/**
 * @author gabriel_2
 * 
 */
@SuppressWarnings("serial")
public class FormPanel extends JPanel
{
	private ArrayList<ActionListener> listeners = new ArrayList<>();
	private BatchState state = null;
	private JList<String> list = null;
	private ArrayList<JTextField> fields = new ArrayList<JTextField>();
	private PopupClicker dialog = null;
	private Point point = null;
	private int clickCount = 0;
	private ClientCommunicator communicator;
	private HashMap<Integer, ArrayList<String>> knownMap = null;

	/**
	 * @param state
	 */
	public FormPanel(BatchState state_, ClientCommunicator com)
	{
		communicator=com;
		state = state_;
		dialog = new PopupClicker((Window) this.getTopLevelAncestor(), state_);
		knownMap=state.getKnownDataMap();
		createForm();

	}

	public void createForm()
	{
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;

		setLayout(new GridBagLayout());
		Vector<String> listValues = new Vector<>();
		for (int i = 1; i <= state.getNumRecords(); i++)
		{
			listValues.add(i + "");
		}
		list = new JList<>(listValues);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setSelectedIndex(state.getSelectedRow());

		list.addListSelectionListener(notifyListListeners);
		JScrollPane scrollableList = new JScrollPane(list);
		scrollableList.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollableList.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		c.anchor = GridBagConstraints.WEST;
		c.weightx = 0.3;
		c.weighty = 0.3;
		c.gridx = 0;
		c.gridy = 0;
		add(scrollableList, c);

		JPanel formFields = new JPanel();
		formFields.setLayout(new GridBagLayout());
		c.fill = GridBagConstraints.NONE;

		int row = state.getSelectedRow();
		for (int i = 0; i < state.getFields().length; i++)
		{
			c.gridx = 0;
			c.gridy = i;
			formFields.add(new JLabel(state.getFields()[i]), c);
			c.gridx = 1;
			JTextField field = new JTextField(state.getValue(row, i), 10);

			field.getDocument().addDocumentListener(notifyListeners);

			field.addFocusListener(fieldFocus);
			field.addMouseListener(selected);

			formFields.add(field, c);
			fields.add(field);
		}

		JScrollPane scrollableFields = new JScrollPane(formFields);
		scrollableFields.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollableFields.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		c.fill = GridBagConstraints.BOTH;
		c.weightx = 1;
		c.weighty = 1;
		c.gridx = 1;
		c.gridy = 0;
		c.gridwidth = 2;
		c.insets = new Insets(0, 2, 0, 0);

		add(scrollableFields, c);
		addComponentListener(focus);
	}

	private FocusListener fieldFocus = new FocusListener()
	{

		@Override
		public void focusLost(FocusEvent e)
		{
			// TODO Auto-generated method stub
			correct(e);
		}

		@Override
		public void focusGained(FocusEvent e)
		{
			// TODO Auto-generated method stub
			MVCutils.notifyListeners(new ActionEvent(FormPanel.this, 1, "change text"), listeners);
		}
	};
	private ComponentListener focus = new ComponentListener()
	{

		@Override
		public void componentShown(ComponentEvent e)
		{
			MVCutils.notifyListeners(new ActionEvent(FormPanel.this, 1, "focus"), listeners);
		}

		@Override
		public void componentResized(ComponentEvent e)
		{
			// TODO Auto-generated method stub

		}

		@Override
		public void componentMoved(ComponentEvent e)
		{
			// TODO Auto-generated method stub

		}

		@Override
		public void componentHidden(ComponentEvent e)
		{
			// TODO Auto-generated method stub

		}
	};
	private MouseAdapter selected = new MouseAdapter()
	{
		public void mousePressed(java.awt.event.MouseEvent e)
		{
			MVCutils.notifyListeners(new ActionEvent(new Object[] { FormPanel.this, e.getSource() }, 1, "selected"), listeners);
		};
	};
	public ListSelectionListener notifyListListeners = new ListSelectionListener()
	{

		@Override
		public void valueChanged(ListSelectionEvent e)
		{
			MVCutils.notifyListeners(new ActionEvent(FormPanel.this, 1, "list"), listeners);
		}
	};

	public DocumentListener notifyListeners = new DocumentListener()
	{

		@Override
		public void changedUpdate(DocumentEvent arg0)
		{
			MVCutils.notifyListeners(new ActionEvent(FormPanel.this, 1, "change text"), listeners);
		}

		@Override
		public void insertUpdate(DocumentEvent arg0)
		{
			MVCutils.notifyListeners(new ActionEvent(FormPanel.this, 1, "change text"), listeners);
		}

		@Override
		public void removeUpdate(DocumentEvent arg0)
		{

			MVCutils.notifyListeners(new ActionEvent(FormPanel.this, 1, "change text"), listeners);
		}

	};
	public ActionListener update = new ActionListener()
	{

		@Override
		public void actionPerformed(ActionEvent e)
		{
			//update list
			//turn off listener
			list.removeListSelectionListener(notifyListListeners);
			list.setSelectedIndex(state.getSelectedRow());
			list.addListSelectionListener(notifyListListeners);
			//update Fields
			int row = state.getSelectedRow();
			for (JTextField field : fields)
			{
				//remove listener so that state is not notified of change since this update is coming from the state
				field.getDocument().removeDocumentListener(notifyListeners);
				//update text
				if (state.getSelectedColumn() == fields.indexOf(field) + 1)
				{
					field.setText(state.getValue(row, fields.indexOf(field)));
					field.requestFocus();
				}
				else
				{
					field.setText(state.getValue(row, fields.indexOf(field)));
				}

				field.getDocument().addDocumentListener(notifyListeners);

			}
			FormPanel.this.repaint();
		}
	};

	public void correct(FocusEvent e)
	{
		//get currently selected row
		int row=list.getSelectedIndex();
		//get value
		JTextField field_=(JTextField) e.getSource();
		String value=field_.getText();
		//check column
		int column=0;
		for(JTextField field:fields)
		{
			if(field==field_)
			{
				column=fields.indexOf(field);
				break;
			}
		}
		//check if word is in checker
		
		
		
	}
	public boolean find(ArrayList<String> container, String value)
	{
		for (String string : container)
		{
			if (string.equals(value.toLowerCase()))
				return true;
		}
		return false;

	}
	public ArrayList<String> getWords(int column)
	{
		ArrayList<String> result = new ArrayList<String>();
		String url = "http://" + communicator.getHost() + ":" + communicator.getPort() + "/" + state.getKnownData()[column - 1];

		try
		{
			Scanner input = new Scanner(communicator.downloadFile(url));
			input.useDelimiter(",");
			while (input.hasNext())
			{
				result.add(input.next().toLowerCase());
			}
			input.close();
		}
		catch (ClientException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		knownMap.put(column, result);
		return result;

	}
	private MouseAdapter rightClick = new MouseAdapter()
	{

		public void mouseClicked(java.awt.event.MouseEvent e)
		{
			if (e.getButton() == MouseEvent.BUTTON3 && clickCount == 0)
			{
				point = e.getLocationOnScreen();
				//check if clicked cell is red and get its text
				//get the row and column that was chosen
				Point point = new Point(e.getX(), e.getY());
				JTable table = (JTable) e.getSource();
				int row = table.rowAtPoint(point);
				int column = table.columnAtPoint(point);
				String value = (String) table.getModel().getValueAt(row, column);

				Color color = table.getCellRenderer(row, column).getTableCellRendererComponent(table, value, false, false, row, column).getBackground();

				Color red = new Color(255, 0, 0);
				if (color.getRed() == red.getRed() && color.getBlue() == red.getBlue() && color.getGreen() == red.getGreen())
				{
					System.out.println(value);
					System.out.println(color);
					//bring up pop up next to the mouse
					//get the known data for the column and create dialog that will suggest corrections

					dialog.setWords(state.getKnownDataMap().get(column));
					dialog.setSearchWord(value);
					dialog.setLocation(e.getLocationOnScreen());
					dialog.setVisible(true);
					dialog.setRow(row);
					dialog.setColumn(column);

					//create dialog with known data
				}
			}
			clickCount = 1;
		};

		public void mouseReleased(MouseEvent e)
		{
			clickCount = 0;
		};

		public void mouseMoved(MouseEvent e)
		{
			//checks how far mouse moved from initial position and make dialog not visible if too far
			if (point != null)
			{
				int x = (int) Math.abs(point.getX() - e.getLocationOnScreen().getX());
				int y = (int) Math.abs(point.getY() - e.getLocationOnScreen().getY());
				if (x > 50 || y > 50)
				{
					dialog.setVisible(false);
					point = null;
				}
			}
		};

	};

	public void addActionListener(ActionListener l)
	{
		listeners.add(l);
	}

	/**
	 * @return the listeners
	 */
	public ArrayList<ActionListener> getListeners()
	{
		return listeners;
	}

	/**
	 * @param listeners
	 *            the listeners to set
	 */
	public void setListeners(ArrayList<ActionListener> listeners)
	{
		this.listeners = listeners;
	}

	/**
	 * @return the list
	 */
	public JList<String> getList()
	{
		return list;
	}

	/**
	 * @param list
	 *            the list to set
	 */
	public void setList(JList<String> list)
	{
		this.list = list;
	}

	/**
	 * @return the fields
	 */
	public ArrayList<JTextField> getFields()
	{
		return fields;
	}

	/**
	 * @param fields
	 *            the fields to set
	 */
	public void setFields(ArrayList<JTextField> fields)
	{
		this.fields = fields;
	}

}
