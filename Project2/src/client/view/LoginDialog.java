/**
 * 
 */
package client.view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import client.controller.MVCutils;

/**
 * @author gabriel_2
 * 
 */
@SuppressWarnings("serial")
public class LoginDialog extends JDialog
{
	private JTextField username = new JTextField(25);
	private JPasswordField password = new JPasswordField(25);
	private GridBagConstraints c = new GridBagConstraints();
	private ArrayList<ActionListener> listener = new ArrayList<>();

	public LoginDialog(String title)
	{
		setTitle(title);
		setLayout(new GridBagLayout());
		setResizable(false);
		c.fill = GridBagConstraints.NONE;

		//username
		c.gridx = 0;
		c.gridy = 0;
		c.insets = new Insets(10, 5, 0, 10);
		add(new JLabel("Username: "), c);

		
		c.gridx = 1;
		c.gridy = 0;
		add(username, c);

		//password
		c.gridx = 0;
		c.gridy = 1;
		add(new JLabel("Password: "), c);

		c.gridx = 1;
		c.gridy = 1;
		add(password, c);

		//buttons
		JPanel buttons = new JPanel();
		buttons.setLayout(new GridBagLayout());

		JButton loginButton = new JButton("Login");
		loginButton.addActionListener(validate);
		c.gridx = 0;
		c.gridy = 0;
		c.insets = new Insets(5, 0, 5, 0);
		buttons.add(loginButton, c);

		JButton exitButton = new JButton("Exit");
		exitButton.addActionListener(exit);
		c.gridx = 1;
		c.gridy = 0;
		c.insets = new Insets(5, 5, 5, 0);
		buttons.add(exitButton, c);

		c.gridx = 0;
		c.gridy = 2;
		c.gridwidth = 2;
		add(buttons, c);
		pack();
		MVCutils.centerWindow((java.awt.Window) this);
	}

	private ActionListener validate = new ActionListener()
	{

		@Override
		public void actionPerformed(ActionEvent e)
		{
			ArrayList<String> parameters = new ArrayList<String>();
			parameters.add(username.getText());
			parameters.add(new String(password.getPassword()));

			MVCutils.notifyListeners(new ActionEvent(parameters, 1, "validate"), listener);
		}

	};
	private ActionListener exit = new ActionListener()
	{

		@Override
		public void actionPerformed(ActionEvent arg0)
		{
			LoginDialog.this.dispose();
		}
	};

	public void addActionListener(ActionListener action)
	{
		listener.add(action);
	}

}
