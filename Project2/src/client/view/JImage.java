/**
 * 
 */
package client.view;

import interfaces.FunctionPointer;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseWheelEvent;
import java.awt.geom.AffineTransform;

import javax.swing.JComponent;

/**
 * This is a generic Image component that supports zoom in and zoom out
 * operations. Scaling is done based on the center of parent component.
 * 
 */
@SuppressWarnings("serial")
public class JImage extends JComponent
{
	private Image image = null;
	private double scale = 0;
	private double scaleLimit = 2;
	private double scaleChange = 0.1;
	
	public JImage(Image image_, double scale_)
	{

		image = image_;
		scale = scale_;
		componentSize();

		this.addMouseListener(test);

		setMinimumSize(new Dimension(0, 0));

	}

	public JImage(Image image_, double scale_, double scaleLimit_)
	{

		image = image_;
		scale = scale_;
		scaleLimit = scaleLimit_;
		componentSize();

		this.addMouseListener(test);

		setMinimumSize(new Dimension(0, 0));

	}

	private void componentSize()
	{
		Dimension d = new Dimension();
		d.setSize(image.getWidth(null) * scale, image.getHeight(null) * scale);
		setPreferredSize(d);
		setSize(d);
		System.out.println(d.toString());
		repaint();
	}

	@Override
	protected void paintComponent(Graphics g)
	{

		super.paintComponent(g);

		Graphics2D g2 = (Graphics2D) g;
		g2.drawImage(image, new AffineTransform(scale, 0, 0, scale, 0, 0), null);

	}

	MouseAdapter test = new MouseAdapter()
	{
		@Override
		public void mouseWheelMoved(MouseWheelEvent e)
		{

		};
	};

	public void zoomIn()
	{
		if (scale < scaleLimit)
		{
			FunctionPointer function = new FunctionPointer()
			{

				@Override
				public void Run(Object parameter)
				{
					scale += scaleChange;
					componentSize();

				}
			};
			
			perpectiveAlgorithm(function);
		}
	}

	public void zoomOut()
	{
		if (scale > 1 / scaleLimit)
		{
			FunctionPointer function = new FunctionPointer()
			{

				@Override
				public void Run(Object parameter)
				{
					scale -= scaleChange;
					componentSize();

				}
			};
			
			perpectiveAlgorithm(function);
		}
	}

	private void perpectiveAlgorithm(FunctionPointer function)
	{
		JComponent component = (JComponent) getParent();
		//get middle point of parent component and this component
		Point componentMiddle = new Point(component.getWidth() / 2, component.getHeight() / 2);

		//determine the slope of perspective lines from middle of parent component
		double slopeTleft = (componentMiddle.getY() - getY()) / (getX() - componentMiddle.getX());
		double slopeTright = (componentMiddle.getY() - getY()) / ((getX() + getWidth()) - componentMiddle.getX());
		double slopeBleft = (componentMiddle.getY() - (getY() + getHeight())) / (getX() - componentMiddle.getX());
		int initialx = (int) (getX() - componentMiddle.getX());
		int initialy = (int) (componentMiddle.getY() - getY());
		int oldx = getX();
		int oldy = getY();

		//is center left inside or right of image, -1 for left and 1 for right
		int position = 1;
		if (componentMiddle.getX() - getX() < 0)
		{
			position = 1;
		}
		else if (componentMiddle.getX() - getX() > 0)
		{
			position = -1;
		}

		//1 above, 0 below;
		int aboveBelow = 1;
		if (componentMiddle.getY() - getY() < 0)
		{
			aboveBelow = 0;
		}
		else if (componentMiddle.getY() - getY() > 0)
		{
			aboveBelow = 1;
		}

		//scale the distance to match scaling
		function.Run(null);

		//position component so that each corner is on one of the corresponding lines
		//get two points that are on the line and that have a distance equal to the width of this component
		int afterx = 0;
		int aftery = 0;
		boolean found = false;
		double x = 0;
		while (found == false && x < component.getWidth() * scaleLimit)
		{
			int y1 = (int) (slopeTleft * x);
			int y3 = (int) (slopeBleft * x);

			int x1 = (int) (x / slopeTleft);
			int x2 = (int) (x / slopeTright);

			int heigthDifference = Math.abs(y1 - y3);
			int widthDifference = Math.abs(x1 - x2);

			if (Math.abs(heigthDifference - getHeight()) < 1 && position == 1)
			{

				afterx = (int) x;
				aftery = y1;
				System.out.println("heigth found " + x + " " + y1 + " slope " + slopeTleft + " position " + position + " oldx " + oldx + " change x "
						+ (afterx - initialx));
				found = true;
				break;
			}
			else if (Math.abs(widthDifference - getWidth()) < 1 && aboveBelow == 1)
			{

				afterx = x1;
				aftery = (int) x;
				System.out.println("width found " + x1 + " " + (-x) + " slope " + slopeTleft + " position " + position + " oldx " + oldx + " change x "
						+ afterx);
				found = true;
				break;
			}

			y1 = (int) (slopeTleft * -x);
			y3 = (int) (slopeBleft * -x);

			x1 = (int) (-x / slopeTleft);
			x2 = (int) (-x / slopeTright);

			widthDifference = Math.abs(x1 - x2);
			heigthDifference = Math.abs(y1 - y3);
			if (Math.abs(heigthDifference - getHeight()) < 1 && position == -1)
			{

				afterx = (int) -x;
				aftery = y1;
				System.out.println("heigth found " + (-x) + " " + y1 + " slope " + slopeTleft + " position " + position + " oldx " + oldx + " change x "
						+ afterx);
				found = true;
				break;
			}
			else if (Math.abs(widthDifference - getWidth()) < 1 && aboveBelow == 0)
			{

				afterx = x1;
				aftery = (int) -x;
				System.out.println("width found " + x1 + " " + (-x) + " slope " + slopeTleft + " position " + position + " oldx " + oldx + " change x "
						+ afterx);
				found = true;
				break;
			}

			x=x+0.1;
		}
		int finalx = oldx + (afterx - initialx);
		int finaly = oldy - (aftery - initialy);

		if (finalx > component.getWidth() - (getWidth()) / 10)
		{
			finalx = (int) (component.getWidth() - (getWidth() / 10));
		}
		else if (finalx < -(getWidth() * 9) / 10)
		{
			finalx = (int) (-(getWidth() * 9) / 10);
		}
		if (finaly > component.getHeight() - (getHeight() / 10))
		{
			finaly = (int) (component.getHeight() - (getHeight() / 10));
		}
		else if (finaly < -(getHeight() * 9) / 10)
		{
			finaly = (int) -((getHeight() * 9) / 10);
		}
		System.out.println(finalx);
		setLocation(finalx, finaly);
	}

	/**
	 * @return the scale
	 */
	public double getScale()
	{
		return scale;
	}

	/**
	 * @param scale
	 *            the scale to set
	 */
	public void setScale(double scale)
	{
		this.scale = scale;
	}

	/**
	 * @return the scaleChange
	 */
	public double getScaleChange()
	{
		return scaleChange;
	}

	/**
	 * @param scaleChange
	 *            the scaleChange to set
	 */
	public void setScaleChange(double scaleChange)
	{
		this.scaleChange = scaleChange;
	}

}
