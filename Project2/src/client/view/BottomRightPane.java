/**
 * 
 */
package client.view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.text.AbstractDocument;

/**
 * @author gabriel_2
 * 
 */
@SuppressWarnings("serial")
public class BottomRightPane extends JPanel
{
	private GridBagConstraints c = new GridBagConstraints();
	private ImageNavigator imageNav=null;
	private JEditorPane fhelp=null;
	
	public BottomRightPane()
	{
		setLayout(new GridBagLayout());

		JTabbedPane tabs = new JTabbedPane();
		c.fill = GridBagConstraints.BOTH;
		fhelp = new JEditorPane();
		fhelp.setContentType("text/html");
		AbstractDocument doc = (AbstractDocument) fhelp.getDocument();
		doc.setAsynchronousLoadPriority(0);
		fhelp.setEditable(false);
		fhelp.setLayout(new GridBagLayout());

		JScrollPane scrollableHelp = new JScrollPane(fhelp);
		scrollableHelp.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollableHelp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		imageNav = new ImageNavigator();

		tabs.add("Field Help", scrollableHelp);
		tabs.addTab("Image Navigation", imageNav);

		c.weightx = 1;
		c.weighty = 1;
		c.gridx = 0;
		c.gridy = 0;
		add(tabs, c);
	}

	/**
	 * @return the imageNav
	 */
	public ImageNavigator getImageNav()
	{
		return imageNav;
	}

	/**
	 * @param imageNav the imageNav to set
	 */
	public void setImageNav(ImageNavigator imageNav)
	{
		this.imageNav = imageNav;
	}

	/**
	 * @return the fhelp
	 */
	public JEditorPane getFhelp()
	{
		return fhelp;
	}

	/**
	 * @param fhelp the fhelp to set
	 */
	public void setFhelp(JEditorPane fhelp)
	{
		this.fhelp = fhelp;
	}

	
}
