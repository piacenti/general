/**
 * 
 */
package client.view;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.*;

import shared.communication.SampleGet;
import client.ClientCommunicator;
import client.ClientException;
import client.controller.MainFrameController;
import client.model.BatchState;

/**
 * @author gabriel_2
 * 
 */
@SuppressWarnings("serial")
public class Window extends JFrame
{
	private GridBagConstraints c = new GridBagConstraints();
	private ClientCommunicator communicator = null;

	private MenuPane menu = null;
	private ToolBarPane toolbar = null;
	private DownloadDialog dialog = null;
	private ImagePanel bigImage = null;
	private BottomLeftPane bleft = null;
	private BottomRightPane bright = null;

	private Rectangle windowBounds = GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds();
	private ArrayList<ActionListener> listeners = new ArrayList<ActionListener>();
	private ArrayList<String> userInfo = new ArrayList<>();
	private MainFrameController controller = null;

	private JSplitPane bottomPane = null;
	private JSplitPane splitPane = null;

	public Window(ClientCommunicator communicator_)
	{
		communicator = communicator_;
		menu = new MenuPane("File", new String[] { "Download Batch", "Logout", "Exit" });
		toolbar = new ToolBarPane(new String[] { "Zoom In", "Zoom Out", "Invert Image", "Toggle Highlight", "Save", "Submit" });
		

		setLayout(new GridBagLayout());
		fullWindow();
		setMainWindowPanes();

		

	}

	public void fullWindow()
	{
		setSize(windowBounds.width, windowBounds.height);
		setLocation(0, 0);
		Window.this.setResizable(true);
	}

	public void setMainWindowPanes()
	{
		setMenu();
		setToolBar();
		setSplitPanes();
	}

	/**
	 * 
	 */
	private void setMenu()
	{
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1;
		c.gridx = 0;
		c.gridy = 0;
		add(menu, c);
		menu.addActionListener(menuListener);
	}

	/**
	 * 
	 */
	private void setToolBar()
	{
		c.fill = GridBagConstraints.NONE;
		c.anchor = GridBagConstraints.WEST;
		c.weightx = 1;
		c.gridx = 0;
		c.gridy = 1;
		c.insets = new Insets(2, 0, 2, 0);
		toolbar.addActionListener(toolBarEvent);
		add(toolbar, c);
	}

	public void setEnableToolBar(boolean input)
	{
		for (Component component : toolbar.getComponents())
		{
			component.setEnabled(input);
		}
	}

	private ActionListener toolBarEvent = new ActionListener()
	{

		@Override
		public void actionPerformed(ActionEvent e)
		{
			controller.toolBarOperations(e);
		}
	};

	/**
	 * 
	 */
	public void setSplitPanes()
	{
		bleft = new BottomLeftPane();
		bright = new BottomRightPane();
		bigImage = new ImagePanel();
		bottomPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, bleft, bright);
		bottomPane.setDividerLocation((int) (Math.abs(windowBounds.width) / 2.5));
		
		splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, bigImage, bottomPane);
		splitPane.setDividerLocation(Math.abs(windowBounds.height) / 2);
		
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 1;
		c.weighty = 1;
		c.gridx = 0;
		c.gridy = 2;
		add(splitPane, c);

	}

	public PropertyChangeListener splitterChanged = new PropertyChangeListener()
	{

		@Override
		public void propertyChange(PropertyChangeEvent arg0)
		{
			windowEvent.actionPerformed(null);
		}
	};
	public ComponentListener windowSizeLoc = new ComponentListener()
	{

		@Override
		public void componentShown(ComponentEvent arg0)
		{

		}

		@Override
		public void componentResized(ComponentEvent arg0)
		{
			windowEvent.actionPerformed(null);

		}

		@Override
		public void componentMoved(ComponentEvent arg0)
		{
			windowEvent.actionPerformed(null);
		}

		@Override
		public void componentHidden(ComponentEvent arg0)
		{

		}
	};
	public ActionListener windowEvent = new ActionListener()
	{

		@Override
		public void actionPerformed(ActionEvent e)
		{
			BatchState state=controller.getState();
			state.setVerticalSplit(Window.this.bottomPane.getDividerLocation());
			state.setHorizontalSplit(Window.this.splitPane.getDividerLocation());
			state.setWindowSize(Window.this.getSize());
			state.setWindowPosition(Window.this.getLocation());
		}
	};
	public void downloadBatchDialog()
	{
		dialog = new DownloadDialog(userInfo.get(0), userInfo.get(1), communicator, ((JFrame) this));
		dialog.addActionListener(downloadDialog);
		dialog.setVisible(true);
	}

	public void getSample()
	{
		int projectId = 0;

		projectId = getProjectId();

		if (projectId != 0)
		{
			SampleGet input = new SampleGet(userInfo.get(0), userInfo.get(1), projectId);
			JImage image = new JImage(getImage(sampleURLRequest(input)), 0.5);
			SampleImageDialog sampleDialog = new SampleImageDialog(image, dialog);
			sampleDialog.setVisible(true);
		}
	}

	public int getProjectId()
	{
		return Integer.parseInt(dialog.getProjectIds().get(dialog.getBox().getSelectedItem()));
	}

	private String sampleURLRequest(SampleGet input)
	{
		try
		{
			return communicator.getSampleImage(input);
		}
		catch (ClientException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public void setEnableMenuDownload(boolean input)
	{
		for (JMenuItem item : menu.getReference())
		{
			if (item.getText().equals("Download Batch"))
				item.setEnabled(input);
		}
	}

	public java.awt.Image getImage(String url)
	{
		BufferedImage image = null;
		try
		{
			image = ImageIO.read((InputStream) communicator.downloadFile(url));
		}
		catch (IOException | ClientException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return image;
	}

	private ActionListener menuListener = new ActionListener()
	{

		@Override
		public void actionPerformed(ActionEvent e)
		{
			controller.menuOperations(e);
		}

	};
	private ActionListener downloadDialog = new ActionListener()
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			controller.downloadBOperations(e);
		}

	};

	public void addActionlistener(ActionListener action)
	{
		listeners.add(action);
	}

	/**
	 * @param parameter
	 */
	@SuppressWarnings("unchecked")
	public void setUserInfo(Object parameter)
	{
		userInfo = (ArrayList<String>) parameter;
	}

	/**
	 * @return the listeners
	 */
	public ArrayList<ActionListener> getListeners()
	{
		return listeners;
	}

	/**
	 * @param listeners
	 *            the listeners to set
	 */
	public void setListeners(ArrayList<ActionListener> listeners)
	{
		this.listeners = listeners;
	}

	/**
	 * @return the communicator
	 */
	public ClientCommunicator getCommunicator()
	{
		return communicator;
	}

	/**
	 * @return the userInfo
	 */
	public ArrayList<String> getUserInfo()
	{
		return userInfo;
	}

	/**
	 * @param userInfo
	 *            the userInfo to set
	 */
	public void setUserInfo(ArrayList<String> userInfo)
	{
		this.userInfo = userInfo;
	}

	/**
	 * @param controller2
	 */
	public void setController(MainFrameController controller_)
	{
		controller = controller_;
	}

	/**
	 * @return the dialog
	 */
	public DownloadDialog getDialog()
	{
		return dialog;
	}

	/**
	 * @return the bigImage
	 */
	public ImagePanel getBigImage()
	{
		return bigImage;
	}

	/**
	 * @return the bleft
	 */
	public BottomLeftPane getBleft()
	{
		return bleft;
	}

	/**
	 * @return the bright
	 */
	public BottomRightPane getBright()
	{
		return bright;
	}

	/**
	 * @return the controller
	 */
	public MainFrameController getController()
	{
		return controller;
	}

	/**
	 * @return the bottomPane
	 */
	public JSplitPane getBottomPane()
	{
		return bottomPane;
	}

	/**
	 * @param bottomPane
	 *            the bottomPane to set
	 */
	public void setBottomPane(JSplitPane bottomPane)
	{
		this.bottomPane = bottomPane;
	}

	/**
	 * @return the splitPane
	 */
	public JSplitPane getSplitPane()
	{
		return splitPane;
	}

	/**
	 * @param splitPane
	 *            the splitPane to set
	 */
	public void setSplitPane(JSplitPane splitPane)
	{
		this.splitPane = splitPane;
	}

	/**
	 * @return the menu
	 */
	public MenuPane getMenu()
	{
		return menu;
	}

	/**
	 * @return the toolbar
	 */
	public ToolBarPane getToolbar()
	{
		return toolbar;
	}

}
