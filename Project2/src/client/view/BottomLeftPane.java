/**
 * 
 */
package client.view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;

/**
 * @author gabriel_2
 * 
 */
@SuppressWarnings("serial")
public class BottomLeftPane extends JPanel
{
	private GridBagConstraints c = new GridBagConstraints();
	private JList<String> list = null;
	private JTable table = null;
	private JTabbedPane tabs = null;

	public BottomLeftPane()
	{
		setLayout(new GridBagLayout());
		tabs = new JTabbedPane();
		c.fill = GridBagConstraints.BOTH;

		tabs.addTab("Table Entry", new JPanel());
		tabs.addTab("Form Entry", createForm());

		c.weightx = 1;
		c.weighty = 1;
		c.gridx = 0;
		c.gridy = 0;
		add(tabs, c);
	}

	public JPanel createForm()
	{
		JPanel form = new JPanel();
		form.setLayout(new GridBagLayout());
		list = new JList<>();

		JScrollPane scrollableList = new JScrollPane(list);
		scrollableList.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollableList.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		c.anchor = GridBagConstraints.WEST;
		c.weightx = 0.3;
		c.weighty = 0.3;
		c.gridx = 0;
		c.gridy = 0;
		form.add(scrollableList, c);

		JPanel formFields = new JPanel();

		JScrollPane scrollableFields = new JScrollPane(formFields);
		scrollableFields.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollableFields.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		c.fill = GridBagConstraints.BOTH;
		c.weightx = 1;
		c.weighty = 1;
		c.gridx = 1;
		c.gridy = 0;
		c.gridwidth = 2;
		c.insets = new Insets(0, 2, 0, 0);

		form.add(scrollableFields, c);
		return form;
	}

	/**
	 * @return the table
	 */
	public JTable getTable()
	{
		return table;
	}

	/**
	 * @param table
	 *            the table to set
	 */
	public void setTable(JTable table)
	{
		this.table = table;
		System.out.println("new table");
		revalidate();
		repaint();
		setVisible(true);
	}

	/**
	 * @return the tabs
	 */
	public JTabbedPane getTabs()
	{
		return tabs;
	}

	/**
	 * @param tabs
	 *            the tabs to set
	 */
	public void setTabs(JTabbedPane tabs)
	{
		this.tabs = tabs;
	}

}
