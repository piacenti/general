/**
 * 
 */
package client.view;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

/**
 * @author gabriel_2
 * 
 */
@SuppressWarnings("serial")
public class MenuPane extends JPanel
{
	private GridBagConstraints c = new GridBagConstraints();
	private ArrayList<ActionListener> listeners = new ArrayList<ActionListener>();
	private JMenuBar menuBar = null;
	private static int column = 0;
	private ArrayList<JMenuItem> itemReference=new ArrayList<JMenuItem>();

	public MenuPane(String menuName, String[] options)
	{
		setLayout(new GridBagLayout());

		menuBar = new JMenuBar();
		menuBar.setMinimumSize(new Dimension(100, 30));
		addMenu(menuName, options);
	}

	public void addMenu(String menuName, String[] options)
	{
		JMenu menu = new JMenu(menuName);
		menuBar.add(menu);

		for (String string : options)
		{
			JMenuItem item = new JMenuItem(string);
			item.addActionListener(notifyListeners);
			menu.add(item);
			itemReference.add(item);
		}

		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = column;
		c.gridy = 0;
		c.weightx = 1;
		add(menuBar, c);
	}

	/**
	 * @return
	 */
	private ActionListener notifyListeners = new ActionListener()
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			for (ActionListener listener : MenuPane.this.listeners)
			{
				listener.actionPerformed(e);
			}

		}
	};

	/**
	 * @return the reference
	 */
	public ArrayList<JMenuItem> getReference()
	{
		return itemReference;
	}

	public void addActionListener(ActionListener e)
	{
		listeners.add(e);
	}

	/**
	 * @return the listeners
	 */
	public ArrayList<ActionListener> getListeners()
	{
		return listeners;
	}

	/**
	 * @param listeners the listeners to set
	 */
	public void setListeners(ArrayList<ActionListener> listeners)
	{
		this.listeners = listeners;
	}
	
}
