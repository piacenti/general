/**
 * 
 */
package client.view;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultCellEditor;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import client.controller.MVCutils;

/**
 * @author gabriel_2
 * 
 */
@SuppressWarnings("serial")
public class TableCellEdit extends DefaultCellEditor
{
	private int curRow = 0;
	private int curCol = 0;
	/**
	 * @param name
	 */

	public TableCellEdit(JTextField name)
	{
		
		super(name);
		setClickCountToStart(1);
		name.getDocument().addDocumentListener(notifyListeners);
	}

	private ArrayList<ActionListener> listeners = new ArrayList<ActionListener>();

	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
	{
		curRow = row;
		curCol = column;
		MVCutils.notifyListeners(new ActionEvent(new int[] { curRow, curCol }, 1, "highlight"), listeners);
		return super.getTableCellEditorComponent(table, value, isSelected, row, column);
	}

	public void addActionListerner(ActionListener listener)
	{
		listeners.add(listener);
	}

	public void removeActionListener(ActionListener listener)
	{
		listeners.remove(listener);
	}

	private DocumentListener notifyListeners = new DocumentListener()
	{

		@Override
		public void removeUpdate(DocumentEvent e)
		{
			MVCutils.notifyListeners(new ActionEvent(new Object[] {e, new int[]{curRow, curCol} } , 1, "table text update"), listeners);
		}

		@Override
		public void insertUpdate(DocumentEvent e)
		{
			MVCutils.notifyListeners(new ActionEvent(new Object[] {e, new int[]{curRow, curCol} } , 1, "table text update"), listeners);
		}

		@Override
		public void changedUpdate(DocumentEvent e)
		{
			MVCutils.notifyListeners(new ActionEvent(new Object[] {e, new int[]{curRow, curCol} } , 1, "table text update"), listeners);
		}
	};

}
