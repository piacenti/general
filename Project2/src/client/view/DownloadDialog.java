/**
 * 
 */
package client.view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.TreeMap;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import client.ClientCommunicator;
import client.ClientException;
import client.controller.MVCutils;
import shared.communication.ProjectGet;
import shared.communication.output.ProjectGetOutput;
import shared.model.Project;

/**
 * @author gabriel_2
 * 
 */
@SuppressWarnings("serial")
public class DownloadDialog extends JDialog
{
	private GridBagConstraints c = new GridBagConstraints();
	private ClientCommunicator communicator = null;
	private ArrayList<ActionListener> listeners = new ArrayList<ActionListener>();
	private JComboBox<String> box = null;
	private TreeMap<String, String> projectIds = null;

	public DownloadDialog(String username, String password, ClientCommunicator communicator_, JFrame window)
	{
		super(window);
		communicator = communicator_;
		setLayout(new GridBagLayout());
		setModal(true);
		setResizable(false);

		JPanel organize = new JPanel();
		c.fill = GridBagConstraints.NONE;
		c.anchor = GridBagConstraints.CENTER;
		c.insets = new Insets(0, 2, 0, 2);
		c.gridx = 0;
		c.gridy = 0;

		organize.add(new JLabel("Project: "), c);

		ProjectGet projects = new ProjectGet(username, password);
		box = createComboBox(projects);

		c.gridx = 1;
		c.gridy = 0;
		organize.add(box, c);

		JButton viewSample = new JButton("View Sample");
		viewSample.addActionListener(notifylisteners);
		JButton cancel = new JButton("Cancel");
		cancel.addActionListener(notifylisteners);
		JButton download = new JButton("Download");
		download.addActionListener(notifylisteners);

		c.gridx = 2;
		c.gridy = 0;
		organize.add(viewSample, c);

		c.gridx = 0;
		c.gridy = 0;
		add(organize, c);

		JPanel org = new JPanel();

		c.gridx = 0;
		c.gridy = 0;
		org.add(cancel, c);

		c.gridx = 1;
		c.gridy = 0;
		org.add(download, c);

		c.gridx = 0;
		c.gridy = 1;
		add(org, c);

		pack();
		MVCutils.centerWindow(this);

	}

	private ActionListener notifylisteners = new ActionListener()
	{

		@Override
		public void actionPerformed(ActionEvent e)
		{
			MVCutils.notifyListeners(e, listeners);

		}
	};

	private ProjectGetOutput getProjects(ProjectGet projects)
	{
		try
		{
			return communicator.getProjects(projects);
		}
		catch (ClientException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	private JComboBox<String> createComboBox(ProjectGet projects)
	{
		JComboBox<String> result = new JComboBox<String>();
		ProjectGetOutput proResult = getProjects(projects);
		result = new JComboBox<>();
		projectIds = new TreeMap<>();
		for (Project project : proResult.getProjects())
		{
			projectIds.put(project.getTitle().trim(), Integer.toString(project.getId()).trim());
			result.addItem(project.getTitle());
		}
		return result;
	}

	public void addActionListener(ActionListener listener_)
	{
		listeners.add(listener_);
	}

	/**
	 * @return the box
	 */
	public JComboBox<String> getBox()
	{
		return box;
	}

	/**
	 * @return the projectIds
	 */
	public TreeMap<String, String> getProjectIds()
	{
		return projectIds;
	}

}
