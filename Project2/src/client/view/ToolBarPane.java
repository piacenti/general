/**
 * 
 */
package client.view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;

import client.controller.MVCutils;

/**
 * @author gabriel_2
 * 
 */
@SuppressWarnings("serial")
public class ToolBarPane extends JPanel
{
	private GridBagConstraints c = new GridBagConstraints();
	private ArrayList<ActionListener> listeners = new ArrayList<ActionListener>();

	public ToolBarPane(String[] titles)
	{
		setLayout(new GridBagLayout());
		c.fill = GridBagConstraints.NONE;
		c.anchor = GridBagConstraints.WEST;
		c.insets = new Insets(0, 5, 0, 0);

		int counter = 1;
		for (String title : titles)
		{
			JButton button = new JButton(title);
			button.addActionListener(notifyListeners);
			c.gridx = counter;
			c.gridy = 0;
			add(button, c);
			counter++;
		}

	}

	ActionListener notifyListeners = new ActionListener()
	{

		@Override
		public void actionPerformed(ActionEvent e)
		{
			MVCutils.notifyListeners(e, listeners);

		}
	};

	/**
	 * @param toolBarEvent
	 */
	public void addActionListener(ActionListener toolBarEvent)
	{
		listeners.add(toolBarEvent);
	}

	/**
	 * @return the listeners
	 */
	public ArrayList<ActionListener> getListeners()
	{
		return listeners;
	}

	/**
	 * @param listeners the listeners to set
	 */
	public void setListeners(ArrayList<ActionListener> listeners)
	{
		this.listeners = listeners;
	}
	
}
