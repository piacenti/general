/**
 * 
 */
package client.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import client.model.BatchState;
import spell.Corrector;
import spell.Dictionary;
import spell.SpellCorrector.NoSimilarWordFoundException;

/**
 * @author gabriel_2
 * 
 */
@SuppressWarnings("serial")
public class PopupClicker extends java.awt.Window
{
	private ArrayList<String> words = null;
	private String searchWord = "";
	private JLabel label = null;
	private Color defaultColor = null;
	private JPanel wrapper = null;
	private JDialog dialog = null;
	private JList<String> list=null;
	private int row=0;
	private int column=0;
	BatchState state=null;

	/**
	 * @param view
	 * @param words
	 */
	public PopupClicker(Window view, BatchState state_)
	{
		super(view);
		state=state_;
		label = new JLabel("See Suggestions");
		defaultColor = label.getBackground();
		label.setOpaque(true);
		wrapper = new JPanel();
		wrapper.setPreferredSize(new Dimension(100, 25));
		wrapper.add(label);
		//add listener to label
		label.addMouseListener(hover);
		add(wrapper);

		pack();
		repaint();

	}

	/**
	 * @return the words
	 */
	public ArrayList<String> getWords()
	{
		return words;
	}

	/**
	 * @param words
	 *            the words to set
	 */
	public void setWords(ArrayList<String> words)
	{
		this.words = words;
	}

	private MouseAdapter hover = new MouseAdapter()
	{
		public void mouseClicked(java.awt.event.MouseEvent e)
		{
			System.out.println(PopupClicker.this.words);
			PopupClicker.this.setVisible(false);
			GridBagConstraints c = new GridBagConstraints();

			dialog = new JDialog();
			dialog.setLayout(new GridBagLayout());

			list = new JList<String>(getSuggestions());
			list.setPreferredSize(new Dimension(100, 100));
			list.setSize(new Dimension(100, 100));
			JScrollPane scrollableList = new JScrollPane(list);
			scrollableList.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
			JButton cancel = new JButton("Cancel");

			cancel.addActionListener(close);
			JButton useSuggestion = new JButton("Use Suggestion");

			useSuggestion.addActionListener(replaceWord);

			c.fill = GridBagConstraints.BOTH;
			c.weightx = 1;
			c.weighty = 1;
			c.gridwidth = 2;
			c.gridx = 0;
			c.gridy = 0;
			dialog.add(scrollableList, c);
			c.weightx = 0;
			c.weighty = 0;
			c.gridwidth = 1;
			c.gridx = 0;
			c.gridy = 1;
			dialog.add(cancel, c);
			c.gridx = 1;
			c.gridy = 1;
			dialog.add(useSuggestion, c);

			int height = 250;
			int width = 250;
			dialog.setResizable(false);
			dialog.setPreferredSize(new Dimension(width, height));
			dialog.setSize(new Dimension(width, height));
			dialog.setModal(true);
			dialog.setLocation(e.getXOnScreen(), e.getYOnScreen() - height);
			dialog.setVisible(true);
			//dialog.pack();

		};

		public void mouseEntered(java.awt.event.MouseEvent e)
		{
			label.setBackground(new Color(0, 0, 255, 100));
			repaint();
		};

		public void mouseExited(java.awt.event.MouseEvent e)
		{
			label.setBackground(defaultColor);
			repaint();
		};
	};
	private ActionListener replaceWord = new ActionListener()
	{

		@Override
		public void actionPerformed(ActionEvent e)
		{
			state.setValues(list.getSelectedValue(), row, column-1);
			dialog.dispose();
			getParent().repaint();
		}
	};
	private ActionListener close = new ActionListener()
	{

		@Override
		public void actionPerformed(ActionEvent e)
		{
			dialog.dispose();
		}
	};

	/**
	 * @return the searchWord
	 */
	public String getSearchWord()
	{
		return searchWord;
	}

	/**
	 * @param searchWord
	 *            the searchWord to set
	 */
	public void setSearchWord(String searchWord)
	{
		this.searchWord = searchWord;
	}

	private String[] getSuggestions()
	{
		String[] result = null;
		Corrector corrector = new Corrector();
		Dictionary dictionary = new Dictionary();
		StringBuilder eval = new StringBuilder();
		for (String val : words)
		{
			eval.append(val + " ");
		}
		dictionary.create_dictionary_from_stream(new Scanner(eval.toString()));
		corrector.setDictionary(dictionary);
		try
		{
			ArrayList<String> res = corrector.suggestSimilarWord(searchWord);
			result = new String[res.size()];
			for (int i = 0; i < res.size(); i++)
			{
				result[i] = res.get(i);
			}
		}
		catch (NoSimilarWordFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;

	}

	/**
	 * @return the row
	 */
	public int getRow()
	{
		return row;
	}

	/**
	 * @param row the row to set
	 */
	public void setRow(int row)
	{
		this.row = row;
	}

	/**
	 * @return the column
	 */
	public int getColumn()
	{
		return column;
	}

	/**
	 * @param column the column to set
	 */
	public void setColumn(int column)
	{
		this.column = column;
	}
	
}
