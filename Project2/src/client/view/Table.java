/**
 * 
 */
package client.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.net.URL;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;

import client.model.BatchState;
import client.model.TableModel;

/**
 * @author gabriel_2
 * 
 */
@SuppressWarnings("serial")
public class Table extends JTable
{
	private BatchState state = null;
	private Window view = null;
	private int previousRow = -1;
	private int previousCol = -1;

	public Table(BatchState state_, Window view_)
	{
		state = state_;
		view = view_;
		createTable();
	}

	public void createTable()
	{

		TableModel model = new TableModel(state.getValues(), state.getFields());
		model.addActionListener(state.tableEvent);
		model.addActionListener(highlight);
		//model.addActionListener(correct);
		setModel(model);
		//right justify the record numbers
		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment(JLabel.RIGHT);
		getColumnModel().getColumn(0).setCellRenderer(rightRenderer);

		PopupClicker dialog= new PopupClicker(view, state);
		//set cell editor for each column
		for (int i = 1; i < getColumnModel().getColumnCount(); i++)
		{
			TableColumn column = getColumnModel().getColumn(i);
			TableCellEdit edit = new TableCellEdit(new JTextField());
			TableCellLook look = new TableCellLook(state, view.getCommunicator());
			
			if(state.getCorrectorSet()!=null)
			{
				look.setChecker(state.getCorrectorSet());
				look.setKnownMap(state.getKnownDataMap());
			}
			look.setDialog(dialog);
			//set state to listen to each cell of the table
			edit.addActionListerner(highlight);
			look.addActionListener(highlight);
			edit.addActionListerner(state.tableEvent);
			look.addActionListener(state.tableEvent);

			column.setCellEditor(edit);
			column.setCellRenderer(look);
		}
		//selects only one cell, not the row
		setCellSelectionEnabled(true);

		//set focus on cell of table if there is any
		if (state.getSelectedColumn() != 0)
		{
			setRowSelectionInterval(state.getSelectedRow(), state.getSelectedRow());
			setColumnSelectionInterval(state.getSelectedColumn(), state.getSelectedColumn());
		}

		view.getBleft().setTable(Table.this);

		JScrollPane scrollableTable = new JScrollPane(Table.this);
		scrollableTable.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollableTable.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		view.getBleft().getTabs().setComponentAt(0, scrollableTable);

		//set table to listen to changes in the state
		state.addTableListener(selectCell);
	}

	//table listener
	public ActionListener selectCell = new ActionListener()
	{

		@Override
		public void actionPerformed(ActionEvent e)
		{
			//remove listeners
			for (int i = 1; i < getColumnModel().getColumnCount(); i++)
			{
				TableColumn column = getColumnModel().getColumn(i);
				((TableCellEdit) column.getCellEditor()).removeActionListener(Table.this.state.tableEvent);
				((TableCellLook) column.getCellRenderer()).removeActionListener(Table.this.highlight);
				column.getCellEditor().cancelCellEditing();
			}
			//update row and column that is selected and the highlight
			//text is updated automatically since table model has a pointer to the values stored in the state
			setRowSelectionInterval(state.getSelectedRow(), state.getSelectedRow());
			setColumnSelectionInterval(state.getSelectedColumn(), state.getSelectedColumn());
			highlight.actionPerformed(new ActionEvent(new int[] { state.getSelectedRow(), state.getSelectedColumn() }, 1, "highlight"));
			//editCellFrom(state.getSelectedRow(), state.getSelectedColumn());
			//remove listeners
			for (int i = 1; i < getColumnModel().getColumnCount(); i++)
			{
				TableColumn column = getColumnModel().getColumn(i);
				((TableCellEdit) column.getCellEditor()).addActionListerner(Table.this.state.tableEvent);
				((TableCellLook) column.getCellRenderer()).addActionListener(Table.this.highlight);
			}
		}
	};
	private ActionListener highlight = new ActionListener()
	{

		@Override
		public void actionPerformed(ActionEvent e)
		{
			if (e.getSource() instanceof int[])
			{
				int[] rowColumn = (int[]) e.getSource();

				if (!e.getActionCommand().equals("values update") && (previousRow != rowColumn[0] || previousCol != rowColumn[1]))
				{
					//set column and row selected on state
					state.setSelectedRow(rowColumn[0]);
					state.setSelectedColumn(rowColumn[1]);
					int x = state.getxCoords()[rowColumn[1] - 1];
					int y = state.getFirstYCoord() + state.getHeight() * rowColumn[0];

					view.getBigImage().setHighlight(new Rectangle2D.Double(x, y, state.getWidths()[rowColumn[1] - 1], state.getHeight()));

					//update field help
					String url = (state.getHelpPath()[rowColumn[1] - 1]).replaceAll("\\\\", "/");
					try
					{
						view.getBright().getFhelp()
								.setPage(new URL("http://" + view.getCommunicator().getHost() + ":" + view.getCommunicator().getPort() + "/" + url));
					}
					catch (IOException e1)
					{
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					previousRow = rowColumn[0];
					previousCol = rowColumn[1];
					view.repaint();
				}

			}
		}
	};
}
