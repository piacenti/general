/**
 * 
 */
package client.view;

import interfaces.DrawingListener;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

import javax.swing.JPanel;

import client.controller.MVCutils;
import client.model.BatchState;

/**
 * @author gabriel_2
 * 
 */
@SuppressWarnings("serial")
public class ImagePanel extends JPanel
{
	private Image image = null;
	private Rectangle2D rect = null;
	private Rectangle2D highlight = null;
	private boolean highLenable = false;
	private boolean inverted = false;
	private BatchState state = null;

	private Point mouseDelta = null;
	private int w_deltaX = 0;
	private int w_deltaY = 0;
	private Point prevMouse = null;
	private Point offset = null;

	private double scale;
	private double scaleLimit = 3;
	private int w_centerX;
	private int w_centerY;

	private boolean dragging;
	private int w_dragStartX;
	private int w_dragStartY;

	private ArrayList<DrawingListener> imageSyncListeners = new ArrayList<DrawingListener>();
	private ArrayList<ActionListener> batchStateListeners = new ArrayList<ActionListener>();
	private ArrayList<ActionListener> highlightListener = new ArrayList<ActionListener>();

	/**
	 * @param communicator
	 */
	public ImagePanel()
	{
		setBackground(new Color(100, 100, 100, 255));
		Dimension d = new Dimension(100, 100);
		setPreferredSize(d);

	}

	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponents(g);

		Graphics2D g2 = (Graphics2D) g;
		g2.setColor(getBackground());
		g2.fillRect(0, 0, getWidth(), getHeight());
		if (image != null)
		{
			g2.translate(getWidth() / 2, getHeight() / 2);
			g2.scale(scale, scale);
			g2.translate(-w_centerX - image.getWidth(null) / 2, -w_centerY - image.getHeight(null) / 2);

			g2.drawImage(image, (int) rect.getMinX(), (int) rect.getMinY(), (int) rect.getMaxX(), (int) rect.getMaxY(), 0, 0, image.getWidth(null),
					image.getHeight(null), null);

		}
		if (highlight != null && highLenable == true)
		{
			g2.setColor(new Color(0, 0, 255, 100));
			g2.fill(highlight);
		}
	}

	private MouseAdapter mouseEvent = new MouseAdapter()
	{
		@Override
		public void mousePressed(java.awt.event.MouseEvent e)
		{
			int d_X = e.getX();
			int d_Y = e.getY();

			AffineTransform transform = new AffineTransform();
			transform.translate(getWidth() / 2, getHeight() / 2);
			transform.scale(scale, scale);
			transform.translate(-w_centerX - image.getWidth(null) / 2, -w_centerY - image.getHeight(null) / 2);

			Point2D d_Pt = new Point2D.Double(d_X, d_Y);
			Point2D w_Pt = new Point2D.Double();
			try
			{
				transform.inverseTransform(d_Pt, w_Pt);
			}
			catch (NoninvertibleTransformException ex)
			{
				return;
			}
			int w_X = (int) w_Pt.getX();
			int w_Y = (int) w_Pt.getY();

			boolean hitShape = false;

			if (rect.contains(w_X, w_Y) == true)
				hitShape = true;
			if (hitShape)
			{
				dragging = true;
				w_dragStartX = w_X;
				w_dragStartY = w_Y;

				mouseDelta = new Point(0, 0);
				prevMouse = new Point(w_X, w_Y);
				offset = new Point(0, 0);
			}
		};

		@Override
		public void mouseDragged(java.awt.event.MouseEvent e)
		{
			if (dragging)
			{
				int d_X = e.getX();
				int d_Y = e.getY();

				AffineTransform transform = new AffineTransform();
				transform.translate(getWidth() / 2, getHeight() / 2);
				transform.scale(scale, scale);
				transform.translate(-w_centerX - image.getWidth(null) / 2, -w_centerY - image.getHeight(null) / 2);

				Point2D d_Pt = new Point2D.Double(d_X, d_Y);
				Point2D w_Pt = new Point2D.Double();
				try
				{
					transform.inverseTransform(d_Pt, w_Pt);
				}
				catch (NoninvertibleTransformException ex)
				{
					return;
				}
				int w_X = (int) w_Pt.getX();
				int w_Y = (int) w_Pt.getY();

				//gets the variation in mouse position from last time this event triggered to this time
				mouseDelta = new Point((int) (w_X - prevMouse.getX()), (int) (w_Y - prevMouse.getY()));
				//updates the previous mouse position with current position
				prevMouse = new Point(w_X, w_Y);

				//if the world center is within the bounds or if the mouse is moving towards the bounded area from outside update it accounting for any mouse offset
				boolean changed = false;
				if ((getWidth() / 2 - w_centerX < rect.getWidth() || mouseDelta.getX() < -1)
						&& (w_centerX - getWidth() / 2 < -rect.getWidth() / 4 || mouseDelta.getX() > 1))
				{
					w_deltaX = (int) (w_X - w_dragStartX - offset.getX());
					w_centerX -= w_deltaX;
					changed = true;
				}
				//if the mouse goes out of bounds start tracking the offset so that if mouse starts going in the direction of the bound area, the image starts moving again
				else
				{
					offset.x += mouseDelta.getX();
				}
				//if the world center is within the bounds or if the mouse is moving towards the bounded area from outside update it accounting for any mouse offset
				if ((getHeight() / 2 - w_centerY < rect.getHeight() || mouseDelta.getY() < -1)
						&& (w_centerY - getHeight() / 2 < rect.getHeight() / 4 || mouseDelta.getY() > 1))
				{
					w_deltaY = (int) (w_Y - w_dragStartY - offset.getY());
					w_centerY -= w_deltaY;
					changed = true;
				}
				//if the mouse goes out of bounds start tracking the offset so that if mouse starts going in the direction of the bound area, the image starts moving again
				else
				{
					offset.y += mouseDelta.getY();
				}

				if (changed == true)
				{
					notifyOriginChanged(w_centerX, w_centerY);
					repaint();
				}
			}
		};

		@Override
		public void mouseReleased(java.awt.event.MouseEvent e)
		{
			initDrag();
		};

		public void mouseClicked(java.awt.event.MouseEvent e)
		{
			//highlight cell
			//convert to world
			int d_X = e.getX();
			int d_Y = e.getY();

			AffineTransform transform = new AffineTransform();
			transform.translate(getWidth() / 2, getHeight() / 2);
			transform.scale(scale, scale);
			transform.translate(-w_centerX - image.getWidth(null) / 2, -w_centerY - image.getHeight(null) / 2);

			Point2D d_Pt = new Point2D.Double(d_X, d_Y);
			Point2D w_Pt = new Point2D.Double();
			try
			{
				transform.inverseTransform(d_Pt, w_Pt);
			}
			catch (NoninvertibleTransformException ex)
			{
				return;
			}
			int w_X = (int) w_Pt.getX();
			int w_Y = (int) w_Pt.getY();
			//find the rectangle that contains clicked point
			int[] xCoords = state.getxCoords();
			int height = state.getHeight();
			int[] widths = state.getWidths();
			int firstY = state.getFirstYCoord();
			int numRows = state.getNumRecords();
			int column = 0;
			int row = 0;
			for (int i = 0; i < numRows; i++)
			{
				for (int a = 0; a < xCoords.length; a++)
				{
					//create a rectangle using current x pos 
					Rectangle rect = new Rectangle(xCoords[a], firstY + i * height, widths[a], height);
					if (rect.contains(w_X, w_Y))
					{
						row = i;
						column = a;
						state.setSelectedRow(row);
						state.setSelectedColumn(column + 1);
						MVCutils.notifyListeners(new ActionEvent(new int[] { row, column }, 1, "highlight"), highlightListener);
						break;
					}
				}
			}

		};

		public void mouseWheelMoved(java.awt.event.MouseWheelEvent e)
		{
			double change = e.getPreciseWheelRotation();
			if(scale>scaleLimit)
			{
				scale=scaleLimit;
			}
			else if(scale<1/scaleLimit)
			{
				scale=1/scaleLimit;
			}
			if (scale <= scaleLimit && scale >= 1 / scaleLimit)
			{
				scale += change/100;
				notifyOriginChanged(w_centerX, w_centerY);
				repaint();
			}
		};
	};

	public void notifyOriginChanged(int w_newOriginX, int w_newOriginY)
	{
		for (DrawingListener listener : imageSyncListeners)
		{
			listener.originChanged(w_newOriginX, w_newOriginY, scale, getWidth(), getHeight());
		}
		MVCutils.notifyListeners(new ActionEvent(this, 0, "invert image"), batchStateListeners);
	}

	/**
	 * @param image2
	 */
	public void setImage(Image image2)
	{
		image = image2;
		rect = new Rectangle2D.Double(0, 0, image.getWidth(null), image.getHeight(null));
		w_centerX = 0;
		w_centerY = 0;
		scale = (double) (getHeight()) / (double) (image.getHeight(null));
		initDrag();
		addMouseListener(mouseEvent);
		addMouseMotionListener(mouseEvent);
		addMouseWheelListener(mouseEvent);
		MVCutils.notifyListeners(new ActionEvent(this, 0, "general"), batchStateListeners);
	}

	private void initDrag()
	{
		dragging = false;
		w_dragStartX = 0;
		w_dragStartY = 0;
		offset = new Point(0, 0);
	}

	public void addDrawingListener(DrawingListener listener)
	{
		imageSyncListeners.add(listener);
	}

	public void zoomIn()
	{
		if (scale < scaleLimit)
		{
			scale += 0.1;
			repaint();
			notifyOriginChanged(w_centerX, w_centerY);
		}
	}

	public void zoomOut()
	{
		if (scale > 1 / scaleLimit)
		{

			scale -= 0.1;
			repaint();
			notifyOriginChanged(w_centerX, w_centerY);
		}
	}

	/**
	 * @param w_newOriginX
	 * @param w_newOriginY
	 * @param scale2
	 * @param height
	 * @param width
	 */
	public void setOrigin(int w_newOriginX, int w_newOriginY, double scale2, int width, int height)
	{
		w_centerX = w_newOriginX;
		w_centerY = w_newOriginY;
		MVCutils.notifyListeners(new ActionEvent(this, 0, "general"), batchStateListeners);
		this.repaint();
	}

	/**
	 * @return the scale
	 */
	public double getScale()
	{
		return scale;
	}

	/**
	 * @param scale
	 *            the scale to set
	 */
	public void setScale(double scale)
	{
		this.scale = scale;
		MVCutils.notifyListeners(new ActionEvent(this, 0, "scale"), batchStateListeners);
		repaint();
	}

	/**
	 * @return the w_centerX
	 */
	public int getW_centerX()
	{
		return w_centerX;
	}

	/**
	 * @param w_centerX
	 *            the w_centerX to set
	 */
	public void setW_centerX(int w_centerX)
	{
		this.w_centerX = w_centerX;
		MVCutils.notifyListeners(new ActionEvent(this, 0, "center x"), batchStateListeners);
		repaint();
	}

	/**
	 * @return the w_centerY
	 */
	public int getW_centerY()
	{
		return w_centerY;
	}

	/**
	 * @param w_centerY
	 *            the w_centerY to set
	 */
	public void setW_centerY(int w_centerY)
	{
		this.w_centerY = w_centerY;
		MVCutils.notifyListeners(new ActionEvent(this, 0, "center y"), batchStateListeners);
		repaint();
	}

	/**
	 * @return the image
	 */
	public Image getImage()
	{
		return image;
	}

	/**
	 * @return the highlight
	 */
	public Rectangle2D getHighlight()
	{
		return highlight;
	}

	/**
	 * @param highlight
	 *            the highlight to set
	 */
	public void setHighlight(Rectangle2D highlight)
	{
		this.highlight = new Rectangle2D.Double(highlight.getX(), highlight.getY(), highlight.getWidth(), highlight.getHeight());
		MVCutils.notifyListeners(new ActionEvent(this, 0, "highlighter"), batchStateListeners);
		repaint();
	}

	/**
	 * @return the highLenable
	 */
	public boolean isHighLenable()
	{
		return highLenable;
	}

	/**
	 * @param highLenable
	 *            the highLenable to set
	 */
	public void setHighLenable(boolean highLenable)
	{
		this.highLenable = highLenable;
		MVCutils.notifyListeners(new ActionEvent(this, 0, "highlight toggle"), batchStateListeners);
		repaint();
	}

	/**
	 * @return the inverted
	 */
	public boolean isInverted()
	{
		return inverted;
	}

	/**
	 * @param inverted
	 *            the inverted to set
	 */
	public void setInverted(boolean inverted)
	{
		this.inverted = inverted;
		MVCutils.notifyListeners(new ActionEvent(this, 0, "invert image"), batchStateListeners);
	}

	/**
	 * @param imageEvent
	 */
	public void addActionListener(ActionListener imageEvent)
	{
		batchStateListeners.add(imageEvent);
	}

	/**
	 * @return the highlightListener
	 */
	public ArrayList<ActionListener> getHighlightListener()
	{
		return highlightListener;
	}

	/**
	 * @param highlightListener
	 *            the highlightListener to set
	 */
	public void addHighlightListener(ActionListener highlightListener)
	{
		this.highlightListener.add(highlightListener);
	}

	/**
	 * @return the state
	 */
	public BatchState getState()
	{
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(BatchState state)
	{
		this.state = state;
	}

}
