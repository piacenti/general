/**
 * 
 */
package client.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import client.ClientCommunicator;
import client.ClientException;
import client.controller.MVCutils;
import client.model.BatchState;

/**
 * @author gabriel_2
 * 
 */
@SuppressWarnings("serial")
public class TableCellLook extends DefaultTableCellRenderer
{

	private ArrayList<ActionListener> listeners = new ArrayList<ActionListener>();
	private BatchState state = null;
	private ClientCommunicator communicator = null;
	private HashMap<String, Boolean> checker = new HashMap<String, Boolean>();
	private HashMap<Integer, ArrayList<String>> knownMap = new HashMap<Integer, ArrayList<String>>();
	private PopupClicker dialog = null;
	private int clickCount = 0;
	private Point point = null;

	public TableCellLook(BatchState state_, ClientCommunicator communicator_)
	{
		super();
		state = state_;
		communicator = communicator_;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.swing.table.DefaultTableCellRenderer#getTableCellRendererComponent
	 * (javax.swing.JTable, java.lang.Object, boolean, boolean, int, int)
	 */
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
	{
		JLabel label = new JLabel((String) value);
		label.setOpaque(true);
		label.setBackground(new Color(255, 255, 255, 255));
		if (value != null && ((String) value).equals("") == false)
		{

			if (checker.get((String) value + column) == null)
			{
				//get known data for field and compares to current value
				ArrayList<String> words = getWords(column);
				//check if value is found in list
				boolean result = true;
				if (words.size() > 0)
				{
					result = find(words, (String) value);
				}
				if (result == false)
				{
					//color label red
					label.setBackground(new Color(255, 0, 0, 255));
					//attach mouse listener
					table.addMouseListener(rightClick);
				}

				checker.put((String) value + column, result);
				state.setKnownDataMap(knownMap);
				state.setCorrectorSet(checker);
			}
			else if (checker.get((String) value + column) == false)
			{
				//color label red
				label.setBackground(new Color(255, 0, 0, 255));
				//attach mouse listener
				table.addMouseListener(rightClick);
				table.addMouseMotionListener(rightClick);
			}

		}
		if (isSelected == true)
		{
			MVCutils.notifyListeners(new ActionEvent(new int[] { row, column }, 1, "cell highlight"), listeners);
			label.setBorder(BorderFactory.createBevelBorder(1));
			label.setBackground(new Color(0, 0, 255, 100));
		}
		return label;
	}

	private MouseAdapter rightClick = new MouseAdapter()
	{

		public void mouseClicked(java.awt.event.MouseEvent e)
		{
			if (e.getButton() == MouseEvent.BUTTON3 && clickCount == 0)
			{
				point = e.getLocationOnScreen();
				//check if clicked cell is red and get its text
				//get the row and column that was chosen
				Point point = new Point(e.getX(), e.getY());
				JTable table = (JTable) e.getSource();
				int row = table.rowAtPoint(point);
				int column = table.columnAtPoint(point);
				String value = (String) table.getModel().getValueAt(row, column);

				Color color = table.getCellRenderer(row, column).getTableCellRendererComponent(table, value, false, false, row, column).getBackground();

				Color red = new Color(255, 0, 0);
				if (color.getRed() == red.getRed() && color.getBlue() == red.getBlue() && color.getGreen() == red.getGreen())
				{
					System.out.println(value);
					System.out.println(color);
					//bring up pop up next to the mouse
					//get the known data for the column and create dialog that will suggest corrections

					dialog.setWords(state.getKnownDataMap().get(column));
					dialog.setSearchWord(value);
					dialog.setLocation(e.getLocationOnScreen());
					dialog.setVisible(true);
					dialog.setRow(row);
					dialog.setColumn(column);

					//create dialog with known data
				}
			}
			clickCount = 1;
		};

		public void mouseReleased(MouseEvent e)
		{
			clickCount = 0;
		};

		public void mouseMoved(MouseEvent e)
		{
			//checks how far mouse moved from initial position and make dialog not visible if too far
			if (point != null)
			{
				int x = (int) Math.abs(point.getX() - e.getLocationOnScreen().getX());
				int y = (int) Math.abs(point.getY() - e.getLocationOnScreen().getY());
				if (x > 50 || y > 50)
				{
					dialog.setVisible(false);
					point = null;
				}
			}
		};

	};

	public ArrayList<String> getWords(int column)
	{
		ArrayList<String> result = new ArrayList<String>();
		String url = "http://" + communicator.getHost() + ":" + communicator.getPort() + "/" + state.getKnownData()[column - 1];

		try
		{
			Scanner input = new Scanner(communicator.downloadFile(url));
			input.useDelimiter(",");
			while (input.hasNext())
			{
				result.add(input.next().toLowerCase());
			}
			input.close();
		}
		catch (ClientException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		knownMap.put(column, result);
		return result;

	}

	public boolean find(ArrayList<String> container, String value)
	{
		for (String string : container)
		{
			if (string.equals(value.toLowerCase()))
				return true;
		}
		return false;

	}

	public void addActionListener(ActionListener l)
	{
		listeners.add(l);
	}

	public void removeActionListener(ActionListener l)
	{
		listeners.remove(l);
	}

	/**
	 * @return the state
	 */
	public BatchState getState()
	{
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(BatchState state)
	{
		this.state = state;
	}

	/**
	 * @return the communicator
	 */
	public ClientCommunicator getCommunicator()
	{
		return communicator;
	}

	/**
	 * @param communicator
	 *            the communicator to set
	 */
	public void setCommunicator(ClientCommunicator communicator)
	{
		this.communicator = communicator;
	}

	/**
	 * @return the checker
	 */
	public HashMap<String, Boolean> getChecker()
	{
		return checker;
	}

	/**
	 * @param checker
	 *            the checker to set
	 */
	public void setChecker(HashMap<String, Boolean> checker)
	{
		this.checker = checker;
	}

	/**
	 * @return the dialog
	 */
	public PopupClicker getDialog()
	{
		return dialog;
	}

	/**
	 * @param dialog
	 *            the dialog to set
	 */
	public void setDialog(PopupClicker dialog)
	{
		this.dialog = dialog;
	}

	/**
	 * @return the knownMap
	 */
	public HashMap<Integer, ArrayList<String>> getKnownMap()
	{
		return knownMap;
	}

	/**
	 * @param knownMap
	 *            the knownMap to set
	 */
	public void setKnownMap(HashMap<Integer, ArrayList<String>> knownMap)
	{
		this.knownMap = knownMap;
	}

}
