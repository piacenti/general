package servertester.controllers;

import java.util.*;

import client.ClientCommunicator;
import client.ClientException;
import servertester.views.*;
import shared.communication.BatchGet;
import shared.communication.BatchSubmit;
import shared.communication.FieldGet;
import shared.communication.ProjectGet;
import shared.communication.SampleGet;
import shared.communication.Search;
import shared.communication.Validate;

public class Controller implements IController
{

	private IView _view;

	public Controller()
	{
		return;
	}

	public IView getView()
	{
		return _view;
	}

	public void setView(IView value)
	{
		_view = value;
	}

	// IController methods
	//

	@Override
	public void initialize()
	{
		getView().setHost("localhost");
		getView().setPort("39640");
		operationSelected();
	}

	@Override
	public void operationSelected()
	{
		ArrayList<String> paramNames = new ArrayList<String>();
		paramNames.add("User");
		paramNames.add("Password");

		switch (getView().getOperation())
		{
		case VALIDATE_USER:
			break;
		case GET_PROJECTS:
			break;
		case GET_SAMPLE_IMAGE:
			paramNames.add("Project");
			break;
		case DOWNLOAD_BATCH:
			paramNames.add("Project");
			break;
		case GET_FIELDS:
			paramNames.add("Project");
			break;
		case SUBMIT_BATCH:
			paramNames.add("Batch");
			paramNames.add("Record Values");
			break;
		case SEARCH:
			paramNames.add("Fields");
			paramNames.add("Search Values");
			break;
		default:
			assert false;
			break;
		}

		getView().setRequest("");
		getView().setResponse("");
		getView().setParameterNames(paramNames.toArray(new String[paramNames.size()]));
	}

	@Override
	public void executeOperation()
	{
		switch (getView().getOperation())
		{
		case VALIDATE_USER:
			validateUser();
			break;
		case GET_PROJECTS:
			getProjects();
			break;
		case GET_SAMPLE_IMAGE:
			getSampleImage();
			break;
		case DOWNLOAD_BATCH:
			downloadBatch();
			break;
		case GET_FIELDS:
			getFields();
			break;
		case SUBMIT_BATCH:
			submitBatch();
			break;
		case SEARCH:
			search();
			break;
		default:
			assert false;
			break;
		}
	}

	private void validateUser()
	{
		
	}

	private void getProjects()
	{
		
	}

	private void getSampleImage()
	{
		String username = getView().getParameterValues()[0];
		String password = getView().getParameterValues()[1];
		int project = Integer.parseInt(getView().getParameterValues()[2].trim());
		ClientCommunicator communicator = new ClientCommunicator(_view.getHost(), _view.getPort());
		try
		{
			_view.setResponse(communicator.getSampleImage(new SampleGet(username, password, project)));
		}
		catch (ClientException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void downloadBatch()
	{
		
	}

	private void getFields()
	{
		
	}

	private void submitBatch()
	{
		String username = getView().getParameterValues()[0];
		String password = getView().getParameterValues()[1];
		int project = Integer.parseInt(getView().getParameterValues()[2].trim());
		String values = getView().getParameterValues()[3];

		ClientCommunicator communicator = new ClientCommunicator(_view.getHost(), _view.getPort());
		try
		{
			_view.setResponse(communicator.submitBatch(new BatchSubmit(username, password, project, values)));
		}
		catch (ClientException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void search()
	{
		
	}

}
