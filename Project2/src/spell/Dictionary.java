package spell;

import java.util.Scanner;

public class Dictionary implements Trie
{
	private Word dictionary_words = new Word();
	private int word_count = 0;
	private int node_count = 1;
	private int alphabet_size = 26;
	private StringBuilder words = new StringBuilder();
	private StringBuilder all_words = new StringBuilder();
	private int a_char_index=(int) 'a';

	public Dictionary()
	{

	}
	public void create_dictionary_from_stream(Scanner input)
	{
		
		String word;
		while(input.hasNext()==true)
		{
			word = input.next();
			//System.out.println(word);
			this.add(word.toLowerCase());
		}
		//System.out.println(this.toString()+" nodes: "+this.getNodeCount()+" words: "+this.getWordCount()+" find apple: "+this.find("apple"));
		
	}

	/**
	 * Adds the specified word to the trie (if necessary) and increments the
	 * word's frequency count
	 * 
	 * @param word
	 *            The word being added to the trie
	 */
	@Override
	public void add(String word)
	{
		word = word.toLowerCase();
		Word temp = dictionary_words;
		for (int i = 0; i < word.length(); i++)
		{
			int index = (int) word.charAt(i) - a_char_index;
			//System.out.println(index);
			if (temp.letters[index] == null)
			{
				temp.letters[index] = new Word();
				node_count++;
			}
			temp = temp.letters[index];
		//	System.out.println(index);
		}
		temp.frequency++;
		word_count++;
	}

	/**
	 * Searches the trie for the specified word
	 * 
	 * @param word
	 *            The word being searched for
	 * 
	 * @return A reference to the trie node that represents the word, or null if
	 *         the word is not in the trie
	 */
	@Override
	public Node find(String word)
	{
		word = word.toLowerCase();
		Word temp = dictionary_words;
		for (int i = 0; i < word.length(); i++)
		{
			int index = (int) word.charAt(i) - a_char_index;
			if (temp.letters[index] != null)
			{
				temp = temp.letters[index];
			}
			else
			{
				return null;
			}

		}
		if(temp.frequency>0)
			return temp;
		else
			return null;
	}

	/**
	 * Returns the number of unique words in the trie
	 * 
	 * @return The number of unique words in the trie
	 */
	@Override
	public int getWordCount()
	{
		return word_count;
	}

	/**
	 * Returns the number of nodes in the trie
	 * 
	 * @return The number of nodes in the trie
	 */
	@Override
	public int getNodeCount()
	{
		return node_count;
	}

	/**
	 * The toString specification is as follows: For each word, in alphabetical
	 * order: <word> <count>\n
	 */
	@Override
	public String toString()
	{
		all_words = new StringBuilder();
		toString(dictionary_words);
		return all_words.toString();
	}

	private void toString(Word node)
	{
		if (node.frequency > 0)
		{
			all_words.append(words + " " + node.frequency + "\n");
		}

		for (int i = 0; i < alphabet_size; i++)
		{

			if (node.letters[i] != null)
			{

				words.append((char) (i + a_char_index));
				toString(node.letters[i]);
			}

		}
		if (words.length() > 0)
			words.deleteCharAt(words.length() - 1);

	}

	@Override
	public int hashCode()
	{
		return (word_count+words.toString().hashCode()+dictionary_words.hashCode()+node_count+all_words.toString().hashCode());
	}

	@Override
	public boolean equals(Object o)
	{
		if(this==o)
		{
			return true;
		}
		if(o==null)
		{
			return false;
		}
		if(this.getClass() !=  o.getClass())
		{
			return false;
		}
		Dictionary other = (Dictionary) o;
		return (word_count==other.word_count && words.toString().equals(other.words.toString()) && node_count == other.node_count &&  all_words.toString().equals(other.all_words.toString()) && dictionary_words.equals(other.dictionary_words));

	}

	public class Word implements Node
	{
		private int frequency = 0;
		private Word letters[] = new Word[26];

		/**
		 * Returns the frequency count for the word represented by the node
		 * 
		 * @return The frequency count for the word represented by the node
		 */
		public int getValue()
		{
			return frequency;
			
		}
		public int hashCode()
		{
			int counter=0;
			for(int i=0;i<letters.length;i++)
			{
				if(letters[i]!=null)
				{
					counter = counter+(i*3);
				}
			}
			return (counter+frequency);
		}
		public boolean equals(Object o)
		{
			if(this==o)
			{
				return true;
			}
			if(o==null)
			{
				return false;
			}
			if(this.getClass() !=  o.getClass())
			{
				return false;
			}
			Word other = (Word) o;
			return (frequency == other.frequency && equalNodes(this, other));
			
		}
		private boolean equalNodes(Word self, Word other)
		{
			boolean test = true;
			for(int i=0;i<alphabet_size;i++)
			{
				if(self.letters[i]!=null && other.letters[i]!=null)
				{
					self = self.letters[i];
					other = other.letters[i];
					test = equalNodes(self, other);
				}
				else if(self.letters[i]==null && other.letters[i]==null)
				{
					//do nothing
				}
				else
				{
					test = false;
				}
			}
			return test; //if at any of the levels it returns a false then return false along the chain
		}
	}
}
