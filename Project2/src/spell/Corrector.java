package spell;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.SortedSet;
import java.util.TreeSet;

import spell.Dictionary.Word;

public class Corrector implements SpellCorrector
{
	private Dictionary dictionary = new Dictionary();
	private SortedSet<String> possible_words = new TreeSet<>();
	private ArrayList<String> match = new ArrayList<>();
	private String input = "";
	private int a_char_index = (int) 'a';

	public Corrector()
	{

	}

	public Dictionary getDictionary()
	{
		return dictionary;
	}

	public void setDictionary(Dictionary dictionary)
	{
		this.dictionary = dictionary;
	}

	private void runCorrector()
	{
		// System.out.println(input);
		if (dictionary.find(input) == null)
		{
			// System.out.println(input);
			for (int i = 0; i < 2 && findInSet() == false; i++)
			{
				SortedSet<String> words = new TreeSet<>();
				words.addAll(possible_words);
				deletion_distance(words);
				transposition_distance(words);
				alteration_distance(words);
				insertion_distance(words);
			}
		}
		matchWord();

	}

	private void matchWord()
	{
		int highest_frequency = 0;

		for (String word : possible_words)
		{
			Word test = (Word) dictionary.find(word);
			if (test != null)
			{
				match.add(word);
			}
		}
	}

	private boolean findInSet()
	{
		for (String word : possible_words)
		{
			if (dictionary.find(word) != null)
			{
				return true;
			}
		}
		return false;

	}

	private void deletion_distance(SortedSet<String> words)
	{
		for (String word : words)
		{
			for (int i = 0; i < word.length(); i++)
			{
				String new_set_word = "";
				new_set_word += word.substring(0, i);
				new_set_word += word.substring(i + 1, word.length());
				possible_words.add(new_set_word);

				// System.out.println(new_set_word);
			}
		}

	}

	private void transposition_distance(SortedSet<String> words)
	{
		for (String word : words)
		{
			for (int i = 0; i < word.length() - 1; i++)
			{
				String new_set_word = "";
				new_set_word += word.substring(0, i);
				new_set_word += word.substring(i + 1, i + 2);
				new_set_word += word.substring(i, i + 1);
				new_set_word += word.substring(i + 2);
				possible_words.add(new_set_word);

				// System.out.println(new_set_word);
			}
		}
	}

	private void alteration_distance(SortedSet<String> words)
	{
		int alphabet_size = 26;
		for (String word : words)
		{
			for (int i = 0; i < word.length(); i++)
			{
				for (int a = 0; a < alphabet_size; a++)
				{
					String new_set_word = "";
					new_set_word += word.substring(0, i);
					new_set_word += (char) (a + a_char_index);
					new_set_word += word.substring(i + 1);
					possible_words.add(new_set_word);
					// System.out.println(new_set_word);
				}

			}
		}
	}

	private void insertion_distance(SortedSet<String> words)
	{
		int alphabet_size = 26;
		for (String word : words)
		{
			for (int i = 0; i <= word.length(); i++)
			{
				for (int a = 0; a < alphabet_size; a++)
				{
					String new_set_word = "";
					new_set_word += word.substring(0, i);
					new_set_word += (char) (a + a_char_index);
					new_set_word += word.substring(i);
					possible_words.add(new_set_word);
					// System.out.println(new_set_word);
				}
			}
		}
	}

	/**
	 * Tells this <code>SpellCorrector</code> to use the given file as its
	 * dictionary for generating suggestions.
	 * 
	 * @param dictionaryFileName
	 *            File containing the words to be used
	 * @throws IOException
	 *             If the file cannot be read
	 */
	@Override
	public void useDictionary(String dictionaryFileName) throws IOException
	{
		Scanner input = new Scanner(Paths.get(dictionaryFileName));
		input.useDelimiter("\\s+");
		dictionary.create_dictionary_from_stream(input);
		input.close();
	}

	/**
	 * Suggest a word from the dictionary that most closely matches
	 * <code>inputWord</code>
	 * 
	 * @param inputWord
	 * @return The suggestion
	 * @throws NoSimilarWordFoundException
	 *             If no similar word is in the dictionary
	 */
	@Override
	public ArrayList<String> suggestSimilarWord(String inputWord) throws NoSimilarWordFoundException
	{
		possible_words = new TreeSet<>();
		possible_words.add(inputWord);
		input = inputWord;
		runCorrector();

		if (match != null)
			return match;
		else
			throw new NoSimilarWordFoundException();

	}

}
