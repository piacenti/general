/**
 * 
 */
package client.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Scanner;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import spell.Corrector;
import spell.SpellCorrector.NoSimilarWordFoundException;
import client.ClientCommunicator;
import client.ClientException;
import client.controller.MVCutils;
import client.model.BatchState;

/**
 * @author gabriel_2
 * 
 */
@SuppressWarnings("serial")
public class TableCellLook extends DefaultTableCellRenderer
{

	private ArrayList<ActionListener> listeners = new ArrayList<ActionListener>();
	private BatchState state = null;
	private ClientCommunicator communicator = null;
	private HashMap<String, ArrayList<String>> result = new HashMap<>();

	public TableCellLook(BatchState state_, ClientCommunicator communicator_)
	{
		super();
		state = state_;
		communicator = communicator_;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.swing.table.DefaultTableCellRenderer#getTableCellRendererComponent
	 * (javax.swing.JTable, java.lang.Object, boolean, boolean, int, int)
	 */
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
	{

		if ((value != null && value.equals("") == false) && result.get(row + "," + column) == null)
		{
			//check if the value is a new value and if not equal check it

			Corrector corrector = null;
			spell.Dictionary dictionary = new spell.Dictionary();
			Scanner input = null;
			try
			{
				String url = "http://" + communicator.getHost() + ":" + communicator.getPort() + "/" + state.getKnownData()[column - 1];

				input = new Scanner(communicator.downloadFile(url));
				input.useDelimiter("\\,");
				dictionary.create_dictionary_from_stream(input);
				corrector = new Corrector();
				corrector.setDictionary(dictionary);
				result.put(row + "," + column, corrector.suggestSimilarWord(state.getValue(row, column - 1).toLowerCase()));
				//if invalid change background to red
				if (result.get(row + "," + column) == null || result.get(row + "," + column).size() == 0 || result.get(row + "," + column).get(0).equals(state.getValue(row, column - 1)) == false)
				{
					setBackground(new Color(255, 0, 0, 255));

					//create the corrector dialog populated with the results from check
					//corrector= new Corrector(result);
				}
				else
				{
					result.remove(row + "," + column);
				}

			}

			catch (ClientException | NoSimilarWordFoundException err)
			{
				// TODO Auto-generated catch block
				err.printStackTrace();
			}
		}
		
		JLabel label=new JLabel((String) value);
		label.setOpaque(true);
		if(result.get(row + "," + column) != null)
		{
			label.setBackground(new Color(255, 0, 0, 255));
		}
		else
		{
			label.setBackground(new Color(255, 255, 255));
		}
		if (isSelected == true)
		{
			MVCutils.notifyListeners(new ActionEvent(new int[] { row, column }, 1, "cell highlight"), listeners);
		}
		return label;
	}

	public void addActionListener(ActionListener l)
	{
		listeners.add(l);
	}

	public void removeActionListener(ActionListener l)
	{
		listeners.remove(l);
	}

	/**
	 * @return the state
	 */
	public BatchState getState()
	{
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(BatchState state)
	{
		this.state = state;
	}

	/**
	 * @return the communicator
	 */
	public ClientCommunicator getCommunicator()
	{
		return communicator;
	}

	/**
	 * @param communicator
	 *            the communicator to set
	 */
	public void setCommunicator(ClientCommunicator communicator)
	{
		this.communicator = communicator;
	}

	/**
	 * @return the result
	 */
	public HashMap<String, ArrayList<String>> getResult()
	{
		return result;
	}

	/**
	 * @param result the result to set
	 */
	public void setResult(HashMap<String, ArrayList<String>> result)
	{
		this.result = result;
	}

}
