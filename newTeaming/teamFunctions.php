<?php
require ('../../includes/includeMeBlank.php');
require ('employeeOperations.php');
class TeamDatabaseOperations
{
	private $empOp;
	public function __construct()
	{
		$this -> empOp = new employeeOperations();
	}

	public function teamsData()
	{
		global $area;
		$query = "SELECT * FROM teams WHERE area='".$area."' ORDER BY name";
		$results = mysql_query($query) or die("Error".mysql_error());
		$row = 0;
		$result = array();
		while ($cur = mysql_fetch_array($results))
		{
			$result[$row]["id"] = $cur['ID'];
			$result[$row]["teamName"] = $cur['name'];
			$result[$row]["leader"] = $this -> empOp -> getNameByNetId($cur['lead']);
			$row++;
		}
		echo json_encode($result);
	}

	public function leaderArray()
	{
		global $area;
		$perm = mysql_query("SELECT * FROM `permissionArea` JOIN `permission` ON `permissionArea`.`permissionId` = `permission`.`permissionId` WHERE area='".$area."' AND shortName ='teamLeader'");
		$perm = mysql_fetch_array($perm);
		$query = "SELECT * FROM employeePermissions where permission='".$perm['index']."' ORDER BY `netID` ASC";
		$result = mysql_query($query) or die("ERROR".mysql_error());
		$return = array();
		$row = 0;
		while ($data = mysql_fetch_array($result))
		{
			$return[$row]["name"] = $this -> empOp -> getNameByNetId($data['netID']);
			$return[$row]["netId"] = $data['netID'];
			$row++;
		}
		echo json_encode($return);
	}

	private function insertTeams($data)
	{
		global $area;
		global $sql;
		$values = "";
		foreach ($data as $key => $x)
		{
			$netId = $this -> empOp -> getNetIdByName($x -> leader);
			$tuple = "(";
			$tuple .= "'".$x -> teamName."',";
			$tuple .= "'".$netId."',";
			$tuple .= $area;
			$tuple .= ")";
			if ($key < sizeof($data) - 1)
			{
				$tuple .= ",";
			}
			$values .= $tuple;
		}
		$query = "INSERT INTO teams (name, lead, area) VALUES $values";
		$result = mysql_query($query) or die("Error in query: $sql. ".mysql_error());
		return $result;
	}

	private function deleteTeams($data)
	{
		global $area;
		$data = implode(",", $data);
		$query = "DELETE FROM teams WHERE ID IN($data) AND area=$area";
		$result = mysql_query($query) or die("Error in query: $sql. ".mysql_error());
		return $result;
	}

	private function updateTeams($data)
	{
		global $area;
		global $sql;
		global $connection;
		$values = "";
		foreach ($data as $key => $x)
		{
			$netId = $this -> empOp -> getNetIdByName($x -> leader);
			$tuple = "(";
			$tuple .= $x -> id.",";
			$tuple .= "'".$x -> teamName."',";
			$tuple .= "'".$netId."'";
			$tuple .= ")";
			if ($key < sizeof($data) - 1)
			{
				$tuple .= ",";
			}
			$values .= $tuple;
		}
		$query = "INSERT INTO teams (ID, name, lead) VALUES $values ON DUPLICATE KEY UPDATE name=VALUES(name), lead=VALUES(lead)";

		$result = mysql_query($query) or die("Error in query: $sql. ".mysql_error());
		return $result;

	}

	public function submitChanges($data)
	{
		$data = json_decode($data);
		$result = true;
		if (sizeof($data -> insert) > 0)
		{
			$result = $result && $this -> insertTeams($data -> insert);
		}
		if (sizeof($data -> delete) > 0)
		{
			$result = $result && $this -> deleteTeams($data -> delete);
		}
		if (sizeof($data -> update) > 0)
		{
			$result = $result && $this -> updateTeams($data -> update);
		}
		if ($result == false)
		{
			echo mysql_error();
		}
		else
		{
			echo $result;
		}
	}

}

$request = "";
$data = "";
if (isset($_GET["function"]))
{
	$request = $_GET["function"];
}
else
if (isset($_POST["function"]))
{
	$request = $_POST["function"];

}
if (isset($_GET["data"]))
{
	$data = $_GET["data"];
}
else
if (isset($_POST["data"]))
{
	$data = $_POST["data"];
}
$operation = new TeamDatabaseOperations();
switch($request)
{
	case "teamsData" :
		$operation -> teamsData();
		break;
	case "leaderArray" :
		$operation -> leaderArray();
		break;
	case "submitChanges" :
		$operation -> submitChanges($data);
		break;
}
?>