function teamTableOperations(containerId, listId)
{
	this.container=containerId;
	this.list= this.createSelect(listId);
	this.teamsTableModel="";
	this.leaderListData = "";
	this.selectedLeader = "";
	this.deletedDatabaseRows=new Array();
	this.ajaxResult={success:1, fail:0, error:false, errorMessage:""};
	this.modifiedDatabaseRow=new Array();
};
teamTableOperations.prototype.initialize = function()
{
	//get reference to this object
	var self = this;
	//load data
	$.post("teamFunctions.php",
	{
	function : "teamsData"
	}, function(data)
	{
		$.post("teamFunctions.php",
		{
		function : "leaderArray"
		}, function(data2)
		{
			self.deletedDatabaseRows=new Array();
			self.modifiedDatabaseRow=new Array();
			self.teamsTableModel = JSON.parse(data);
			self.leaderListData = JSON.parse(data2);
			
			//create table
			self.createTable();
		});
		
	});
}
teamTableOperations.prototype.createSelect=function(containerId)
{
	$("#"+containerId).html("<select id='newTeaming.leaderList'></select>");
	return "newTeaming\\.leaderList";
}
teamTableOperations.prototype.insertRows = function()
{
	var selectedLeader="";
	if(this.selectedLeader!="")
	{
		selectedLeader=this.selectedLeader;
	}
	else
	{
		//get first value in the list of leaders
		selectedLeader=this.leaderListData[0]['name'];
	}	

	this.teamsTableModel.push({"teamName":"", "leader":selectedLeader});
	this.createTable();
	this.updateDeleteList();
}

teamTableOperations.prototype.createTable = function()
{
	
	//reference
	var self = this;
	//create table with loaded data if there is any
	var table = "<table id='teamLeadersTable' class='sortable centered'><tr><th class='sorttable_numeric'>Name</th><th>Leader</th></tr>";
	if(this.teamsTableModel!="teamsTableModel")
	{
		var selectList = "";
		var teams=this.teamsTableModel;
		var rows=0;
		for (row in teams)
		{
			var leader=teams[row]['leader'];
			var teamName=teams[row]['teamName'];
			selectList = "<select name='selectLeaders' id='selectLeaders" + rows + "'>" + this.leaderSelect(leader) + "</select>";
			table += "<tr><td><input type='text' id='teamName"+rows+"' value='"+teamName+"' /></td><td>" + selectList + "</td></tr>";
			rows++;
		}
		table += "</table>";
		$("#"+this.container).html(table);
		
		//add listeners to selections to update current selected
	
		for (var i = 0; i < rows; i++)
		{
			
			$("#selectLeaders" + i).change(function()
			{
				var index=($(this).attr("id"));
				index=index.match(/\d+/g);
				self.selectedLeader = $(this).find("option:selected").text();
				self.teamsTableModel[index]["leader"] = $(this).find("option:selected").text();
			});
			$("#teamName" + i).change(function()
			{
				var index=($(this).attr("id"));
				index=index.match(/\d+/g);
				self.teamsTableModel[index]["teamName"] = $(this).val();
				if(self.teamsTableModel[index]["id"]!=undefined)
				{
					self.modifiedDatabaseRow.push(self.teamsTableModel[index]);
				}
				self.updateDeleteList();
			});
		}

	}
	//else create empty table
	else
	{
		table += "</table>";
		$("#"+this.container).html(table);
	}
	this.updateDeleteList();
	
}


teamTableOperations.prototype.leaderSelect = function(leader)
{
	//parse data
	var json = this.leaderListData;
	var list = "";
	for (row in json)
	{
		if (leader == json[row]['name'] || (leader=="" && this.selectedLeader==json[row]['name']))
		{
			list += "<option  value='" + json[row]['netId'] + "' selected>" + json[row]['name'] + "</option>";
		}
		else
		{
			list += "<option value='" + json[row]['netId'] + "' >" + json[row]['name'] + "</option>";
		}
	}
	return list;
}
teamTableOperations.prototype.updateDeleteList=function()
{
	var list =$("#"+this.list);
	//create options based on what is on the table model
	var teams=this.teamsTableModel;
	var options="";
	var index=0;
	for(row in teams)
	{
		options+="<option value='"+index+"' >" + teams[row]['teamName'] + "</option>";
		index++;
	}
	list.html(options);
	
}
teamTableOperations.prototype.isNamesValid=function()
{
	
	//go over table model and determine if they are all different than ""
	for (row in this.teamsTableModel)
	{
		var name = this.teamsTableModel[row]["teamName"];
		if (name == "")
		{
			alert("Please make sure that all teams have a name");
			return false;
		}
	}
	return true;
}

teamTableOperations.prototype.submitChanges = function()
{
	
	//validate table to make sure there are no fields without names
	if (this.isNamesValid() == true)
	{
		
		//organize teams to add or udpate, if they have an "id" attribute then they came from database and must be updated, else they are new and must be added
		var toAdd = new Array();
		var toUpdate= new Array();
		for(row in this.teamsTableModel)
		{
			var hasId=this.teamsTableModel[row]["id"];
			if(hasId==undefined)
			{
				toAdd.push(this.teamsTableModel[row])
			}
		}
		//items to delete from database
		this.deleteTeams(this.deletedDatabaseRows);
		//insert
		this.insertTeams(toAdd);
		//update
		this.updateTeams(this.modifiedDatabaseRow);
		if(this.ajaxResult.error==true)
		{
			alert();
			this.ajaxResult.error=false;
		}
		else
		{
			this.initialize();
		}
		
	}

}
teamTableOperations.prototype.insertTeams = function(json)
{
	console.log("insert",json);
	var self=this;
	if(Object.keys(json).length!=0)
	{
		$.post("teamFunctions.php", {"function":"insertTeams", "data":JSON.stringify(json)}, function(result)
		{
			self.checkAjaxResult(result);
		}); 
	}

}
teamTableOperations.prototype.deleteTeams = function(indexes)
{
	console.log("delete",indexes);
	var self=this;
	if(Object.keys(indexes).length!=0)
	{
		$.post("teamFunctions.php", {"function":"deleteTeams", "data":JSON.stringify(indexes)}, function(result)
		{
			self.checkAjaxResult(result);
		}); 
	}
}
teamTableOperations.prototype.updateTeams = function(json)
{
	console.log("update",json);
	var self=this;
	if(Object.keys(json).length!=0)
	{
		$.post("teamFunctions.php", {"function":"updateTeams", "data":JSON.stringify(json)}, function(result)
		{
			self.checkAjaxResult(result);
		}); 
	}
}
teamTableOperations.prototype.checkAjaxResult=function(result)
{
	if(this.ajaxResult.success!=result)
	{
		this.ajaxResult.error=true;
		this.ajaxResult.errorMessage=result+"\n";
	}
}
teamTableOperations.prototype.deleteRow = function()
{
	//get index of currently selected row to delete
	var index=$("#"+this.list+" option:selected").val();
	//if the row came from database add to list of rows to remove
	if(this.teamsTableModel[index]["id"]!=undefined)
	{
		this.deletedDatabaseRows.push(this.teamsTableModel[index]["id"]);
	}
	//delete from model
	this.teamsTableModel.splice(index,1);
	//recreate table
	this.createTable();
}
