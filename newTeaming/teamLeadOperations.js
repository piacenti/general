function teamLeadTable(containerId, listId)
{
	this.container=containerId;
	this.list= this.createSelect(listId);
	this.teamsTableModel="";
	this.leaderListData = "";
	this.selectedLeader = "";
	this.deletedDatabaseRows;
	this.ajaxResult =
	{
		success : 1
	}; 
	this.modifiedDatabaseRow;
};
teamLeadTable.prototype.initialize = function()
{
	//get reference to this object
	var self = this;
	//load data
	$.post("teamFunctions.php",
	{
		function : "teamsData"
	}, 
	function(teams)
	{
		$.post("teamFunctions.php",
		{
			function : "leaderArray"
		}, 
		function(leaders)
		{
			self.deletedDatabaseRows=new Array();
			self.modifiedDatabaseRow=new Array();
			self.teamsTableModel = JSON.parse(teams);
			self.leaderListData = JSON.parse(leaders);
			
			//create table
			self.createTable();
		});
		
	});
}
teamLeadTable.prototype.createSelect=function(containerId)
{
	$("#"+containerId).html("<select id='newTeaming.leaderList'></select>");
	return "newTeaming\\.leaderList";
}
teamLeadTable.prototype.insertTableRows = function()
{
	var selectedLeader="";
	if(this.selectedLeader!="")
	{
		selectedLeader=this.selectedLeader;
	}
	else
	{
		//get first value in the list of leaders
		selectedLeader=this.leaderListData[0]['name'];
	}	

	this.teamsTableModel.push({"teamName":"", "leader":selectedLeader});
	this.createTable();
	this.updateDeleteList();
}

teamLeadTable.prototype.createTable = function()
{
	
	//reference
	var self = this;
	//create table with loaded data if there is any
	var table = "<table id='teamLeadersTable' class='sortable centered'><tr><th class='sorttable_numeric'>Name</th><th>Leader</th></tr>";
	if(this.teamsTableModel!="teamsTableModel")
	{
		var selectList = "";
		var teams=this.teamsTableModel;
		var rows=0;
		for (row in teams)
		{
			var leader=teams[row]['leader'];
			var teamName=teams[row]['teamName'];
			selectList = "<select name='selectLeaders' id='selectLeaders" + rows + "'>" + this.leaderSelect(leader) + "</select>";
			table += "<tr><td><input type='text' id='teamName"+rows+"' value='"+teamName+"' /></td><td>" + selectList + "</td></tr>";
			rows++;
		}
		table += "</table>";
		$("#"+this.container).html(table);
		
		//add listeners to selections to update current selected
	
		for (var i = 0; i < rows; i++)
		{
			
			$("#selectLeaders" + i).change(function()
			{
				var index=($(this).attr("id"));
				index=index.match(/\d+/g);
				self.selectedLeader = $(this).find("option:selected").text();
				self.teamsTableModel[index]["leader"] = $(this).find("option:selected").text();
			});
			$("#teamName" + i).change(function()
			{
				var index=($(this).attr("id"));
				index=index.match(/\d+/g);
				self.teamsTableModel[index]["teamName"] = $(this).val();
				if(self.teamsTableModel[index]["id"]!=undefined)
				{
					self.modifiedDatabaseRow.push(self.teamsTableModel[index]);
				}
				self.updateDeleteList();
			});
		}

	}
	//else create empty table
	else
	{
		table += "</table>";
		$("#"+this.container).html(table);
	}
	this.updateDeleteList();
	
}


teamLeadTable.prototype.leaderSelect = function(leader)
{
	//parse data
	var json = this.leaderListData;
	var list = "";
	for (row in json)
	{
		if (leader == json[row]['name'] || (leader=="" && this.selectedLeader==json[row]['name']))
		{
			list += "<option  value='" + json[row]['netId'] + "' selected>" + json[row]['name'] + "</option>";
		}
		else
		{
			list += "<option value='" + json[row]['netId'] + "' >" + json[row]['name'] + "</option>";
		}
	}
	return list;
}
teamLeadTable.prototype.updateDeleteList=function()
{
	var list =$("#"+this.list);
	//create options based on what is on the table model
	var teams=this.teamsTableModel;
	var options="";
	var index=0;
	for(row in teams)
	{
		options+="<option value='"+index+"' >" + teams[row]['teamName'] + "</option>";
		index++;
	}
	list.html(options);
	
}
teamLeadTable.prototype.isNamesValid=function()
{
	
	//go over table model and determine if they are all different than ""
	for (row in this.teamsTableModel)
	{
		var name = this.teamsTableModel[row]["teamName"];
		if (name == "")
		{
			alert("Please make sure that all teams have a name");
			return false;
		}
	}
	return true;
}

teamLeadTable.prototype.submitChanges = function()
{
	
	//validate table to make sure there are no fields without names
	if (this.isNamesValid() == true)
	{
		
		//organize teams to add or udpate, if they have an "id" attribute then they came from database and must be updated, else they are new and must be added
		var toAdd = new Array();
		var toUpdate= new Array();
		for(row in this.teamsTableModel)
		{
			var hasId=this.teamsTableModel[row]["id"];
			if(hasId==undefined)
			{
				toAdd.push(this.teamsTableModel[row])
			}
		}
		var changes = {delete:this.deletedDatabaseRows, insert:toAdd, update:this.modifiedDatabaseRow};
		
		console.log(changes);
		var self=this;
		$.post("teamFunctions.php", {"function":"submitChanges", "data":JSON.stringify(changes)}, function(result)
		{
			if(self.ajaxResult.success!=result)
			{
				alert(result+"\n");
			}
		}); 
		
	}

}

teamLeadTable.prototype.deleteTableRow = function()
{
	//get index of currently selected row to delete
	var index=$("#"+this.list+" option:selected").val();
	//if the row came from database add to list of rows to remove
	if(this.teamsTableModel[index]["id"]!=undefined)
	{
		this.deletedDatabaseRows.push(this.teamsTableModel[index]["id"]);
	}
	//delete from model
	this.teamsTableModel.splice(index,1);
	//recreate table
	this.createTable();
}
