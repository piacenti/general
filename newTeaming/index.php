<?php //teamLeaders.php
//application for displaying and editing teams and their leaders
require ('../../includes/includeme.php');
require ('../teamingFunctions.php');

//check permission
checkPermissionRedirect('editTeams');
?>
<link rel="styleSheet" type="text/css" href="css.css"/>
<script language="javascript" type="text/javascript" src="teamLeadOperations.js"></script>
<script language="javascript" type="text/javascript" src="handlers.js"></script>

<h1 class="centered"><?php
if ($area == 3)
{
	echo 'Training Team';
}
else
{
	echo 'Team ';
}
?>
Leaders</h1>

<h4 class="centered">INSTRUCTIONS: After editing a team name or changing a team leader
<br/>
click "Submit Changes" BEFORE doing anything else or your changes will not be saved. </h4>

<div class="centered">
	<button id="insertTeam" class="centered">
		Insert New Team
	</button>
	<button id="submitChange" class="centered">
		Submit Changes
	</button>
</div>
<div id='teamTable'></div>
<div class="centered">
	<button id="deleteTeams" class="centered">
		Delete Team
	</button>
	<span id='deleteSelectTeam' class="centered"></span>
</div>

<?php
require ('../../includes/includeAtEnd.php');
?>
