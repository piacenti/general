window.onload = function()
{
	viewTable("incidentList");
	updateTable("update");
	viewTable("incidentList");
	applyDatePicker("startDate");
	applyDatePicker("endDate");
	$(':button').button().tooltip();
	$('.bubbleInfo').codabubble(
	{
		direction : "down",
		offset : 0
	});
}
const done = 1;
const notDone = 0;


function applyDataTables(id)
{
	$('#' + id).dataTable(
	{
		"sDom" : 'R<"H"lfr>t<"F"ip>',
		"bJQueryUI" : true,
		"sPaginationType" : "full_numbers",
		"bFilter" : false,
		"bDestroy" : true,
		"bRetrieve" : true
	});

}

function applyDatePicker(id)
{
	if (id != "datepicker")
	{
		$('#' + id).datepicker(
		{
			dateFormat : "yy-mm-dd",
			showOn : 'button',
			buttonImage : '../../includes/libs/img/cal.gif',
			buttonImageOnly : true
		});
	}
	else
	{
		$('.' + id).datepicker(
		{
			dateFormat : "yy-mm-dd",
			showOn : 'button',
			buttonImage : '../../includes/libs/img/cal.gif',
			buttonImageOnly : true
		});
	}
}

function updateTable(action, id)
{
	if (action == "submit")
	{
		var submitInfo = new Object;
		submitInfo.userMessage = $("#userMessage").val();
		//console.log(submitInfo.userMessage);
		submitInfo.engineersDo = $("#engineersDo").val();
		submitInfo.opsDo = $("#opsDo").val();
		submitInfo.discoveredBy = $("#discoveredBy").val();
		submitInfo.rootCause = $("#rootCause").val();
		submitInfo.resolution = $("#resolution").val();
		submitInfo.prevention = $("#prevention").val();
		submitInfo.improvement = $("#improvement").val();

		submitInfo.description = $("#description").val();
		submitInfo.dateCreated = $("#dateCreated").val();
		submitInfo.startTime = $("#ticketStartTime").val();
		submitInfo.endTime = $("#ticketEndTime").val();
		submitInfo.impact = $("#impact").val();

		submitInfo.exceed4 = $("#exceed4").val();
		submitInfo.problemTicket = $("#problemTicket").val();
		submitInfo.rfcCause = $("#rfcCause").val();
		submitInfo.partyCause = $("#partyCause").val();
		submitInfo.coordinator = $("#coordinator").val();
		submitInfo.escalatedTo = $("#escalatedTo").val();

		submitInfo.engTeam = $("#engTeam").val();
		submitInfo.partyResolve = $("#thirdPartyResolve").val();
		submitInfo.kbUsed = $("#kbUsed").val();
		submitInfo.kbHelpful = $("#kbHelpful").val();

		console.log(submitInfo.discoveredBy);
	}


	$.ajax(
	{
		url : "databaseUpdate.php",
		type : "POST",
		data :
		{
			update : action,
			id : id,
			submitInfo : submitInfo
		},
		success : function(result)
		{
			result = result.trim();
			if (action == "submit")
			{
				titlelessDialog("popUps", result);
			}

		}
	});



}

function titlelessDialog(id, message, buttons)
{
	$("#" + id).html(message).dialog(
	{
		modal : true,
		buttons : [
		{
			text : "Close",
			click : function()
			{
				$(".ui-dialog-titlebar").show();
				$("#" + id).dialog("close")
			}
		}]
	})
	$(".ui-dialog-titlebar").hide()
}

function viewTable(tableToView, id)
{
	var startDateInfo = $("#startDate").val();
	var endDdateInfo = $("#endDate").val();

	$.ajax(
	{
		url : "view.php",
		type : "POST",
		data :
		{
			table : tableToView,
			startDate : startDateInfo,
			endDate : endDdateInfo,
			incidentId : id
		},
		success : function(result)
		{
			if (tableToView == "incidentList")
			{
				$("#log").attr("class", "show").html(result);
				$("#log").find("table").width($("#log").width());
				applyDataTables("logTable");
				confirmDone();
			}
			else if (tableToView == "summary")
			{
				$("#summaryReport").html(result).dialog(
				{
					dragable : true,
					resizable : true,
					width : 1300,
					height : 800,
					title : "Executive Summary Report for Ticket: " + id,
					resizeStop : function(event, ui)
					{
						//maintain fixed and top relative to page
						var scrollTop = $(window).scrollTop();
						var objectTop =  parseInt($("#summaryReport").parent().css("top"));
						$("#summaryReport").parent().css("position", "fixed");
						$("#summaryReport").parent().css("top", objectTop-scrollTop);
					}
				});
				$("#summaryReport").parent().css("position", "fixed");
				$(":button").button();
				applyDatePicker("datepicker");

				if ($("input[incidentid='" + id + "']").prop("checked"))
				{
					$("#summaryReport").find("img").remove();
					$("#summaryReport :input").each(function()
					{
						if ($(this).is("input"))
						{
							if ($(this).is("input[type*='button']"))
							{
								$(this).remove();
							}
							else
							{
								$(this).replaceWith($(this).attr("value"));
							}
						}
						else if ($(this).is("textarea"))
						{
							$(this).replaceWith($(this).val());
						}
					});
				}

			}
		}
	});
}

function changeClass(id, classToBeChanged, newClass)
{
	var classStore = $(id).attr("class");
	if (classStore != undefined)
	{
		classStore = classStore.replace(classToBeChanged, newClass);
		$(id).attr('class', classStore);
	}
	else
	{
		$(id).attr('class', newClass);
	}
}

function email(sendTo, id, caller)
{
	updateTable("submit", id);
	var subject = "Ticket Review for ticket " + id;
	var message = "";
	if (sendTo == "engineers")
	{
		var engineer = prompt("Please enter the first and last name of the engineer followed by his e-mail");
		engineer = engineer.split(" ");
		while (engineer[0] == undefined || engineer[1] == undefined || engineer[2] == undefined || engineer[2].search("@") == -1)
		{
			engineer = prompt("You entered incomplete information. Please enter the first and last name of the engineer followed by his e-mail");
			engineer = engineer.split(" ");
		}
		message += engineer[0] + " " + engineer[1] + ",<br><br><br>";
		var address = engineer[2];

	}
	if (sendTo == "managers")
	{
		message += "<b style='font-size: 14px;'>Managers:</b><br><br>";
	}
	if (sendTo == "oitNotify")
	{
		message += "<b style='font-size: 14px;'>OIT:</b><br><br>"
	}
	$("#temp").html("<div>" + $("#userMessage").children().val() + "</div>");
	$("#temp").children().css(
	{
		"width" : '100%',
		"text-align" : "left",
		'font-size' : '14px'
	});
	//console.log($("#temp").text());
	message += $("#temp").html();
	message = message.replace(/\r?\n/g, '<br>');

	//===============================================================formats the look of the table for email=============================================================

	$('#temp').html(uniqueClone("summaryReportTable"));
	$("#summaryReportTable :input").each(function()
	{
		if ($(this).is("input") == true)
		{
			$("#" + $(this).attr("id") + "_Clone").attr("value", $(this).val());
		}
		else if ($(this).is("textarea") == true)
		{
			$("#" + $(this).attr("id") + "_Clone").html($(this).val());
		}
		/*
		 console.log($("#" + $(this).attr("id") + "_Clone"));
		 console.log($(this).val());
		 console.log($("#" + $(this).attr("id") + "_Clone").val());*/

	});

	//prepend blank boxes for notes in the email
	var prependTh = "<th style='width:2%' class='blank' rowspan='4'></th>";
	var prependTd = "<td style='width:2%' class='blank'></td>";
	$('#temp td, #temp th').each(function()
	{
		if ($(this).text() == "For Prevention" )//|| $(this).text() == "For Improvement")
		{

			prependTh += $(this).parent().html();

			$(this).parent().html(prependTh);
			//prependTh = "<th style='width:10%'class='blank' rowspan='2'></th>";
		}
		/*if ($(this).attr('id') == "preventionTd" || $(this).attr('id') == "improvementTd")
		{
			prependTd += $(this).parent().html();
			$(this).parent().html(prependTd);
			prependTd = "<td style='width:10%'class='blank'></td>";
		}*/
	});
	//overall table css
	$('#temp table').css(
	{
		width : '100%',
		border : "1px solid gray",
		"border-left-style" : "none",
		"border-top-style" : "none"
	});
	$('#temp table').removeAttr("id");
	$('#temp th').css(
	{
		"background-color" : "rgb(220, 220, 220)"
	});
	$('#temp th, #temp td').css(
	{
		"border" : "1px solid gray",
		"border-right" : "none",
		"border-bottom" : "none"
	});

	$('#temp textarea').each(function()// gets rid of textarea element and replace it with its contents
	{
		var content = $(this).text();
		$(this).replaceWith(content);

	});

	//remove the buttons
	$('#temp :button').remove();
	//remove Email message header
	$('#temp h3').remove();
	//remove calendar icon and input field
	content = $("#temp tr:nth-child(4)").find("input").attr("value");
	$("#temp tr:nth-child(4)").children("td:nth-child(2)").attr("id", "dateTdTemp");
	$("#temp tr:nth-child(4)").children("td:nth-child(2)").html(content);

	//auto fill with "No" for any areas where user did not enter any information. Empty cells used for notes have class blank and they are left alone.
	$('#temp table td').each(function()
	{
		if ($(this).text() == "" && $(this).hasClass("blank") == false)
		{
			$(this).text("No")
		}
	});

	message += $('#temp').html();


	$.ajax(
	{
		url : "email.php",
		type : "POST",
		data :
		{
			to : sendTo,
			address : address,
			incidentId : id,
			subject : subject,
			message : message
		},
		success : function(result)
		{
			result = result.trim();
			if (result == "e-mail was successfully sent")//check if result succeeded.
			{
				changeClass(caller, 'notSent', 'sent');
			}
			titlelessDialog("popUps", result);

			viewTable("incidentList");
		}
	});



}

function uniqueClone(id)
{
	var content = fullElementHtml(id);
	var clonedText = "";
	var id = false;
	var quote = false;

	for ( i = 0; i < content.length; i++)
	{
		clonedText += content[i];
		if (content[i] == "i")
		{
			if (content[i] + content[i + 1] + content[i + 2] == "id=" || content[i] + content[i + 1] + content[i + 2] + content[i + 3] == "id =")
			{
				id = true;
			}
		}
		else if (id == true && (content[i] == "\"" || content[i] == "\'"))
		{
			quote = !quote;
			//turns it to true at opening quotes and false at closing quotes
			if (quote == false)
			{
				id = false;
			}
		}
		else if (quote == true && (content[i + 1] == "\"" || content[i + 1] == "\'"))
		{
			clonedText += "_Clone";
		}
	}
	//console.log(clonedText);
	return clonedText;
}

function fullElementHtml(id)
{
	var text = $("#" + id).wrap("<div></div>").parent().html();
	return text;
}

function confirmEmail(source, id)
{
	var confirmation = confirm("Are you sure you want to re-send this message?");
	if (confirmation == true)
	{
		email(source, id);
	}
}

function confirmDone()
{
	$(".confirmDone").each(function()
	{
		$(this).click(function()
		{
			if ($(this).prop("checked"))
			{
				var confirmation = confirm("Are you sure you want to mark this as done?");
				if (confirmation == false)
				{
					$(this).removeAttr("checked");
				}
				else
				{
					updateDone(done, this);
				}
			}
			else
			{
				var confirmation = confirm("Are you sure you want to mark this as not done?");
				if (confirmation == false)
				{
					$(this).attr("checked", "checked");
				}
				else
				{
					updateDone(notDone, this);
				}
			}
		});
	});
}

function updateDone(action, element)// action is 1 or 0. 1 for setting it as done and 0 as not done
{
	var id = $(element).attr("incidentid");
	console.log(id);
	$.post("databaseUpdate.php",
	{
		"update" : "markAsDone",
		"id" : id,
		"done" : action
	}, function(result)
	{
		viewTable("incidentList");
	});
}

