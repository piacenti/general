<?php

require ('../../includes/includeMeBlank.php');

function emptyResult($selectString)
{
	$result = mysql_query($selectString);
	if (mysql_num_rows($result) == 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

if ($_POST['table'] == "incidentList")
{
	if ($_POST['startDate'] != ""  &&  $_POST['endDate'] != "")//gets rows that fall between the dates plus all the ones that have not been completed
	{
		$query = "SELECT * FROM `incidentManagement` WHERE `dateCreated` BETWEEN '" . $_POST['startDate'] . "' AND '" . $_POST['endDate'] . "' ";
	}
	else if ($_POST['startDate'] != ""  &&  $_POST['endDate'] == "")//use start date up to most recent if no end date is selected
	{
		$query = "SELECT * FROM `incidentManagement` WHERE `dateCreated`>= '" . $_POST['startDate'] . "'";
	}
	else
	{
		$query = "SELECT * FROM `incidentManagement` WHERE `emailFlag`=0";
		//gets only the ones that have not been completed
	}
	if (emptyResult($query) == false)
	{
		$result = mysql_query($query);
		echo "<table id='logTable' style='position:relative'><thead><tr><th>Done</th><th>Incident Number</th><th>Description</th></tr></thead><tbody>";
		while ($row = mysql_fetch_array($result))
		{
			echo "<tr><td ".(($row['emailFlag'] == 1) ? "class='sent'" : "")."><input type='checkbox' incidentId='$row[incidentId]' class='confirmDone' " . (($row['emailFlag'] == 1) ? "checked='checked'" : "") . "><span class='hidden'>".(($row['emailFlag'] == 1) ? "done" : "undone")."</span></td><td class='bubbleInfo' style='position:relative;'>
			<a id='" . $row['incidentId'] . "'  href='https://it.byu.edu/incident.do?sys_id=" . $row['incidentSysId'] . "' target='_blank' class='trigger'> 
			" . $row['incidentId'] . "<div class='popup'><a onclick=viewTable(&quot;summary&quot;,&quot;" . $row['incidentId'] . "&quot;)>Review Ticket</a></div></a>
			</td><td>" . $row['description'] . "</td></tr>";
		}
		echo "</tbody></table>";
		echo "<script>$(\".bubbleInfo\").codabubble({direction:\"right\", offset:0, right:0, hideDelay:100, customCss:\"font-weight:bold;\"});</script>";
	}
}

if ($_POST['table'] == "summary")
{
	$id = $_POST['incidentId'];

	$engEmailFlag = "SELECT * FROM `incidentManagement` WHERE `engEmail`=0 AND incidentId='$id'";
	$engResult = mysql_query($engEmailFlag);
	$oitEmailFlag = "SELECT * FROM `incidentManagement` WHERE `oitEmail`=0 AND incidentId='$id'";
	$oitResult = mysql_query($oitEmailFlag);
	$managEmailFlag = "SELECT * FROM `incidentManagement` WHERE `managersEmail`=0 AND incidentId='$id'";
	$managResult = mysql_query($managEmailFlag);
	$sender = "\n\nThank you.\nSincerely,\n\n" . nameByNetId($netID) . "\nNetwork Operations Center\n(801) 422-4341\nmmimgroup@byu.edu";

	$query = "SELECT * FROM `incidentManagement` WHERE `incidentId`='$id' ";
	$result = mysql_fetch_array(mysql_query($query));
	if ($result['userMessage'] == "")
	{
		$messagePlaceHolder = "Our goal is to send this Executive Summary out to management within 5 business days of the P1. We have decided to send out an email to the OPS Managers to review the information before sending out the Executive Summary to OIT Notify. Please review the information we have, make any corrections, provide any additional information (for fields containing Unknown), etc. Thank you for your time. If you have any feedback please email us at mmimgroup@byu.edu.$sender\n\n";
	}
	else
	{
		$messagePlaceHolder = $result['userMessage'] . "\n\n";
	}

	$class = "showTable";
	$content = <<<Text
	<h3>Email Message:</h3>
	 <div id='userMessage'><textarea  style="height:100px; width:100%;" >$messagePlaceHolder</textarea></div>
	 <table id='summaryReportTable' style="width:100%">
	 <tr><th id='incidentAndDescription' colspan='4' scope='colgroup'>Subject</th></tr>
	 <tr><td colspan='4'><textarea id='description'>$result[description]</textarea></td></tr>
	 
	 <tr class='$class'><th>Incident Number</th><th>Date</th><th>Start Time</th><th>End Time</th></tr>
	 <tr class='$class'><td>$id</td> <td id='dateTd'><input id='dateCreated' class='datepicker' value='$result[dateCreated]'></td> <td><textarea id='ticketStartTime'>$result[startTime]</textarea></td> <td><textarea id='ticketEndTime'>$result[endTime]</textarea></td></tr>
	 
	 <tr class='$class'><th colspan='4'>Impact/Scope</th></tr>
	 <tr class='$class'><td colspan='4'><textarea id='impact'>$result[impact]</textarea></td></tr>
	 
	 <tr><th colspan='4'>Resolution Time Exceeded 4 Hours</th></tr>
	 <tr><td colspan='4'><textarea id='exceed4'>$result[exceed4]</textarea></td></tr>
	 
	 <tr><th colspan='4'>OPS Role</th></tr>
	 <tr><td colspan='4'>Discovered By:<br><textarea id='discoveredBy'>$result[discoveredBy]</textarea><br><br>Steps Taken:<br><textarea id='opsDo'>$result[opsDo]</textarea></td></tr>
	 
	 <tr><th colspan='4'>Engineer's Role</th></tr>
	 <tr><td colspan='4'><textarea id='engineersDo'>$result[engineersDo]</textarea></td></tr>
	 
	 <tr><th colspan='4'>Root Cause</th></tr>
	 <tr><td colspan='4'><textarea id='rootCause'>$result[rootCause]</textarea></td></tr>
	 
	 <tr ><th style='width: 20%'>Associated Problem</th><th>Related RFC</th><th colspan='2'>3rd Party Cause</th></tr>
	 <tr ><td style='width: 20%'><textarea id='problemTicket'>$result[associatedPT]</textarea></td> <td><textarea id='rfcCause'>$result[rfcCause]</textarea></td> <td colspan='2'><textarea id='partyCause'>$result[thirdPartyCause]</textarea></td></tr>
	 
	 <tr ><th>Incident Coordinator</th><th>Escalated to</th><th colspan='2'>Responsible Engineering Team</th></tr>
	 <tr ><td><textarea id='coordinator'>$result[incidentCoordinator]</textarea></td> <td><textarea id='escalatedTo'>$result[escalatedTo]</textarea></td> <td colspan='2'><textarea id='engTeam'>$result[responsibleEngTeam]</textarea></td></tr>
	 
	 <tr><th colspan='4'>Resolution</th></tr>
	 <tr><td colspan='4'><textarea id='resolution'>$result[resolution]</textarea></td></tr>
	 
	 <tr><th colspan='4'>Resolved by 3rd Party</th></tr>
	 <tr><td colspan='4'><textarea id='thirdPartyResolve'>$result[thirdPartyResolve]</textarea></td></tr>
	 
	 <tr ><th>KB Used</th><th colspan='3'>KB Evaluation</th></tr>
	 <tr ><td><textarea id='kbUsed'>$result[kbUsed]</textarea></td> <td colspan='3'><textarea id='kbHelpful'>$result[kbHelpful]</textarea></td></tr>
	 
	 <tr><th colspan='4'>Corrective Actions</th></tr>
	 <tr><th colspan='4'>For Prevention</th></tr>
	 <tr><td colspan='4' id='preventionTd'><textarea id='prevention'>$result[prevention]</textarea></td></tr>
	 <tr><th colspan='4'>For Improvement</th></tr>
	 <tr><td colspan='4' id='improvementTd'><textarea id='improvement'>$result[improvement]</textarea></td></tr>

	 </table>
	 
	 <input type='button'  class='right' value='Save' onclick='updateTable("submit", "$id")'>
	 
Text;
	((mysql_num_rows($engResult) == 1) ? $content .= "<input type='button'  class='right notSent' value='Send to Engineer' onclick='email(\"engineers\", \"$id\", this)'>" : $content .= "<input type='button'  class='right sent' value='Send to Engineer' onclick='confirmEmail(\"engineers\", \"$id\")'>");
	((mysql_num_rows($managResult) == 1) ? $content .= "<input type='button'  class='right notSent' value='Send to Managers' onclick='email(\"managers\", \"$id\", this)'>" : $content .= "<input type='button'  class='right sent' value='Send to Managers' onclick='confirmEmail(\"managers\", \"$id\")'>");
	((mysql_num_rows($oitResult) == 1) ? $content .= "<input type='button'  class='left notSent' value='Send to OIT Notify' onclick='email(\"oitNotify\", \"$id\", this);'>" : $content .= "<input type='button'  class='left sent' value='Send to OIT Notify' onclick='confirmEmail(\"oitNotify\", \"$id\")'>");

	echo $content;
}
?>
