<?php

require ('../../includes/includeMeBlank.php');

//the API for the incident review don't give you the incident ID just their sys_id so this function gets the incident numbers for each incident review
function getIncidentInfo($sys_id)
{
	$incident = new SoapClient("https://it.byu.edu/incident_list.do?WSDL", array(
		'trace' => 1,
		'exceptions' => true,
		'cache_wsdl' => WSDL_CACHE_MEMORY,
		'compression' => SOAP_COMPRESSION_ACCEPT  |  SOAP_COMPRESSION_GZIP,
		'login' => "opsnet-ws",
		'password' => "0psN3tw0rkT3am"
	));

	$incidentSys = $incident->getRecords(array('__encoded_query' => "sys_id=" . $sys_id));

	if (isset($incidentSys->getRecordsResult))
	{
		$returnValues = (object) array(
			"number" => $incidentSys->getRecordsResult->number,
			"description" => $incidentSys->getRecordsResult->short_description,
			"startTime" => $incidentSys->getRecordsResult->opened_at,
			"endTime" => $incidentSys->getRecordsResult->closed_at,
			"dateCreated" => $incidentSys->getRecordsResult->sys_created_on,
			"priority" => $incidentSys->getRecordsResult->priority
		);
		return $returnValues;
	}
}

function alreadyInTable($selectString)
{
	$result = mysql_query($selectString);
	if (mysql_num_rows($result) == 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}

if ($_POST['update'] == "update")
{
	$incidentReviews = new SoapClient("https://it.byu.edu/u_incident_review.do?WSDL", array(
		'trace' => 1,
		'exceptions' => true,
		'cache_wsdl' => WSDL_CACHE_MEMORY,
		'compression' => SOAP_COMPRESSION_ACCEPT  |  SOAP_COMPRESSION_GZIP,
		'login' => "opsnet-ws",
		'password' => "0psN3tw0rkT3am"
	));

	$resultSet = $incidentReviews->getRecords(array("__encoded_query" => "sys_created_on>=2013-04-26 05:52:51"));

	if (isset($resultSet->getRecordsResult))
	{
		
		//var_dump($resultSet);
		foreach ($resultSet->getRecordsResult as $incident)
		{
			//var_dump($incident->u_incident);
			$incidentInfo = getIncidentInfo($incident->u_incident);
			$incidentNumber = $incidentInfo->number;
			$incidentDescription = $incidentInfo->description;
			//$date = date("Y-m-d", strtotime($incident->sys_created_on));
			$date = date("Y-m-d", strtotime($incidentInfo->dateCreated));
			$startTime = date("g:i A", strtotime($incidentInfo->startTime)-60*60*7);
			$endtTime = date("g:i A", strtotime($incidentInfo->endTime)-60*60*7);
			$isItInTable = "SELECT * FROM `incidentManagement` WHERE `incidentId`='$incidentNumber'";
			if (alreadyInTable($isItInTable) == false && $incidentInfo->priority==1)
			{
				$query = "INSERT INTO `incidentManagement` (
				`incidentId`, `description` ,`engineersDo`, `opsDo`,
				 `rootCause`, `resolution`, `prevention`, `improvement`,
				  `emailFlag`, `dateCreated`, `incidentSysId`, `startTime`,
				   `endTime`, `impact`, `exceed4`, `associatedPt`, 
				   `rfcCause`, `thirdPartyCause`, `incidentCoordinator`, `escalatedTo`,
				    `responsibleEngTeam`, `thirdPartyResolve`, `kbUsed`, `kbHelpful`, `engEmail`, `oitEmail`, `managersEmail`, `discoveredBy`) VALUES (
				    '$incidentNumber', '".addslashes($incidentDescription)."' ,'".addslashes($incident->u_what_engineers_did_to_resolv)."', '".addslashes($incident->u_what_ops_did)."',
				  '".addslashes($incident->u_root_cause)."', '".addslashes($incident->u_resolution)."', '".addslashes($incident->u_prevention)."', '".addslashes($incident->u_improvement)."',
				   0, '$date', '$incident->u_incident', '$startTime',
				    '$endtTime', '".addslashes($incident->u_impact_scope)."', '".addslashes($incident->u_resolution_exceed_4_hours__w)."', '".addslashes($incident->u_associated_with_a_problem_ti)."',
				     '".addslashes($incident->u_rfc_caused_the_issue_)."', '".addslashes($incident->u_third_party_caused_the_issue)."', '".addslashes($incident->u_incident_coordinator)."', '".addslashes($incident->u_escalated)."',
				      '".addslashes($incident->u_on_call_engineer)."', '".addslashes($incident->u_third_party)."', '".addslashes($incident->u_kb_article_used)."', '".addslashes($incident->u_kb_helpful_)."', 0, 0, 0, '".addslashes($incident->u_discovered)."') ";
				mysql_query($query);
				echo mysql_error();
			}
		}
	}
}
if ($_POST['update'] == "submit")
{
	$submitInfo = (object)$_POST["submitInfo"];
	//var_dump($submitInfo);
	$id = $_POST['id'];

	$query = "UPDATE `incidentManagement` SET 
		`engineersDo`='".addslashes($submitInfo->engineersDo)."', 
		`opsDo`='".addslashes($submitInfo->opsDo)."', 
		`rootCause`='".addslashes($submitInfo->rootCause)."', 
		`resolution`='".addslashes($submitInfo->resolution)."', 
		`prevention`='".addslashes($submitInfo->prevention)."', 
		`improvement`='".addslashes($submitInfo->improvement)."',
		`description`='".addslashes($submitInfo->description)."',
		`dateCreated`='$submitInfo->dateCreated',
		`startTime`='$submitInfo->startTime',
		 `endTime`='$submitInfo->endTime', 
		 `impact`='".addslashes($submitInfo->impact)."',
		 `exceed4`='".addslashes($submitInfo->exceed4)."', 
		 `associatedPT`='".addslashes($submitInfo->problemTicket)."',
		 `rfcCause`='".addslashes($submitInfo->rfcCause)."', 
		 `thirdPartyCause`='".addslashes($submitInfo->partyCause)."',
		 `incidentCoordinator`='".addslashes($submitInfo->coordinator)."', 
		 `escalatedTo`='".addslashes($submitInfo->escalatedTo)."', 
		 `responsibleEngTeam`='".addslashes($submitInfo->engTeam)."',
		 `thirdPartyResolve`='".addslashes($submitInfo->partyResolve)."',
		 `kbUsed`='".addslashes($submitInfo->kbUsed)."', 
		 `kbHelpful`='".addslashes($submitInfo->kbHelpful)."',
		 `discoveredBy`='".addslashes($submitInfo->discoveredBy)."',
		 `userMessage`= '".addslashes($submitInfo->userMessage)."'
	WHERE `incidentId`='".addslashes($id)."'";
	mysql_query($query);
	if (mysql_query($query) == true)
	{
		echo "Summary was successfully saved";
	}
	else if (mysql_query($query) == false)
	{
		echo "Summary was not saved due to connection issues or bug. Please contact a developer";
	}
}

if ($_POST['update'] == "markAsDone")
{
	$query = "UPDATE `incidentManagement` SET `emailFlag`='$_POST[done]' WHERE `incidentId`='$_POST[id]'";
	mysql_query($query);
	if (mysql_query($query) == true)
	{
		echo "Summary was successfully saved";
	}
	else if (mysql_query($query) == false)
	{
		echo "Summary was not saved due to connection issues or bug. Please contact a developer";
		 die('Invalid query: ' . mysql_error());
	}
}
?>
