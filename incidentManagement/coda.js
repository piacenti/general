/*!
* jQuery Codabubble Plugin
* http://github.com/elidupuis
*
* Copyright 2010, Eli Dupuis but edited and improved by Gabriel Piacenti
* Version: 0.4
* Dual licensed under the MIT (http://www.opensource.org/licenses/mit-license.php) and GPL (http://creativecommons.org/licenses/GPL/2.0/) licenses.
* Requires: jQuery v1.4.2 or later
* Based heavily on Remy Sharp's snippet at http://jqueryfordesigners.com/coda-popup-bubbles/

TODO:
- destroy function
- add smart-direction feature? if direction is set to RIGHT and there's no room for element, make it appear on the left.
*/

// This just returns the browser being used, I use it for css issues



(function($)
{

	var ver = '0.4', methods =
	{
		init : function(options)
		{
			
			// iterate and reformat each matched element
			return this.each(function()
			{
				var $this = $(this), opts = $.extend(
				{
				}, $.fn.codabubble.defaults, options), data = $this.data('codabubble');

				// If the plugin hasn't been initialized yet
				if (!data)
				{
					
					var hideDelayTimer = null, 
					beingShown = false, // tracker
					shown = false, 
					trigger = $(opts.triggerClass, this), 
					popup = $(opts.popupClass, this).css('opacity', 0), 
					customCSS = opts.customCSS, 
					offset, 
					defaultCss ={}, 
					up = opts.up, 
					down = opts.down, 
					left = opts.left, 
					right = opts.right, 
					directionProperty;
					//  determine offset format:
					if ( typeof opts.offset === 'function')
					{
						offset = opts.offset.call(this, popup, $this);
					}
					else
					{
						offset = opts.offset;
					};
					//set up initial css
					defaultCss['width'] = popup.width() + 15 + 'px';
					defaultCss['background-color'] = "white";
					defaultCss['padding'] = "5px 10px 5px 10px";
					defaultCss['-moz-border-radius'] = "10px";
					defaultCss['-webkit-border-radius'] = "10px";
					defaultCss['border-radius'] = "10px";
					defaultCss['border'] = "2px solid rgb(75, 118, 158)";
					defaultCss['font-weight'] = "bold";
					customCSS = customCSS.split(";");
					//parses and sets the valus of defaultCss for every entry made by the user, overrides default if conflict exists
					if (customCSS.length > 0)
					{
						for (x in customCSS)
						{

							customCSS[x] = customCSS[x].split(":");
							if (customCSS[x][0] != undefined && customCSS[x][1] != undefined)
							{
								defaultCss[(customCSS[x][0]).trim()] = (customCSS[x][1]).trim();
							}

						}
					}

					// determine desired direction: (this should work for elements outside and inside tables)
					var padding = defaultCss['padding'].split(" ");

					if (opts.direction == "left")
					{
						directionProperty = 'right';
						if ( typeof InstallTrigger !== 'undefined')//settings for firefox
						{
							if (  popup.parent().is("td") == true)
							{
								var leftPosition = trigger.position().left - parseInt(trigger.width()) - left + right;
								var topPosition = $(this).position().top + parseInt(trigger.height()) / 2 - up + down;
							}
							else
							{
								var leftPosition = trigger.position().left - 1.4 * parseInt(trigger.width()) + parseInt(padding[1]) - left + right;
								var topPosition = trigger.position().top - parseInt(trigger.height()) / 2 - up + down;
							}
						}
						else if (!!window.chrome == true)//settings for chrome
						{
							if ( tableParent = popup.parent().is("td") == true)
								var leftPosition = trigger.position().left - 1.5 * parseInt(trigger.width()) - parseInt(padding[1]) - left + right;
							else
								var leftPosition = trigger.position().left - 1.2 * parseInt(trigger.width()) - parseInt(padding[1]) - left + right;
							var topPosition = trigger.position().top - parseInt(trigger.height()) / 2 - up + down;
						}

						$(popup).offset(
						{
							left : leftPosition - left + right,
							top : topPosition - up + down
						});
						if (defaultCss['right'] != undefined)
							offset += parseInt(defaultCss['right']);

						offset += leftPosition;

					}
					else if (opts.direction == "right")
					{
						directionProperty = 'left';
						if ( typeof InstallTrigger !== 'undefined')//settings for firefox
						{
							if ( popup.parent().is("td") == true)
							{
								var leftPosition = trigger.position().left + 1.7 * parseInt(trigger.width()) + parseInt(padding[1]) - left + right;
								var topPosition = $(this).position().top + parseInt(trigger.height()) / 2 - up + down;
							}
							else
							{
								var leftPosition = trigger.position().left + parseInt(trigger.width()) + parseInt(padding[1]) - left + right;
								var topPosition = trigger.position().top - parseInt(trigger.height()) / 2 - up + down;
							}
						}
						else if (!!window.chrome == true)//settings for chrome
						{
							var leftPosition = trigger.position().left + parseInt(trigger.width()) + parseInt(padding[1]) - left + right - left + right;
							var topPosition = trigger.position().top - parseInt(trigger.height()) / 2 - up + down;
						}

						$(popup).offset(
						{
							left : leftPosition,
							top : topPosition
						});
						if (defaultCss['left'] != undefined)
							offset += parseInt(defaultCss['left']);

						offset += leftPosition;
					}
					else if (opts.direction == "down")
					{
						directionProperty = 'top';
						if ( typeof InstallTrigger !== 'undefined')//settings for firefox
						{
							if (  popup.parent().is("td") == true)
							{
								var leftPosition = trigger.position().left + parseInt(trigger.width()) / 2 - left + right;
								var topPosition = $(this).position().top + parseInt(trigger.height()) + parseInt(padding[1]) + parseInt(padding[3]) - up + down;
							}
							else
							{
								var leftPosition = trigger.position().left - left + right;
								var topPosition = trigger.position().top + parseInt(trigger.height()) + parseInt(padding[0]) - up + down;
							}
						}
						else if (!!window.chrome == true)//settings for chrome
						{
							var leftPosition = trigger.position().left - parseInt(padding[1]) - left + right;
							var topPosition = trigger.position().top + parseInt(trigger.height()) / 2 + parseInt(padding[1]) + parseInt(padding[3]) - up + down;
						}

						$(popup).offset(
						{
							left : leftPosition - left + right,
							top : topPosition - up + down
						});
						if (defaultCss['left'] != undefined)
							offset += parseInt(defaultCss['top']);

						offset += topPosition;
					}
					else
					{
						directionProperty = 'top';
						if ( typeof InstallTrigger !== 'undefined')//settings for firefox
						{
							if ( popup.parent().is("td") == true)
							{
								var leftPosition = trigger.position().left + parseInt(trigger.width()) / 2 - left + right;
								var topPosition = $(this).position().top + parseInt(trigger.height()) + parseInt(padding[1]) + parseInt(padding[3]) - up + down;
							}
							else
							{
								var leftPosition = trigger.position().left - left + right;
								var topPosition = trigger.position().top + parseInt(trigger.height()) + parseInt(padding[0]) - up + down;
							}
						}
						else if (!!window.chrome == true)//settings for chrome
						{
							var leftPosition = trigger.position().left - parseInt(padding[1]) - left + right;
							if ( tableParent = popup.parent().is("td") == true)
								var topPosition = trigger.position().top - parseInt(trigger.height()) / 2 - parseInt(padding[0]) - parseInt(padding[2]) - up + down;
							else
								var topPosition = trigger.position().top - parseInt(trigger.height()) - parseInt(padding[1]) - parseInt(padding[3]) - up + down;
						}

						$(popup).offset(
						{
							left : leftPosition,
							top : topPosition - up + down
						});
						if (defaultCss['left'] != undefined)
							offset += parseInt(defaultCss['top']);

						offset += topPosition;
					}


					// console.log(popup.width());
					//setup offset and additional user css
					defaultCss[directionProperty] = offset + "px";
					defaultCss['display'] = 'block';

					$([trigger.get(0), popup.get(0)]).bind(
					{
						'mouseover.codabubble' : function()
						{
							// stops the hide event if we move from the trigger to the popup element
							if (hideDelayTimer)
							{
								clearTimeout(hideDelayTimer);
							};

							// don't trigger the animation again if we're being shown, or already visible
							if (beingShown || shown)
							{
								return;
							}
							else
							{
								beingShown = true;

								// setup animation properties
								var animCSS =
								{
									opacity : 1
								};
								animCSS[directionProperty] = '-=' + opts.distance + 'px';

								// reset position of popup box and animate:
								popup.css(defaultCss).animate(animCSS, opts.time, 'swing', function()
								{
									// once the animation is complete, set the tracker variables
									beingShown = false;
									shown = true;
								});

							}
						},
						'mouseout.codabubble' : function()
						{
							// reset the timer if we get fired again - avoids double animations
							if (hideDelayTimer)
							{
								clearTimeout(hideDelayTimer);
							};

							// store the timer so that it can be cleared in the mouseover if required
							hideDelayTimer = setTimeout(function()
							{
								hideDelayTimer = null;

								var animCSS =
								{
									opacity : 0
								};
								animCSS[directionProperty] = '-=' + opts.distance + 'px';

								popup.animate(animCSS, opts.time, 'swing', function()
								{
									shown = false;
									// once the animate is complete, set the tracker variables
									popup.hide();
									// hide the popup entirely after the effect (opacity alone doesn't do the job)
								});
							}, opts.hideDelay);
						}
					}).trigger('mouseout.codabubble');


					//  attach data:
					$(this).data('codabubble',
					{
						target : $this,
						opts : opts
					});

				};
			});
		},
		destroy : function()
		{
			// to be implemented....
			if (window.console)
				window.console.log('destroy called.');
		}
	};

	$.fn.codabubble = function(method)
	{
		if (methods[method])
		{
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		}
		else if ( typeof method === 'object' || !method)
		{
			return methods.init.apply(this, arguments);
		}
		else
		{
			$.error('Method ' + method + ' does not exist on jQuery.codabubble');
		}
		;
	};

	//	defaults
	$.fn.codabubble.defaults =
	{
		distance : 10, //  distance traveled by bubble during animation.
		offset : 0, //  offset distance. either an integer of a function that returns an integer
		time : 250, //  milliseconds. duration of the animation.
		hideDelay : 500, //  milliseconds. time before bubble fades out (after mouseout)
		direction : 'up', //  either 'left', 'right', down' or 'up'
		triggerClass : '.trigger', //  class of the trigger (in your markup)
		popupClass : '.popup', //  class of the bubble (in your markup)
		customCSS : "", //user CSS for popup
		up : 0,
		down : 0,
		left : 0,
		right : 0
	};

	$.fn.codabubble.ver = function()
	{
		return "jquery.codabubble ver. " + ver;
	};

})(jQuery);
