<?php

//check permission
require ('../../includes/includeme.php');
//checkPermissionRedirect('incidentManagement')
if(checkPermission('incidentManagement'))
{
?>
<link rel="stylesheet" type="text/css" href="css.css">
<script type="text/javascript" src="javascript.js"></script>
<h1 class="center">Incident Management</h1>
<h2> Incident Review </h2>
<br>
<br>

<label for='startDate'>Start Date</label>
<input type="text" id="startDate" onclick="this.select()">
<label for='endDate'>End Date</label>
<input type="text" id="endDate" onclick="this.select()">
<input type="button" id="searchButton" value="Search" class="right" onclick="viewTable('incidentList');" title="search between dates">

<br>
<br>
<div id="log" class="hidden"></div>
<div id="picture" class="hidden"></div>
<div id="summaryReport" class="hidden"></div>
<div id="popUps" class="hidden"></div>
<div id="temp" class="hidden"></div>

<!-- <span id="testing">Testing</span> -->

<?php
}else{
    echo "<h1>You are not authorized to view this page</h1>";
}
require ('../../includes/includeAtEnd.php');
?>
<script type="text/javascript" src="coda.js"></script>
