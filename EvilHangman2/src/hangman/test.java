package hangman;

import hangman.EvilHangmanGame.GuessAlreadyMadeException;

import java.io.File;
import java.util.Scanner;
import java.util.Set;

public class test
{
	private static int guesses;
	private static EvilHangman newGame;
	private static StringBuilder displayedWord;
	private static char[] lettersUsed;
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args)
	{
		File dictionaryPath = new File(args[0]);
		int wordLength = Integer.parseInt(args[1]);
		guesses = Integer.parseInt(args[2]);
		displayedWord = new StringBuilder();
		lettersUsed = new char[guesses];

		for (int i = 0; i < wordLength; i++)
		{
			displayedWord.append("-");
		}
		displayedWord = new StringBuilder();

		for (int i = 0; i < wordLength; i++)
		{
			displayedWord.append("-");
		}

		newGame = new EvilHangman();
		newGame.startGame(dictionaryPath, wordLength);

		runGame();

	}

	private static void runGame()
	{
		while (isGameOver() == false)
		{
			System.out.println("You have " + guesses + " guesses left");
			lettersUsed();
			System.out.println("Word: " + displayedWord.toString());
			guessInput();

		}
		input.close();

	}

	private static boolean isGameOver()
	{
		Set<String> validWords = newGame.getValidWords();
		if (guesses == 0)
		{
			String word = "";
			for (String i : validWords)
			{
				word = i;
				break;
			}
			System.out.println("You Lose!");
			System.out.println("The Word was: " + word);

			return true;
		}
		else if (displayedWord.toString().indexOf('-') == -1)
		{
			System.out.println("You Win!");
			System.out.println(displayedWord.toString());
			return true;
		}
		System.out.println();
		return false;
	}

	private static void lettersUsed()
	{
		System.out.print("Used letters: ");
		for (char i : lettersUsed)
		{
			System.out.print(i + " ");
		}
		System.out.println();
	}

	private static void guessInput()
	{
		System.out.print("Enter guess: ");
		String test = input.next();
		char guess = test.charAt(0);
		guess = Character.toLowerCase(guess);
		if (Character.isAlphabetic(guess) == false || test.length() > 1)
		{
			System.out.println("Invalid input");
			guessInput();
		}
		else
		{
			try
			{
				Set<String> validWords = newGame.makeGuess(guess);
				processGuessResult(validWords, guess);

			}
			catch (GuessAlreadyMadeException e)
			{
				System.out.println("You already used that letter");
				guessInput();
			}
		}
	}

	private static void processGuessResult(Set<String> validWords, char guess)
	{
		//pick just the first word and see where in the word the character is found
		String word="";
		for (String i : validWords)
		{
			word = i;
			break;
		}
		int numbers = 0;
		for (int i = 0; i < word.length(); i++)
		{
			if (word.charAt(i) == guess)
			{
				displayedWord.replace(i, i + 1, Character.toString(guess));
				numbers++;
			}
		}
		if (numbers > 0)
		{
			if (numbers < 2)
				System.out.println("Yes, there is " + numbers + " " + guess);
			else
				System.out.println("Yes, there are " + numbers+ " " + guess + "'s");
		}
		else
		{
			System.out.println("Sorry, there are no " + guess + "'s");
			guesses--;
		}
	}
}
