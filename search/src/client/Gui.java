/**
 * 
 */
package client;

import java.awt.EventQueue;

import javax.swing.JFrame;

/**
 * @author gabriel_2
 * 
 */
public class Gui
{
	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				Window window = new Window();
				window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				window.setVisible(true);
				
			}
		});
	}
}
