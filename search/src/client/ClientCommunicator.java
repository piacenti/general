package client;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import shared.communication.*;
import shared.communication.output.BatchGetOutput;
import shared.communication.output.BatchSubmitOutput;
import shared.communication.output.FieldGetOuput;
import shared.communication.output.ProjectGetOutput;
import shared.communication.output.SampleGetOutput;
import shared.communication.output.SearchOutput;
import shared.communication.output.ValidateOutput;
import shared.model.Field;
import shared.model.Image;
import shared.model.Project;
import shared.model.User;

public class ClientCommunicator
{
	//information from current user
	private String host;
	private String port;

	public ClientCommunicator(String host_, String port_)
	{
		host = host_;
		port = port_;
	}

	/**
	 * @param input
	 * @return
	 * @throws ClientException
	 */
	public ValidateOutput validateUser(Validate input) throws ClientException
	{
		ValidateOutput result_ = null;
		Object object = doPost("/validateUser", input);
		if (object instanceof String && object.equals("FAILED\n"))
		{
			return null;
		}

		if (object instanceof String && object.equals("empty"))
		{
			return null;
		}
		else
		{
			User user = (User) object;
			result_ = new ValidateOutput(true, user.getFirstName(), user.getLastName(), user.getIndexedRecords());

		}
		return result_;
	}

	/**
	 * @param input
	 * @return
	 * @throws ClientException
	 */
	public ProjectGetOutput getProjects(ProjectGet input) throws ClientException
	{
		ProjectGetOutput result = null;
		Object object = doPost("/getProjects", input);
		if (object instanceof String && object.equals("FAILED\n"))
		{
			return null;
		}
		if (object instanceof String && object.equals("empty"))
		{
			return null;
		}
		@SuppressWarnings("unchecked")
		List<Project> projects = (List<Project>) object;
		result = new ProjectGetOutput(projects);

		return result;
	}

	/**
	 * @param input
	 * @return
	 * @throws ClientException
	 */
	public String getSampleImage(SampleGet input) throws ClientException
	{
		SampleGetOutput result = null;
		Object object = doPost("/getSampleImage", input);
		if (object instanceof String && object.equals("FAILED\n"))
		{
			return "FAILED\n";
		}
		if (object instanceof String && object.equals("empty"))
		{
			return "FAILED\n";
		}
		Image image = (Image) object;

		result = new SampleGetOutput(image.getPath());

		return "http:"+File.separator+File.separator+host+":"+port+File.separator+result.toString();
	}

	/**
	 * @param input
	 * @return
	 * @throws ClientException
	 */
	public String downloadBatch(BatchGet input) throws ClientException
	{
		BatchGetOutput result = null;
		Object object = doPost("/downloadBatch", input);
		if (object instanceof String && object.equals("FAILED\n"))
		{
			return "FAILED\n";
		}
		if (object instanceof String && object.equals("empty"))
		{
			return "FAILED\n";
		}
		result = (BatchGetOutput) object;
		result.setImageUrl("http:"+File.separator+File.separator+host+":"+port+File.separator+result.getImageUrl());
		FieldGetOuput.setHostPort(host, port, result.getFields());

		return result.toString();

	}

	/**
	 * @param input
	 * @return
	 * @throws ClientException
	 */
	public String submitBatch(BatchSubmit input) throws ClientException
	{
		BatchSubmitOutput result = null;
		Object object = doPost("/submitBatch", input);
		if (object instanceof String && object.equals("FAILED\n"))
		{
			return "FAILED\n";
		}
		if (object instanceof String && object.equals("empty"))
		{
			return "FAILED\n";
		}
		if((boolean) object==false)
		{
			return "FAILED\n";
		}
		result = new BatchSubmitOutput((boolean) object);

		return result.toString().toUpperCase();

	}

	/**
	 * @param input
	 * @return
	 * @throws ClientException
	 */
	public FieldGetOuput getFields(FieldGet input) throws ClientException
	{
		FieldGetOuput result = null;
		Object object = doPost("/getFields", input);
		if (object instanceof String && object.equals("FAILED\n"))
		{
			return null;
		}
		if (object instanceof String && object.equals("empty"))
		{
			return null;
		}
		@SuppressWarnings("unchecked")
		List<Field> fields = (List<Field>) object;

		result = new FieldGetOuput(fields);
		result.setHostPort(host, port);
		return result;

	}

	/**
	 * @param input
	 * @return
	 * @throws ClientException
	 */
	public SearchOutput search(Search input) throws ClientException
	{
		SearchOutput result = null;
		Object object = doPost("/search", input);
		if (object instanceof String && object.equals("FAILED\n"))
		{
			return null;
		}
		if (object instanceof String && object.equals("empty"))
		{
			return null;
		}
		result = (SearchOutput) object;
		result.setHostPort(host, port);

		return result;

	}

	/**
	 * 
	 * @param input
	 * @return
	 * @throws ClientException
	 */
	public InputStream downloadFile(String url) throws ClientException
	{
		InputStream result = null;
		result = (InputStream) doGet(url);
		return result;
	}

	private Object doGet(String urlPath) throws ClientException
	{
		Object result = null;
		try
		{
			//make sure the url is formatted correclty
			String formattedString = urlPath.replaceAll("\\\\", "/");
			URL url = new URL(formattedString);

			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			connection.setRequestMethod("GET");

			// Set HTTP request headers, if necessary 

			connection.connect();

			if (connection.getResponseCode() == HttpURLConnection.HTTP_OK)
			{

				//get result object and return it
				result = connection.getInputStream();

			}
			else
			{
				result = "FAILED\n";
			}
		}
		catch (IOException e)
		{
			result = "FAILED\n";
		}
		return result;
	}

	/**
	 * Make HTTP POST request to the specified URL, passing in the specified
	 * postData object
	 * 
	 * @param urlPath
	 *            url path
	 * @param postData
	 * @throws ClientException
	 * @throws ClassNotFoundException
	 */
	private Object doPost(String urlPath, Object postData) throws ClientException
	{
		Object result = null;
		try
		{
			URL url = new URL("http://" + host + ":" + port + urlPath);

			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			connection.setRequestMethod("POST");
			connection.setDoOutput(true);

			connection.connect();

			// Write request body to OutputStream ... 
			//serialize object, 
			//use SealdObject and Cipher to encode object transfered through network, see: http://www.java2s.com/Tutorial/Java/0490__Security/EncryptanobjectwithDES.htm
			ObjectOutputStream out = new ObjectOutputStream(connection.getOutputStream());
			out.writeObject(postData);
			out.flush();
			out.close();

			// Get HTTP response headers, if necessary 
			if (connection.getResponseCode() == HttpURLConnection.HTTP_OK)
			{

				//get result object and return it
				ObjectInputStream response = new ObjectInputStream(connection.getInputStream());
				result = response.readObject();
				result.toString();
			}
			else
			{
				// SERVER RETURNED AN HTTP ERROR 
				result = "FAILED\n";
			}
		}
		catch (IOException | ClassNotFoundException e)
		{
			result = "FAILED\n";
		}
		return result;
	}

}
