/**
 * 
 */
package client;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;

/**
 * @author gabriel_2
 * 
 */
public class DrawableImage
{
	private Image image;
	private Rectangle2D rect;
	private double scale;

	public DrawableImage(Image image_, Rectangle2D rect, double scale_)
	{
		this.image = image_;
		this.rect = rect;
		this.scale=scale_;
	}

	public void draw(Graphics2D g2)
	{
		Rectangle2D bounds = rect.getBounds2D();
		g2.drawImage(image, new AffineTransform(scale, 0, 0, scale, 0, 0), null);
	}
}
