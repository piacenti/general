/**
 * 
 */
package client;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.*;

import shared.communication.FieldGet;
import shared.communication.ProjectGet;
import shared.communication.Search;
import shared.communication.Validate;
import shared.communication.output.FieldGetOuput;
import shared.communication.output.ProjectGetOutput;
import shared.communication.output.SearchOutput;
import shared.communication.output.ValidateOutput;
import shared.model.Field;
import shared.model.Project;

import java.util.ArrayList;

/**
 * @author gabriel_2
 * 
 */
@SuppressWarnings("serial")
public class Window extends JFrame
{
	//track log in parameters
	private ArrayList<JTextField> loginParameterTxtFields;
	private String[] loginParameterArray;

	//layout
	private GridBagConstraints c = new GridBagConstraints();

	//Keep track of components used in multiple functions
	private JPanel search = null;
	private JComboBox<String> projlist = null;
	private JList<String> fieldList = null;
	private JTextField searchText = null;
	private JComboBox<String> results = null;
	private JPanel resultPanel = null;
	private JPanel imagePanel = null;

	//Store in memory necessary information
	private ClientCommunicator communicator = null;
	private FieldGetOuput fieldRes = null;
	private ProjectGetOutput proResult = null;

	public Window()
	{
		super();
		defaultPosSize();
		setTitle("Log In");
		getContentPane().setBackground(new Color(120,120,120,255));
		//create search button
		JButton button = new JButton("Continue");
		button.addActionListener(logIn);

		//button panel
		JPanel buttonPanel = new JPanel();
		//add button to panel
		buttonPanel.add(button);
		//add panel to frame
		add(buttonPanel, BorderLayout.SOUTH);

		//Text panel
		JPanel textPanel = new JPanel();
		textPanel.setLayout(new GridBagLayout());

		//labels
		String[] labels = new String[] { "Host: ", "Port: ", "Username: ", "Password: " };

		for (int i = 0; i < labels.length; i++)
		{
			c.fill = GridBagConstraints.NONE;
			c.gridx = 0;
			c.gridy = i;
			textPanel.add(new JLabel(labels[i]), c);
		}

		//text
		loginParameterTxtFields = new ArrayList<>();

		loginParameterTxtFields.add(new JTextField(10));
		loginParameterTxtFields.add(new JTextField(10));
		loginParameterTxtFields.add(new JTextField(10));
		loginParameterTxtFields.add(new JPasswordField(10));

		for (int i = 0; i < loginParameterTxtFields.size(); i++)
		{
			c.fill = GridBagConstraints.NONE;
			c.gridx = 1;
			c.gridy = i;
			c.insets = new Insets(10, 10, 10, 10);
			textPanel.add(loginParameterTxtFields.get(i), c);
		}

		//add to window
		add(textPanel, BorderLayout.NORTH);
		pack();
	}

	private ActionListener logIn = new ActionListener()
	{

		@Override
		public void actionPerformed(ActionEvent e)
		{
			//validate login information
			loginParameterArray = getLoginParameters();
			communicator = new ClientCommunicator(loginParameterArray[0], loginParameterArray[1]);
			Validate userInput = new Validate(loginParameterArray[2], loginParameterArray[3]);
			ValidateOutput userResult = validateUser(userInput);
			Window.this.setLayout(new GridBagLayout());
			if (userResult != null && userResult.getResult() == true)
			{
				//redraw window with relevant information
				System.out.println("Hello");
				Window.this.getContentPane().removeAll();
				Window.this.setTitle("Search");

				//Add search box and label
				search = new JPanel();
				search.setLayout(new GridBagLayout());
				search.setBackground(new Color(140,140,140, 255));
				search.setBorder(BorderFactory.createLoweredSoftBevelBorder());

				c.fill = GridBagConstraints.NONE;
				c.gridx = 0;
				c.gridy = 0;
				search.add(new JLabel("Search Values"), c);

				searchText = new JTextField(15);
				c.fill = GridBagConstraints.NONE;
				c.gridx = 0;
				c.gridy = 1;
				search.add(searchText, c);

				//add projects to box
				ProjectGet projects = new ProjectGet(loginParameterArray[2], loginParameterArray[3]);
				proResult = getProjects(projects);
				projlist = new JComboBox<>();
				for (Project project : proResult.getProjects())
				{
					projlist.addItem(project.getTitle());
				}
				c.fill = GridBagConstraints.NONE;
				c.gridx = 1;
				c.gridy = 0;
				search.add(new JLabel("Projects"), c);

				c.fill = GridBagConstraints.NONE;
				c.gridx = 1;
				c.gridy = 1;
				search.add(projlist, c);

				//draw initial list of fields
				Window.this.drawFields();
				projlist.addActionListener(setFields);

				search.setSize(100, 30);
				c.gridx = 0;
				c.gridy = 0;
				Window.this.add(search, c);
				pack();
				Window.this.repaint();
			}
		}
	};

	private ActionListener setFields = new ActionListener()
	{

		@Override
		public void actionPerformed(ActionEvent e)
		{
			Window.this.drawFields();
		}

	};

	private void drawFields()
	{
		if (fieldList != null)
		{
			//remove last two indexes added (label, fields, search button)
			search.remove(search.getComponentCount() - 1);
			search.remove(search.getComponentCount() - 1);
			search.remove(search.getComponentCount() - 1);
		}
		//add the label
		c.fill = GridBagConstraints.NONE;
		c.gridx = 2;
		c.gridy = 0;
		search.add(new JLabel("Fields"), c);

		//get the index of currently selected project
		int selectedProId = currentProjectId();

		//get relevant fields
		FieldGet fields = new FieldGet(loginParameterArray[2], loginParameterArray[3], selectedProId);
		fieldRes = getFields(fields);

		//create array of field titles
		String[] values = new String[fieldRes.getFields().size()];
		for (int i = 0; i < fieldRes.getFields().size(); i++)
		{
			values[i] = fieldRes.getFields().get(i).getTitle();
		}

		//create and add list of field titles
		fieldList = new JList<String>(values);
		fieldList.setBorder(BorderFactory.createLoweredSoftBevelBorder());
		fieldList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		c.fill = GridBagConstraints.NONE;
		c.gridx = 2;
		c.gridy = 1;
		search.add(fieldList, c);

		//add search button
		JButton button = new JButton("Search");

		button.addActionListener(performSearch);

		c.fill = GridBagConstraints.NONE;
		c.gridx = 3;
		c.gridy = 1;
		search.add(button, c);

		if (Window.this.getContentPane().getComponentCount() < 1)
		{
			Window.this.add(search);

		}
		Window.this.pack();
		Window.this.repaint();
		Window.this.setVisible(true);
	}

	private ActionListener performSearch = new ActionListener()
	{

		@Override
		public void actionPerformed(ActionEvent e)
		{
			//remove results if already in window
			if (Window.this.getContentPane().getComponentCount() >= 2)
				Window.this.getContentPane().remove(resultPanel);

			results = new JComboBox<String>();

			resultPanel = new JPanel();
			resultPanel.setBackground(new Color(140, 140, 140, 255));
			resultPanel.add(new JLabel("Images"));
			//get search Values
			String searchValues = searchText.getText();

			//gets field values

			StringBuilder fieldValues = new StringBuilder();
			for (String fieldTitle : fieldList.getSelectedValuesList())
			{
				for (Field field : fieldRes.getFields())
				{
					if (fieldTitle.equals(field.getTitle()))
					{
						fieldValues.append(field.getId() + ",");
					}
				}
			}
			//remove last comma
			if (fieldValues.length() != 0)
				fieldValues.deleteCharAt(fieldValues.length() - 1);

			SearchOutput searchResult = null;
			try
			{
				searchResult = communicator.search(new Search(loginParameterArray[2], loginParameterArray[3], fieldValues.toString(), searchValues));
			}
			catch (ClientException e1)
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if (searchResult != null)
			{
				//put result in list of results
				StringBuilder included = new StringBuilder();
				for (shared.communication.output.SearchResult sresult : searchResult.getResults())
				{
					//add only urls not previously added
					if (included.indexOf(sresult.getImageUrl()) == -1)
					{
						results.addItem(sresult.getImageUrl());
						included.append(sresult.getImageUrl() + ", ");
					}

				}
				if (results.getItemCount() == 0)
				{
					results.addItem("No Result");
				}

				//add listener
				results.addActionListener(loadImage);
				resultPanel.add(results);

				c.fill = GridBagConstraints.NONE;
				c.gridx = 0;
				c.gridy = 1;

				Window.this.add(resultPanel, c);
				Window.this.pack();
				Window.this.repaint();
				Window.this.setVisible(true);
				loadImage();
			}
		}
	};
	private ActionListener loadImage = new ActionListener()
	{

		@Override
		public void actionPerformed(ActionEvent e)
		{
			loadImage();
		}
	};

	private void loadImage()
	{
		if (Window.this.getContentPane().getComponentCount() == 3)
		{
			Window.this.getContentPane().remove(imagePanel);
		}

		//get selected url
		String url = (String) results.getSelectedItem();
		BufferedImage image = null;
		if (!url.equals("No Result"))
		{
			//get image

			try
			{
				image = ImageIO.read(communicator.downloadFile(url));
			}
			catch (ClientException | IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			imagePanel = new JPanel();
			imagePanel.setBackground(new Color(50,50,50,255));
			imagePanel.setBorder(BorderFactory.createLoweredSoftBevelBorder());
			JImage imageComponent = new JImage(image, 0.6);

			imagePanel.add(imageComponent);
			c.fill = GridBagConstraints.NONE;
			c.gridx = 0;
			c.gridy = 2;
			add(imagePanel, c);

			repaint();
			setVisible(true);
		}
		pack();
	}

	private void defaultPosSize()
	{
		Toolkit kit = Toolkit.getDefaultToolkit();
		Dimension screensize = kit.getScreenSize();
		//default as half of image size
		setSize(screensize.width / 2, screensize.height / 2);
		//centered
		setLocation(screensize.width / 4, screensize.height / 4);
	}

	private ValidateOutput validateUser(Validate userInput)
	{
		try
		{
			return communicator.validateUser(userInput);
		}
		catch (ClientException e1)
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return null;
	}

	private ProjectGetOutput getProjects(ProjectGet project)
	{
		try
		{
			return communicator.getProjects(project);
		}
		catch (ClientException e1)
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return null;
	}

	private FieldGetOuput getFields(FieldGet fields)
	{
		try
		{
			return communicator.getFields(fields);
		}
		catch (ClientException e1)
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return null;
	}

	private String[] getLoginParameters()
	{
		String[] result = new String[loginParameterTxtFields.size()];
		for (int i = 0; i < loginParameterTxtFields.size(); i++)
		{
			result[i] = loginParameterTxtFields.get(i).getText();
		}

		return result;
	}

	private int currentProjectId()
	{
		for (Project project : proResult.getProjects())
		{
			if (project.getTitle().equals(projlist.getSelectedItem()))
			{
				return project.getId();
			}
		}
		return 0;
	}

}
