/**
 * 
 */
package client;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.Rectangle2D;

import javax.swing.JComponent;

/**
 * @author gabriel_2
 * 
 */
@SuppressWarnings("serial")
public class JImage extends JComponent
{
	DrawableImage image=null;
	public JImage(Image image_, double scale)
	{
		
		image=new DrawableImage(image_, new Rectangle2D.Double(0, 0, image_.getWidth(null), image_.getHeight(null)), scale);
		
		Dimension d = new Dimension();
		d.setSize(image_.getWidth(null)*scale, image_.getHeight(null)*scale);
		this.setPreferredSize(d);
		this.setMinimumSize(new Dimension(0, 0));
	}
	@Override
	protected void paintComponent(Graphics g) {

		super.paintComponent(g);
		
		Graphics2D g2 = (Graphics2D)g;
		image.draw(g2);
	}
}
