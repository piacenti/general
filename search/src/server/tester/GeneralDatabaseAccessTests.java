/**
 * 
 */
package server.tester;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.*;

import server.database.*;
import shared.communication.FieldGet;
import shared.communication.Validate;
import shared.model.*;

/**
 * @author gabriel_2
 * 
 */
public class GeneralDatabaseAccessTests
{
	private Database database;

	public GeneralDatabaseAccessTests() throws DatabaseException
	{
		database = new Database();
	}

	@Before
	public void setUp() throws IOException, SQLException, DatabaseException
	{
		Database.recreateTables();
		database.startTransaction();
	}

	@After
	public void tearDown()
	{
		database.endTransaction();
	}

	//user tests
	@Test
	public void testAddGetUsers() throws IOException, SQLException
	{

		//add user and get it, attempt to add twice as well
		User user = new User("pong", "thing", "john", "flemming", "test@test.com", 0, 0);
		Validate validate = new Validate("pong", "thing");
		assertTrue(database.getUserDB().addUser(user));
		User user2 = database.getUserDB().getUser(validate);
		assertEquals(user2.getUsername(), user.getUsername());
		assertEquals(user2.getPassword(), user.getPassword());
		assertEquals(user2.getEmail(), user.getEmail());
		assertEquals(user2.getFirstName(), user.getFirstName());
		assertEquals(user2.getLastName(), user.getLastName());
		assertEquals(user2.getIndexedRecords(), user.getIndexedRecords());
		assertEquals(user2.getIndexedRecords(), user.getIndexedRecords());
		assertFalse(database.getUserDB().addUser(user));

		//do the same as before with table already having entries
		user = new User("coisa", "feia", "blob", "flemming", "test2@test.com", 0, 0);
		validate = new Validate("coisa", "feia");
		assertTrue(database.getUserDB().addUser(user));
		user2 = database.getUserDB().getUser(validate);
		assertEquals(user2.getUsername(), user.getUsername());
		assertEquals(user2.getPassword(), user.getPassword());
		assertEquals(user2.getEmail(), user.getEmail());
		assertEquals(user2.getFirstName(), user.getFirstName());
		assertEquals(user2.getLastName(), user.getLastName());
		assertEquals(user2.getIndexedRecords(), user.getIndexedRecords());
		assertEquals(user2.getIndexedRecords(), user.getIndexedRecords());
		assertFalse(database.getUserDB().addUser(user));

		//do the same as before with table already having entries
		user = new User("tetris", "feia", "cameo", "mateo", "test2@test.com", 0, 0);
		validate = new Validate("tetris", "feia");
		assertTrue(database.getUserDB().addUser(user));
		user2 = database.getUserDB().getUser(validate);
		assertEquals(user2.getUsername(), user.getUsername());
		assertEquals(user2.getPassword(), user.getPassword());
		assertEquals(user2.getEmail(), user.getEmail());
		assertEquals(user2.getFirstName(), user.getFirstName());
		assertEquals(user2.getLastName(), user.getLastName());
		assertEquals(user2.getIndexedRecords(), user.getIndexedRecords());
		assertEquals(user2.getIndexedRecords(), user.getIndexedRecords());
		assertFalse(database.getUserDB().addUser(user));

		//get unexistent partially wrong info of user, should return null
		validate = new Validate("gambo", "jambo");
		assertNull(database.getUserDB().getUser(validate));
		validate = new Validate("tetris", "thing");
		assertNull(database.getUserDB().getUser(validate));
		validate = new Validate("pong", "feia");
		assertNull(database.getUserDB().getUser(validate));

	}

	@Test
	public void testMultipleUserAdditionsUpdates() throws IOException, SQLException
	{
		//add list of users
		List<User> users = new ArrayList<>();
		users.add(new User("kobald", "sharo", "john", "flemming", "test@test.com", 0, 0));
		users.add(new User("kabis", "hora", "cameo", "mateo", "test2@test.com", 0, 0));
		users.add(new User("thingona", "sabia", "blob", "flemming", "test2@test.com", 0, 0));
		assertTrue(database.getUserDB().addUsers(users));

		users = new ArrayList<>();
		users.add(new User("ljkjl", "kj;l", "john", "flemming", "test@test.com", 0, 0));
		users.add(new User("tetrasdfdis", "rete", "cameo", "mateo", "test2@test.com", 0, 0));
		users.add(new User("adfasd", "sdfgd", "blob", "flemming", "test2@test.com", 0, 0));
		assertTrue(database.getUserDB().addUsers(users));

		users = new ArrayList<>();
		users.add(new User(1, "cortez", "thing", "bbbbb", "ljl;kj", "lasdf", 1, 1));
		users.add(new User(2, "tetris", "cabum", "aaaaa", "asdas", "l;jl;kj", 1, 1));
		users.add(new User(3, "cortez", "cantonez", "eeee", "werer", "l;lkj", 0, 0));
		assertTrue(database.getUserDB().updateUsers(users));

		//update info to match username and password of another user, should not update
		users = new ArrayList<>();
		users.add(new User(1, "tetris", "cabum", "aaaaa", "asdas", "l;jl;kj", 1, 1));
		users.add(new User(2, "tetrasdfdis", "rete", "cameo", "mateo", "test2@test.com", 0, 0));
		//even when mixing with valid entries
		users.add(new User(3, "thingon", "sabia", "blob", "flemming", "test2@test.com", 0, 0));
		assertFalse(database.getUserDB().updateUsers(users));
	}

	@Test
	public void testAddGetFields()
	{
		List<Field> fields = new ArrayList<>();
		fields.add(new Field("thing", 1, 1, "src" + File.separator + "sqlCommands" + File.separator + "sql.txt", "src" + File.separator + "sqlCommands"
				+ File.separator + "sql.txt"));
		fields.add(new Field("test", 1, 1, "src" + File.separator + "sqlCommands" + File.separator + "sql.txt", "src" + File.separator + "sqlCommands"
				+ File.separator + "sql.txt"));
		fields.add(new Field("another", 1, 1, "src" + File.separator + "sqlCommands" + File.separator + "sql.txt", "src" + File.separator + "sqlCommands"
				+ File.separator + "sql.txt"));
		fields.add(new Field("another2", 1, 1, "src" + File.separator + "sqlCommands" + File.separator + "sql.txt", "src" + File.separator + "sqlCommands"
				+ File.separator + "sql.txt"));
		assertTrue(database.getFieldsDB().addFields(fields));

		//0 means no parent id was provided
		FieldGet field = new FieldGet("username", "password", 0);
		assertNull(database.getFieldsDB().getFields(field));
		

		//add more results to existing list
		List<Field> fields3 = new ArrayList<>();
		fields3.add(new Field(";laksjdf", 1, 1, "src" + File.separator + "sqlCommands" + File.separator + "sql.txt", "src" + File.separator + "sqlCommands"
				+ File.separator + "sql.txt"));
		fields3.add(new Field("fgsdg", 1, 1, "src" + File.separator + "sqlCommands" + File.separator + "sql.txt", "src" + File.separator + "sqlCommands"
				+ File.separator + "sql.txt"));
		fields3.add(new Field("rytret", 1, 1, "src" + File.separator + "sqlCommands" + File.separator + "sql.txt", "src" + File.separator + "sqlCommands"
				+ File.separator + "sql.txt"));
		fields3.add(new Field("fgdgfh", 1, 1, "src" + File.separator + "sqlCommands" + File.separator + "sql.txt", "src" + File.separator + "sqlCommands"
				+ File.separator + "sql.txt"));
		assertTrue(database.getFieldsDB().addFields(fields3));


	}

	@Test
	public void testAddImages()
	{
		//adds images with two records to the database
		List<Record> records = new ArrayList<>();
		List<String> strings = new ArrayList<>();
		strings.add("value1");
		strings.add("value2");
		strings.add("value3");
		strings.add("value4");
		records.add(new Record(strings));
		strings = new ArrayList<>();
		strings.add("value5");
		strings.add("value6");
		strings.add("value7");
		strings.add("value8");
		records.add(new Record(strings));
		List<Image> images = new ArrayList<>();
		images.add(new Image(1, "src" + File.separator + "sqlCommands" + File.separator + "sql.txt", 10, 10, 0, 0, records));
		assertTrue(database.getImageDB().addImages(images));

		//adds another image with two records
		records = new ArrayList<>();
		strings = new ArrayList<>();
		strings.add("value10");
		strings.add("value12");
		strings.add("value11");
		strings.add("value14");
		records.add(new Record(strings));
		strings = new ArrayList<>();
		strings.add("value15");
		strings.add("value16");
		strings.add("value17");
		strings.add("value18");
		records.add(new Record(strings));
		images = new ArrayList<>();
		images.add(new Image(1, "src" + File.separator + "sqlCommands" + File.separator + "sql.txt", 10, 10, 0, 0, records));
		assertTrue(database.getImageDB().addImages(images));
		//adds image with no record and rolls back
		records = new ArrayList<>();
		images.add(new Image(1, "src" + File.separator + "sqlCommands" + File.separator + "sql.txt", 10, 2, 0, 0, records));
		assertFalse(database.getImageDB().addImages(images));

		//test BatchGet inside the project test because there is no project for the join in this test
	}

	//since projects includes all other models except user this tests everything combined
	@Test
	public void testProjectsTestAll()
	{
		//adds two projects with two images with two records and four fields for one and 3 fields for the other

		List<Project> projects = new ArrayList<>();

		List<Record> records = new ArrayList<>();
		List<String> strings = new ArrayList<>();
		strings.add("value1");
		strings.add("value2");
		strings.add("value3");
		strings.add("value4");
		records.add(new Record(strings));
		strings = new ArrayList<>();
		strings.add("value5");
		strings.add("value6");
		strings.add("value7");
		strings.add("value8");
		records.add(new Record(strings));
		List<Image> images = new ArrayList<>();
		images.add(new Image(1, "src" + File.separator + "sqlCommands" + File.separator + "sql.txt", 10, 10, 0, 0, records));

		//adds another image with two records
		records = new ArrayList<>();
		strings = new ArrayList<>();
		strings.add("value10");
		strings.add("value12");
		strings.add("value11");
		strings.add("value14");
		records.add(new Record(strings));
		strings = new ArrayList<>();
		strings.add("value15");
		strings.add("value16");
		strings.add("value17");
		strings.add("value18");
		records.add(new Record(strings));
		images.add(new Image(1, "src" + File.separator + "sqlCommands" + File.separator + "sql.txt", 10, 10, 0, 0, records));

		List<Field> fields = new ArrayList<>();
		fields.add(new Field("name", 1, 1, "src" + File.separator + "sqlCommands" + File.separator + "sql.txt", "src" + File.separator + "sqlCommands"
				+ File.separator + "sql.txt"));
		fields.add(new Field("lastname", 1, 1, "src" + File.separator + "sqlCommands" + File.separator + "sql.txt", "src" + File.separator + "sqlCommands"
				+ File.separator + "sql.txt"));
		fields.add(new Field("mother", 1, 1, "src" + File.separator + "sqlCommands" + File.separator + "sql.txt", "src" + File.separator + "sqlCommands"
				+ File.separator + "sql.txt"));
		fields.add(new Field("father", 1, 1, "src" + File.separator + "sqlCommands" + File.separator + "sql.txt", "src" + File.separator + "sqlCommands"
				+ File.separator + "sql.txt"));

		Project project = new Project("1899", 1, 10, 0, 0, 4);
		project.setImages(images);
		project.setFields(fields);

		projects.add(project);
		//add slightly different project

		Project project2 = new Project("1900", 1, 10, 0, 0, 3);
		List<Record> records2 = new ArrayList<>();
		List<String> strings2 = new ArrayList<>();
		strings2.add("value1");
		strings2.add("value2");
		strings2.add("value3");
		records2.add(new Record(strings2));
		strings2 = new ArrayList<>();
		strings2.add("value5");
		strings2.add("value6");
		strings2.add("value7");
		records2.add(new Record(strings2));
		strings2 = new ArrayList<>();
		strings2.add("value5");
		strings2.add("value6");
		strings2.add("value7");
		records2.add(new Record(strings2));
		List<Image> images2 = new ArrayList<>();
		images2.add(new Image(1, "src" + File.separator + "sqlCommands" + File.separator + "sql.txt", 10, 10, 0, 0, records2));

		//adds another image with two records
		records2 = new ArrayList<>();
		strings2 = new ArrayList<>();
		strings2.add("value10");
		strings2.add("value12");
		strings2.add("value11");
		records2.add(new Record(strings2));
		strings2 = new ArrayList<>();
		strings2.add("value15");
		strings2.add("value16");
		strings2.add("value17");
		records2.add(new Record(strings2));
		strings2 = new ArrayList<>();
		strings2.add("value15");
		strings2.add("value16");
		strings2.add("value17");
		records2.add(new Record(strings2));
		images2.add(new Image(1, "src" + File.separator + "sqlCommands" + File.separator + "sql.txt", 10, 10, 0, 0, records2));

		List<Field> fields2 = new ArrayList<>();
		fields2.add(new Field("name", 1, 1, "src" + File.separator + "sqlCommands" + File.separator + "sql.txt", "src" + File.separator + "sqlCommands"
				+ File.separator + "sql.txt"));
		fields2.add(new Field("lastname", 1, 1, "src" + File.separator + "sqlCommands" + File.separator + "sql.txt", "src" + File.separator + "sqlCommands"
				+ File.separator + "sql.txt"));
		fields2.add(new Field("mother", 1, 1, "src" + File.separator + "sqlCommands" + File.separator + "sql.txt", "src" + File.separator + "sqlCommands"
				+ File.separator + "sql.txt"));
		project2.setImages(images2);
		project2.setFields(fields2);

		projects.add(project2);

		//this tests the addProject add image and field lists with parent id
		assertTrue(database.getProjectDB().addProjects(projects));

		//test get Project and make sure it matches the projects added in all relevant areas (not id)
		List<Project> compare = database.getProjectDB().getProjects(0);
		Image imaget1 = projects.get(0).getImages().get(0);
		Image imaget2 = compare.get(0).getImages().get(0);
		Field fieldt1 = projects.get(0).getFields().get(0);
		Field fieldt2 = compare.get(0).getFields().get(0);

		assertEquals(imaget1, imaget2);
		assertEquals(fieldt1, fieldt2);
		assertEquals(projects.get(0), compare.get(0));
		assertEquals(projects, compare);

		//remove a project from compare and check if still matches
		compare.remove(1);

		//should not match	
		assertNotEquals(projects, compare);

		//restore compare and its ids
		compare = database.getProjectDB().getProjects(0);

		//change values that are possible for updates in project "assigned", "completed"
		compare.get(0).setAssigned(1);
		compare.get(1).setAssigned(1);

		//change the assignment of an image
		compare.get(0).getImages().get(1).setAssigned(1);

		//update values of a record
		List<String> newValues = new ArrayList<>();
		newValues.add("oba");
		newValues.add("boaca");
		newValues.add("caboba");
		newValues.add("caraasg");
		compare.get(0).getImages().get(0).getRecordValues().get(0).setValues(newValues);

		assertTrue(database.getProjectDB().updateProjects(compare));
		//since image assignment was changed it will not come up next time so it is removed for comparison
		compare.get(0).getImages().remove(1);

		projects = new ArrayList<>();
		projects = database.getProjectDB().getProjects(0);

		//values should be the same since the only changes made to the database were allowed changes
		assertEquals(projects, compare);

		//compare  assignment and completed for project and images
		assertTrue(projects.get(0).updateEquals(compare.get(0)));
		assertTrue(projects.get(1).updateEquals(compare.get(1)));

		//reset compare to match database
		compare = database.getProjectDB().getProjects(0);

		//attempt to update database with a project that has more changes to it than the ones allowed for updates

		//Only allowed changes should be submitted
		//changes the names of the projects
		compare.get(0).setTitle("New");
		compare.get(0).setAssigned(3);
		compare.get(1).setAssigned(3);

		//attempt update which should be successful for assignments but will not match all the information 
		//that project gets from the database
		//updateEquals should show them as being equal because only updatable parameters are compared
		assertTrue(database.getProjectDB().updateProjects(compare));

		projects = database.getProjectDB().getProjects(0);

		assertNotEquals(projects, compare);
		assertTrue(projects.get(0).updateEquals(compare.get(0)));
		assertTrue(projects.get(1).updateEquals(compare.get(1)));

		//attempt to update the database with an invalid list of projects (id that does not exists in database, 
		//empty project or empty fields, images)

		//empty list
		compare = new ArrayList<>();
		assertFalse(database.getProjectDB().updateProjects(compare));

		//invalid id
		compare = database.getProjectDB().getProjects(0);
		compare.get(0).setId(1000);
		assertFalse(database.getProjectDB().updateProjects(compare));

		//null fields
		compare = database.getProjectDB().getProjects(0);
		compare.get(0).setFields(null);
		assertFalse(database.getProjectDB().updateProjects(compare));

		//empty fields
		compare = database.getProjectDB().getProjects(0);
		compare.get(0).setFields(new ArrayList<Field>());
		assertFalse(database.getProjectDB().updateProjects(compare));

		//null images
		compare = database.getProjectDB().getProjects(0);
		compare.get(0).setImages(null);
		assertFalse(database.getProjectDB().updateProjects(compare));

		//empty images
		compare = database.getProjectDB().getProjects(0);
		compare.get(0).setImages(new ArrayList<Image>(0));
		assertFalse(database.getProjectDB().updateProjects(compare));

		
	}

	@Test
	public void testSearch()
	{

	}

}
