/**
 * 
 */
package server.tester;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.*;

import server.database.*;
import shared.communication.BatchGet;
import shared.communication.BatchSubmit;
import shared.communication.FieldGet;
import shared.communication.SampleGet;
import shared.communication.Search;
import shared.communication.Validate;
import shared.communication.output.BatchGetOutput;
import shared.communication.output.SearchOutput;
import shared.model.*;

/**
 * @author gabriel_2
 * 
 */
public class CommDatabaseAccessTests
{
	private Database database;

	public CommDatabaseAccessTests() throws DatabaseException
	{
		database = new Database();
	}

	@Before
	public void setup() throws IOException, SQLException, DatabaseException
	{
		Database.recreateTables();
		database.startTransaction();
		List<Project> projects = new ArrayList<>();

		List<Record> records = new ArrayList<>();
		List<String> strings = new ArrayList<>();
		strings.add("value1");
		strings.add("value2");
		strings.add("value3");
		strings.add("value4");
		records.add(new Record(strings));
		strings = new ArrayList<>();
		strings.add("value5");
		strings.add("value6");
		strings.add("value7");
		strings.add("value8");
		records.add(new Record(strings));
		strings = new ArrayList<>();
		strings.add("value5");
		strings.add("value6");
		strings.add("value7");
		strings.add("value8");
		records.add(new Record(strings));
		List<Image> images = new ArrayList<>();
		images.add(new Image(1, "src" + File.separator + "sqlCommands" + File.separator + "sql.txt", 10, 10, 0, 0, records));

		//adds another image with three records where one repeats values
		records = new ArrayList<>();
		strings = new ArrayList<>();
		strings.add("value9");
		strings.add("value10");
		strings.add("value11");
		strings.add("value12");
		records.add(new Record(strings));
		strings = new ArrayList<>();
		strings.add("value13");
		strings.add("value14");
		strings.add("value15");
		strings.add("value16");
		records.add(new Record(strings));
		strings = new ArrayList<>();
		strings.add("value9");
		strings.add("value10");
		strings.add("value11");
		strings.add("value12");
		records.add(new Record(strings));
		images.add(new Image(1, "src" + File.separator + "sqlCommands" + File.separator + "sql.txt", 10, 10, 0, 0, records));

		List<Field> fields = new ArrayList<>();
		fields.add(new Field("name", 1, 1, "src" + File.separator + "sqlCommands" + File.separator + "sql.txt", "src" + File.separator + "sqlCommands"
				+ File.separator + "sql.txt"));
		fields.add(new Field("lastname", 1, 1, "src" + File.separator + "sqlCommands" + File.separator + "sql.txt", "src" + File.separator + "sqlCommands"
				+ File.separator + "sql.txt"));
		fields.add(new Field("mother", 1, 1, "src" + File.separator + "sqlCommands" + File.separator + "sql.txt", "src" + File.separator + "sqlCommands"
				+ File.separator + "sql.txt"));
		fields.add(new Field("father", 1, 1, "src" + File.separator + "sqlCommands" + File.separator + "sql.txt", "src" + File.separator + "sqlCommands"
				+ File.separator + "sql.txt"));

		Project project = new Project("1899", 1, 10, 0, 0, 3);
		project.setImages(images);
		project.setFields(fields);

		projects.add(project);
		//add slightly different project

		Project project2 = new Project("1900", 1, 10, 0, 0, 3);
		List<Record> records2 = new ArrayList<>();
		List<String> strings2 = new ArrayList<>();
		strings2.add("value1");
		strings2.add("value2");
		strings2.add("value3");
		records2.add(new Record(strings2));
		strings2 = new ArrayList<>();
		strings2.add("value5");
		strings2.add("value6");
		strings2.add("value7");
		records2.add(new Record(strings2));
		strings2 = new ArrayList<>();
		strings2.add("value5");
		strings2.add("value6");
		strings2.add("value7");
		records2.add(new Record(strings2));
		List<Image> images2 = new ArrayList<>();
		images2.add(new Image(1, "src" + File.separator + "sqlCommands" + File.separator + "sql.txt", 10, 10, 0, 0, records2));

		//adds another image with two records
		records2 = new ArrayList<>();
		strings2 = new ArrayList<>();
		strings2.add("value10");
		strings2.add("value12");
		strings2.add("value11");
		records2.add(new Record(strings2));
		strings2 = new ArrayList<>();
		strings2.add("value15");
		strings2.add("value16");
		strings2.add("value17");
		records2.add(new Record(strings2));
		strings2 = new ArrayList<>();
		strings2.add("value15");
		strings2.add("value16");
		strings2.add("value17");
		records2.add(new Record(strings2));
		images2.add(new Image(1, "src" + File.separator + "sqlCommands" + File.separator + "sql.txt", 10, 10, 0, 0, records2));

		List<Field> fields2 = new ArrayList<>();
		fields2.add(new Field("name", 1, 1, "src" + File.separator + "sqlCommands" + File.separator + "sql.txt", "src" + File.separator + "sqlCommands"
				+ File.separator + "sql.txt"));
		fields2.add(new Field("lastname", 1, 1, "src" + File.separator + "sqlCommands" + File.separator + "sql.txt", "src" + File.separator + "sqlCommands"
				+ File.separator + "sql.txt"));
		fields2.add(new Field("mother", 1, 1, "src" + File.separator + "sqlCommands" + File.separator + "sql.txt", "src" + File.separator + "sqlCommands"
				+ File.separator + "sql.txt"));
		project2.setImages(images2);
		project2.setFields(fields2);

		projects.add(project2);
		database.getProjectDB().addProjects(projects);

		User user = new User("pong", "thing", "john", "flemming", "test@test.com", 0, 0);
		database.getUserDB().addUser(user);

		//do the same as before with table already having entries
		user = new User("coisa", "feia", "blob", "flemming", "test2@test.com", 0, 0);
		database.getUserDB().addUser(user);

		//do the same as before with table already having entries
		user = new User("tetris", "feia", "cameo", "mateo", "test2@test.com", 0, 0);
		database.getUserDB().addUser(user);
	}

	@After
	public void tearDown()
	{
		database.endTransaction();
	}

	@Test
	public void testGetSample()
	{
		//test invalid user
		SampleGet sample = new SampleGet("tad", "thing", 1);
		assertNull(database.getImageDB().getSampeImage(sample));

		//test valid
		sample = new SampleGet("pong", "thing", 1);
		Image image = database.getImageDB().getSampeImage(sample);
		assertNotNull(image);
	}

	@Test
	public void testDownloadBatch()
	{
		//test invalid user
		BatchGet input = new BatchGet("pong", "tong", 1);
		BatchGetOutput batch = database.getImageDB().downloadBatch(input);
		assertNull(batch);

		//using valid user
		input = new BatchGet("coisa", "feia", 2);
		batch = database.getImageDB().downloadBatch(input);
		assertNotNull(batch);
		//should have returned first valid image which would be image id 3 for project 2
		assertEquals(3, batch.getBatchId());

		//image should be assigned to user 2 and user should have as current image image 3
		User user = database.getUserDB().getUser(new Validate("coisa", "feia"));
		Image image = database.getImageDB().getImageById(3);
		assertEquals(3, user.getCurrentImage());
		assertEquals(2, image.getAssigned());

		//try to get another image for same user and should not get anything back
		input = new BatchGet("coisa", "feia", 2);
		batch = database.getImageDB().downloadBatch(input);
		assertNull(batch);

		//try to get another image from project 2 with another user
		input = new BatchGet("pong", "thing", 2);
		batch = database.getImageDB().downloadBatch(input);
		assertNotNull(batch);
		//image id should be 4 and user should have it as assignment, user id is 1 in image assignment
		user = database.getUserDB().getUser(new Validate("pong", "thing"));
		image = database.getImageDB().getImageById(4);
		assertEquals(4, user.getCurrentImage());
		assertEquals(1, image.getAssigned());

		//try to get another batch from project 2 with last valid user
		input = new BatchGet("tetris", "feia", 2);
		batch = database.getImageDB().downloadBatch(input);
		//should return null
		assertNull(batch);

	}

	@Test
	public void testSubmitBatch()
	{
		//test invalid user
		BatchSubmit input = new BatchSubmit("tango", "thing", 1, "1test1,1test2,1test3,1test4;1test5,1test6,1test7,1test8");
		assertFalse(database.getImageDB().submitBatch(input));

		//attempt to update with user that does not have image
		input = new BatchSubmit("pong", "thing", 1, "2test1,2test2,2test3,2test4;2test5,2test6,2test7,2test8");
		assertFalse(database.getImageDB().submitBatch(input));

		//get image for user
		BatchGet inputG = new BatchGet("pong", "thing", 2);
		BatchGetOutput batch = database.getImageDB().downloadBatch(inputG);
		assertNotNull(batch);

		//test bad submit with wrong number of values per row
		input = new BatchSubmit("pong", "thing", 2, "test1,test2,test3,test4;test5,test6,test7,test8,test9");
		assertFalse(database.getImageDB().submitBatch(input));

		//wrong number of values again with some spaces
		input = new BatchSubmit("pong", "thing", 2, "test1,test2,test3,test4;test5, test6,test7,test8;value9,value10,value11,value12, value13");
		assertFalse(database.getImageDB().submitBatch(input));

		//right number of values but wrong image
		input = new BatchSubmit("pong", "thing", 3, "test1,test2,test3,test4;test5, test6,test7,test8;value9,value10,value11,value12");
		assertFalse(database.getImageDB().submitBatch(input));

		//test successful submit that should change first two rows of image 3, using spaces 
		input = new BatchSubmit("pong", "thing", 3, "test1  ,test2 ,test3  ;test5 ,test6  , test7 ;value5,value6,value7");
		assertTrue(database.getImageDB().submitBatch(input));

		//get another image from project 2, should be image 4
		inputG = new BatchGet("pong", "thing", 2);
		batch = database.getImageDB().downloadBatch(inputG);
		assertNotNull(batch);

		//submit image and confirm the project was marked completed

		//submit incomplete image, remaining records should be blank
		input = new BatchSubmit("pong", "thing", 4, "test1,test2,test3;,,;,,");
		assertTrue(database.getImageDB().submitBatch(input));

		//check project completed
		assertEquals(1, database.getProjectDB().getProjects(1).get(1).getCompleted());

	}

	@Test
	public void testGetFields()
	{
		//test invalid user
		FieldGet user = new FieldGet("tongue", "pong", 0);
		assertNull(database.getFieldsDB().getFields(user));

		//test valid user and getting only fields from project 2, there should be 3 fields
		user = new FieldGet("pong", "thing", 2);
		List<Field> fields = database.getFieldsDB().getFields(user);
		assertNotNull(fields);
		assertEquals(3, fields.size());

		//make sure it gets fields 5 through 7
		assertEquals(5, fields.get(0).getId());
		assertEquals(6, fields.get(1).getId());
		assertEquals(7, fields.get(2).getId());

		//test valid user and getting all fields, there should be 7
		user = new FieldGet("pong", "thing", 0);
		assertNotNull(database.getFieldsDB().getFields(user));
		assertEquals(7, database.getFieldsDB().getFields(user).size());
	}

	@Test
	public void search()
	{
		//complete all images so that they may be searched
		assertNotNull(database.getImageDB().downloadBatch(new BatchGet("pong", "thing", 1)));
		assertTrue(database.getImageDB().submitBatch(new BatchSubmit("pong", "thing", 1, "value1,value2,value3,value4")));

		assertNotNull(database.getImageDB().downloadBatch(new BatchGet("pong", "thing", 1)));
		assertTrue(database.getImageDB().submitBatch(new BatchSubmit("pong", "thing", 2, "value9,value10,value11,value12")));

		assertNotNull(database.getImageDB().downloadBatch(new BatchGet("pong", "thing", 2)));
		assertTrue(database.getImageDB().submitBatch(new BatchSubmit("pong", "thing", 3, "value1,value2,value3")));

		assertNotNull(database.getImageDB().downloadBatch(new BatchGet("pong", "thing", 2)));
		assertTrue(database.getImageDB().submitBatch(new BatchSubmit("pong", "thing", 4, "value10,value12,value11")));

		//test invalid user
		Search input = new Search("bla", "bla", "1", "value9");
		SearchOutput result = database.search(input);
		assertNull(result);

		//test with user and 2 results
		input = new Search("pong", "thing", "1", "value9");
		result = database.search(input);
		assertNotNull(result);

		//result should contain 2 search results of column 1, field id 1, rows 1 and 3 and image 2
		assertEquals(2, result.getResults().size());
		assertEquals(result.getResults().get(0).getImageId(), 2);
		assertEquals(result.getResults().get(0).getFieldId(), 1);
		assertEquals(result.getResults().get(0).getRecNumber(), 1);
		assertEquals(result.getResults().get(0).getImageUrl(), "src" + File.separator + "sqlCommands" + File.separator + "sql.txt");
		assertEquals(result.getResults().get(1).getImageId(), 2);
		assertEquals(result.getResults().get(1).getFieldId(), 1);
		assertEquals(result.getResults().get(1).getRecNumber(), 3);
		assertEquals(result.getResults().get(1).getImageUrl(),"src" + File.separator + "sqlCommands" + File.separator + "sql.txt");

		//search with multiple fields and values
		input = new Search("pong", "thing", "1, 4", "value12, value9, value1");
		result = database.search(input);
		assertNotNull(result);
		//there should be 5 results
		assertEquals(5, result.getResults().size());

		assertEquals(result.getResults().get(0).getImageId(), 1);
		assertEquals(result.getResults().get(0).getFieldId(), 1);
		assertEquals(result.getResults().get(0).getRecNumber(), 1);
		assertEquals(result.getResults().get(0).getImageUrl(),"src" + File.separator + "sqlCommands" + File.separator + "sql.txt");
		assertEquals(result.getResults().get(1).getImageId(), 2);
		assertEquals(result.getResults().get(1).getFieldId(), 1);
		assertEquals(result.getResults().get(1).getRecNumber(), 1);
		assertEquals(result.getResults().get(1).getImageUrl(), "src" + File.separator + "sqlCommands" + File.separator + "sql.txt");
		assertEquals(result.getResults().get(2).getImageId(), 2);
		assertEquals(result.getResults().get(2).getFieldId(), 1);
		assertEquals(result.getResults().get(2).getRecNumber(), 3);
		assertEquals(result.getResults().get(2).getImageUrl(),"src" + File.separator + "sqlCommands" + File.separator + "sql.txt");
		assertEquals(result.getResults().get(3).getImageId(), 2);
		assertEquals(result.getResults().get(3).getFieldId(), 4);
		assertEquals(result.getResults().get(3).getRecNumber(), 1);
		assertEquals(result.getResults().get(3).getImageUrl(), "src" + File.separator + "sqlCommands" + File.separator + "sql.txt");
		assertEquals(result.getResults().get(4).getImageId(), 2);
		assertEquals(result.getResults().get(4).getFieldId(), 4);
		assertEquals(result.getResults().get(4).getRecNumber(), 3);
		assertEquals(result.getResults().get(4).getImageUrl(), "src" + File.separator + "sqlCommands" + File.separator + "sql.txt");
	}
}
