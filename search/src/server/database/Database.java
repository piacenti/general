package server.database;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.sql.*;
import java.util.Scanner;
import java.util.logging.Logger;

import shared.communication.Search;
import shared.communication.output.SearchOutput;
import shared.communication.output.SearchResult;

/**
 * @author Gabriel Piacenti
 * 
 */
public class Database
{

	private DBuser userDB;
	private DBproject projectDB;
	private DBimage imageDB;
	private DBfield fieldsDB;
	private static Logger logger;
	private Connection connection;
	private boolean error;

	/**
	 * Database constructor
	 * 
	 * @throws DatabaseException
	 */
	/**
	 * Gives access to SQL database and initializes its extensions to perform
	 * specific operations
	 * 
	 */
	public Database() throws DatabaseException
	{
		userDB = new DBuser(this);
		projectDB = new DBproject(this);
		imageDB = new DBimage(this);
		fieldsDB = new DBfield(this);
		connection = null;
		initialize();
	}

	/**
	 * loads the database driver
	 * 
	 * @throws DatabaseException
	 *             Exception that deals with problems loading database driver
	 *             and initializing connection with database
	 */
	public static void initialize() throws DatabaseException
	{

		logger.entering("server.database.Database", "initialize");

		try
		{
			final String driver = "org.sqlite.JDBC";
			Class.forName(driver);
		}
		catch (ClassNotFoundException e)
		{

			// ERROR! Could not load database driver 
		}

		logger.exiting("server.database.Database", "initialize");
	}

	/**
	 * start a transaction of database queries
	 * 
	 */
	public void startTransaction()
	{

		logger.entering("server.database.Database", "startTransaction");

		String dbName = "db" + File.separator + "Porject1.sqlite";
		String connectionURL = "jdbc:sqlite:" + dbName;
		connection = null;
		try
		{
			// Open a database connection 
			connection = DriverManager.getConnection(connectionURL);

			// Start a transaction 
			connection.setAutoCommit(false);
		}
		catch (SQLException e)
		{
			error = true;
			// ERROR 
		}

		logger.exiting("server.database.Database", "startTransaction");
	}

	/**
	 * end a transaction by either committing it or rolling it back
	 * 
	 */
	public void endTransaction()
	{
		logger.entering("server.database.Database", "endTransaction");

		try
		{
			if (error == false)
			{
				logger.info("successfull transaction");
				connection.commit();
			}
			else
			{
				error = false;
				logger.info("rolled back");
				connection.rollback();
			}
		}
		catch (SQLException e)
		{
			// ERROR 
		}
		finally
		{
			try
			{
				connection.close();
			}
			catch (SQLException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		connection = null;

		logger.exiting("server.database.Database", "endTransaction");
	}

	static
	{
		logger = Logger.getLogger("project");
	}

	/**
	 * 
	 * 
	 * @return Connection object
	 */
	public Connection getConnection()
	{
		return connection;
	}


	public static void recreateTables() throws IOException, SQLException, DatabaseException
	{
		Database database=new Database();
		
		database.startTransaction();
		Statement stmt = null;
		Scanner sqlFile = new Scanner(Paths.get("src" + File.separator + "sqlCommands" + File.separator + "sql.txt"));
		StringBuilder string = new StringBuilder();
		stmt = database.connection.createStatement();

		while (sqlFile.hasNextLine())
		{
			String line = sqlFile.nextLine().trim();
			if (line.endsWith(";"))
			{
				line = line.substring(0, line.length() - 1);
				string.append(line);
				stmt.addBatch(string.toString());
				string = new StringBuilder();
			}
			else
			{
				string.append(line);
			}
		}
		sqlFile.close();

		stmt.executeBatch();

		stmt.close();
		database.endTransaction();
	}

	public SearchOutput search(Search search)
	{
		String fields = search.getFieldsValues();
		String terms = search.getSearchValues();
		ResultSet result = null;
		PreparedStatement stmt = null;
		SearchOutput output = null;

		if (userDB.validateUser(search.getUsername(), search.getPassword()) == true)
		{
			try
			{
				int numberOfFields = fields.split(",").length;
				int numberOfTerms = terms.split(",").length;

				String[] fieldsArray = fields.split(",");
				String[] termsArray = terms.split(",");

				StringBuilder fieldQuestionMarks = new StringBuilder();
				StringBuilder termQuestionMarks = new StringBuilder();

				for (@SuppressWarnings("unused") String string : fields.split(","))
				{
					fieldQuestionMarks.append("?,");
				}
				//remove last comma
				fieldQuestionMarks.deleteCharAt(fieldQuestionMarks.length() - 1);
				for (@SuppressWarnings("unused") String string : terms.split(","))
				{
					termQuestionMarks.append("?,");
				}
				//remove last comma
				termQuestionMarks.deleteCharAt(termQuestionMarks.length() - 1);

				//performs the search in the database and only returns relevant information
				String sql = "SELECT images.id AS imageId, images.file AS imageFile, fields.id AS fieldId, cell.value AS value, cell.row AS row "
						+ "FROM project  " + "LEFT JOIN images ON project.id=images.parentId " + "LEFT JOIN fields ON project.id=fields.parentId "
						+ "LEFT JOIN cell ON images.id=cell.parentId " + "WHERE cell.column=fields.number AND images.completed=1 AND fields.id IN (" + fieldQuestionMarks.toString()
						+ ") AND LOWER(cell.value) IN (" + termQuestionMarks.toString() + ") ";

				stmt = connection.prepareStatement(sql);

				//prepare statement based on the number of fields and terms
				int counter = 1;
				int test = 0;
				for (int i = 0; i < numberOfFields; i++, counter++)
				{
					test = Integer.parseInt(fieldsArray[i].trim());
					stmt.setInt(counter, test);

				}
				for (int i = 0; i < numberOfTerms; i++, counter++)
				{
					stmt.setString(counter, termsArray[i].trim().toLowerCase());
				}
				result = stmt.executeQuery();

				//gets results
				output = new SearchOutput();
				while (result.next())
				{
					SearchResult queryResult = new SearchResult();
					queryResult.setImageId(result.getInt("imageId"));
					queryResult.setImageUrl(result.getString("imageFile"));
					queryResult.setRecNumber(result.getInt("row"));
					queryResult.setFieldId(result.getInt("fieldId"));

					output.getResults().add(queryResult);
				}
			}
			catch (SQLException e)
			{

			}
			finally
			{
				try
				{
					result.close();
				}
				catch (SQLException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				;
				try
				{
					stmt.close();
				}
				catch (SQLException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				;
			}
		}
		return output;
	}

	/**
	 * @return DBuser object
	 */
	public DBuser getUserDB()
	{
		return userDB;
	}

	/**
	 * @return DBproject object
	 */
	public DBproject getProjectDB()
	{
		return projectDB;
	}

	/**
	 * @return DBimage object
	 */
	public DBimage getImageDB()
	{
		return imageDB;
	}

	/**
	 * @return DBfield object
	 */
	public DBfield getFieldsDB()
	{
		return fieldsDB;
	}

	/**
	 * @return the error
	 */
	public boolean isError()
	{
		return error;
	}

	/**
	 * @param error
	 *            the error to set
	 */
	public void setError(boolean error)
	{
		this.error = error;
	}

}
