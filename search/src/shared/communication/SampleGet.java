package shared.communication;

import java.io.Serializable;

/**Holds input information requred to get a sample image
 *
 */
@SuppressWarnings("serial")
public class SampleGet implements Serializable
{
	private String username;
	private String password;
	private int projectId;

	/**
	 * @param name
	 * @param pass
	 * @param proId
	 */
	public SampleGet(String name, String pass, int proId)
	{
		username=name;
		password=pass;
		projectId=proId;
	}

	/**
	 * @return
	 */
	public String getUsername()
	{
		return username;
	}

	/**
	 * @param username 
	 */
	public void setUsername(String username)
	{
		this.username = username;
	}

	/**
	 * @return 
	 */
	public String getPassword()
	{
		return password;
	}

	/**
	 * @param password 
	 */
	public void setPassword(String password)
	{
		this.password = password;
	}

	/**
	 * @return project id
	 */
	public int getProjectId()
	{
		return projectId;
	}

	/**
	 * @param projectId project id
	 */
	public void setProjectId(int projectId)
	{
		this.projectId = projectId;
	}

}
