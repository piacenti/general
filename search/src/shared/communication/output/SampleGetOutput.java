package shared.communication.output;

import java.io.File;
import java.io.Serializable;

/**Holds result information for the SampleGet request
 *
 */
@SuppressWarnings("serial")
public class SampleGetOutput implements Serializable
{
	private String url;
	

	/**
	 * @param path
	 */
	public SampleGetOutput(File path)
	{
		url=path.getPath();
	}

	public String toString()
	{
		return url+"\n";
		
	}

	/**
	 * @return url
	 */
	public String getUrl()
	{
		return url;
	}

	/**
	 * @param url url
	 */
	public void setUrl(String url)
	{
		this.url = url;
	}

	
}
