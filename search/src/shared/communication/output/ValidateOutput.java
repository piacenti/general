package shared.communication.output;

import java.io.Serializable;

/**
 * Holds result of user validation
 * 
 */
@SuppressWarnings("serial")
public class ValidateOutput implements Serializable
{
	private String firstName;
	private String lastName;
	private boolean result;
	private int numRecords;

	/**
	 * @param res
	 * @param name
	 * @param pass
	 * @param indexedRecords
	 */
	public ValidateOutput(boolean res, String name, String pass, int indexedRecords)
	{
		result = res;
		firstName = name;
		lastName = pass;
		numRecords = indexedRecords;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		String res = result + "";
		if (result == true)
		{
			return res.toUpperCase() + "\n" + firstName + "\n" + lastName + "\n" + numRecords + "\n";
		}
		else
		{
			return res.toUpperCase() + "\n";
		}
	}

	/**
	 * @return
	 */
	public String getUsername()
	{
		return firstName;
	}

	/**
	 * @param username
	 */
	public void setUsername(String username)
	{
		this.firstName = username;
	}

	/**
	 * @return
	 */
	public String getPassword()
	{
		return lastName;
	}

	/**
	 * @param password
	 */
	public void setPassword(String password)
	{
		this.lastName = password;
	}

	/**
	 * @return true for successful validation or false otherwise
	 */
	public boolean getResult()
	{
		return result;
	}

	/**
	 * @param result
	 *            true for successful validation or false otherwise
	 */
	public void setResult(boolean result)
	{
		this.result = result;
	}

	/**
	 * @return number of records
	 */
	public int getNumRecords()
	{
		return numRecords;
	}

	/**
	 * @param numRecords
	 *            number of records
	 */
	public void setNumRecords(int numRecords)
	{
		this.numRecords = numRecords;
	}

}
