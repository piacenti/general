package shared.communication;

import java.io.Serializable;

/**Holds input information of search
 *
 */
@SuppressWarnings("serial")
public class Search implements Serializable
{
	private String username;
	private String password;
	//may be empty, string contains ids of fields
	private String fieldsValues;
	//may be empty
	private String searchValues;

	/**
	 * @param name
	 * @param pass
	 * @param fieldV
	 * @param sv
	 */
	public Search(String name, String pass, String fieldV, String sv)
	{
		username=name;
		password=pass;
		fieldsValues=fieldV;
		searchValues=sv;
	}

	/**
	 * @return 
	 */
	public String getUsername()
	{
		return username;
	}

	/**
	 * @param username 
	 */
	public void setUsername(String username)
	{
		this.username = username;
	}

	/**
	 * @return 
	 */
	public String getPassword()
	{
		return password;
	}

	/**
	 * @param password 
	 */
	public void setPassword(String password)
	{
		this.password = password;
	}

	/**
	 * @return  field values
	 */
	public String getFieldsValues()
	{
		return fieldsValues;
	}

	/**
	 * @param fieldsValues  field values
	 */
	public void setFieldsValues(String fieldsValues)
	{
		this.fieldsValues = fieldsValues;
	}

	/**
	 * @return  search values
	 */
	public String getSearchValues()
	{
		return searchValues;
	}

	/**
	 * @param searchValues  search values
	 */
	public void setSearchValues(String searchValues)
	{
		this.searchValues = searchValues;
	}

}
