package shared.communication;

import java.io.Serializable;

/**Holds information required for getting a batch/image from the database
 *
 */
@SuppressWarnings("serial")
public class BatchGet implements Serializable
{
	private String username;
	private String password;
	private int parentId;

	/**
	 * @param name
	 * @param pass
	 * @param parenId
	 */
	public BatchGet(String name, String pass, int parenId)
	{
		username = name;
		password = pass;
		parentId = parenId;
	}

	/**
	 * @return 
	 */
	public String getUsername()
	{
		return username;
	}

	/**
	 * @param username 
	 */
	public void setUsername(String username)
	{
		this.username = username;
	}

	/**
	 * @return 
	 */
	public String getPassword()
	{
		return password;
	}

	/**
	 * @param password 
	 */
	public void setPassword(String password)
	{
		this.password = password;
	}

	/**
	 * @return  project id
	 */
	public int getProjectId()
	{
		return parentId;
	}

	/**
	 * @param projectId  project id
	 */
	public void setProjectId(int projectId)
	{
		this.parentId = projectId;
	}
}
