package shared.communication;

import java.io.Serializable;

/**Hold input information required to submit batches/images to database
 *
 */
@SuppressWarnings("serial")
public class BatchSubmit implements Serializable
{
	private String username;
	private String password;
	private int imageId;
	//may be empty, values delimited by commas and records delimited by semicolon, this is going from top to bottom and lef to right
	private String recordValues;

	/**
	 * @param name
	 * @param pass
	 * @param imaId
	 * @param recV
	 */
	public BatchSubmit(String name, String pass, int imaId, String recV)
	{
		username=name;
		password=pass;
		imageId=imaId;
		recordValues=recV;
	}

	/**
	 * @return
	 */
	public String getUsername()
	{
		return username;
	}

	/**
	 * @param username 
	 */
	public void setUsername(String username)
	{
		this.username = username;
	}

	/**
	 * @return 
	 */
	public String getPassword()
	{
		return password;
	}

	/**
	 * @param password 
	 */
	public void setPassword(String password)
	{
		this.password = password;
	}

	/**
	 * @return  image id
	 */
	public int getImageId()
	{
		return imageId;
	}

	/**
	 * @param imageId  image id
	 */
	public void setImageId(int imageId)
	{
		this.imageId = imageId;
	}

	/**
	 * @return  record values separated by commas and records separated by semicolon
	 */
	public String getRecordValues()
	{
		return recordValues;
	}

	/**
	 * @param recordValues  record values separated by commas and records separated by semicolon
	 */
	public void setRecordValues(String recordValues)
	{
		this.recordValues = recordValues;
	}

}
