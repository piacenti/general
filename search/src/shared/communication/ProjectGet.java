package shared.communication;

import java.io.Serializable;

/**Holds input information required for getting Projects
 *
 */
@SuppressWarnings("serial")
public class ProjectGet implements Serializable
{
	private String username;
	private String password;

	/**
	 * @param name
	 * @param pass
	 */
	public ProjectGet(String name, String pass)
	{
		username=name;
		password=pass;
	}

	/**
	 * @return 
	 */
	public String getUsername()
	{
		return username;
	}

	/**
	 * @param username 
	 */
	public void setUsername(String username)
	{
		this.username = username;
	}

	/**
	 * @return 
	 */
	public String getPassword()
	{
		return password;
	}

	/**
	 * @param password 
	 */
	public void setPassword(String password)
	{
		this.password = password;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "ProjectGet [username=" + username + ", password=" + password + "]";
	}
	
}
