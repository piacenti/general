package shared.model;

/** Exceptions for model classes
 *
 */
@SuppressWarnings("serial")
public class ModelException extends Exception {

	public ModelException() {
		return;
	}

	/**
	 * @param message  
	 */
	public ModelException(String message) {
		super(message);
	}

	/**
	 * @param cause  
	 */
	public ModelException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message 
	 * @param cause 
	 */
	public ModelException(String message, Throwable cause) {
		super(message, cause);
	}

}
