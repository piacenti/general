package shared.model;

import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.*;
import javax.xml.xpath.*;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

import server.database.Database;
import server.database.DatabaseException;

/**
 * Handles importing of files containing data (XML) that can be added to the
 * database
 * 
 */
public class DataImporter
{
	List<Project> projects;
	List<User> users;

	public static void main(String[] args) throws XPathExpressionException, ParserConfigurationException, SAXException, IOException, ZipException
	{

		if (args.length == 1)
		{
			DataImporter dataimporter = new DataImporter();
			dataimporter.unzipReplace(args[0]);
			dataimporter.parseXML();
		}
		else
		{
			return;
		}

	}

	/**
	 * Delete data files from root folder and unzip contents of a filed data
	 * directory into the root folder
	 * 
	 * @param string
	 * @throws IOException
	 * @throws ZipException
	 */
	private void unzipReplace(String path) throws IOException, ZipException
	{

		String root = System.getProperty("user.dir");
		String[] temp = path.split("\\.");
		String fileType = temp[temp.length - 1];
		if (fileType.equals("zip"))
		{
			// delete images, knowndata and fieldhelp folders  and Records.xml file from root project folder
			new File("images").delete();
			new File("knowndata").delete();
			new File("fieldhelp").delete();
			new File("Records.xml").delete();

			ZipFile zipFile = new ZipFile(path);

			zipFile.extractAll(root);
			//copy relevant content from Records to root project directory
			FileUtils.copyDirectory(new File("Records"), new File(root));
			FileUtils.deleteDirectory(new File("Records"));
		}
		else if (fileType.equals("xml"))
		{
			// delete  Records.xml file from root project folder and replace with given xml while renaming it to Record.xml
			FileUtils.deleteDirectory(new File("images"));
			FileUtils.deleteDirectory(new File("knowndata"));
			FileUtils.deleteDirectory(new File("fieldhelp"));
			new File("Records.xml").delete();

			File file = new File(path);
			//copy remaining folders to current folder
			FileUtils.copyDirectory(file.getParentFile(), new File(root));

			//get file name
			String[] pathArray = path.split("(\\\\|/)");
			File oldFileName = new File(pathArray[pathArray.length - 1]);
			oldFileName.renameTo(new File("Records.xml"));

		}
	}

	/**
	 * Parse XML from file
	 * 
	 * @param filePath
	 * 
	 * @throws ParserConfigurationException
	 * @throws IOException
	 * @throws SAXException
	 * @throws XPathExpressionException
	 * 
	 */
	public void parseXML() throws ParserConfigurationException, SAXException, IOException, XPathExpressionException
	{
		DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		//"src" + File.separator + "testRecords" + File.separator + "Records.xml"
		File file = new File("Records.xml");
		Document doc = builder.parse(file);

		XPath xpath = XPathFactory.newInstance().newXPath();

		NodeList userNameList = (NodeList) xpath.evaluate("/indexerdata/users/user/username", doc, XPathConstants.NODESET);
		NodeList userPassList = (NodeList) xpath.evaluate("/indexerdata/users/user/password", doc, XPathConstants.NODESET);
		NodeList userFNameList = (NodeList) xpath.evaluate("/indexerdata/users/user/firstname", doc, XPathConstants.NODESET);
		NodeList userLNameList = (NodeList) xpath.evaluate("/indexerdata/users/user/lastname", doc, XPathConstants.NODESET);
		NodeList userEmailList = (NodeList) xpath.evaluate("/indexerdata/users/user/email", doc, XPathConstants.NODESET);
		NodeList userIndexRList = (NodeList) xpath.evaluate("/indexerdata/users/user/indexedrecords", doc, XPathConstants.NODESET);
		users = new ArrayList<>();
		for (int i = 0; i < userNameList.getLength(); ++i)
		{
			String username = (((Element) userNameList.item(i)).getTextContent()).trim();
			String password = (((Element) userPassList.item(i)).getTextContent()).trim();
			String firstName = (((Element) userFNameList.item(i)).getTextContent()).trim();
			String lastName = (((Element) userLNameList.item(i)).getTextContent()).trim();
			String email = ((Element) userEmailList.item(i)).getTextContent();
			int indexedRecords = Integer.parseInt((((Element) userIndexRList.item(i)).getTextContent()).trim());
			User user = new User(username, password, firstName, lastName, email, indexedRecords, User.NO_CURRENT_IMAGE);
			users.add(user);
		}

		NodeList projectTitle = (NodeList) xpath.evaluate("/indexerdata/projects/project/title", doc, XPathConstants.NODESET);
		NodeList recordsPerImage = (NodeList) xpath.evaluate("/indexerdata/projects/project/recordsperimage", doc, XPathConstants.NODESET);
		NodeList firstYCoord = (NodeList) xpath.evaluate("/indexerdata/projects/project/firstycoord", doc, XPathConstants.NODESET);
		NodeList recordHeight = (NodeList) xpath.evaluate("/indexerdata/projects/project/recordheight", doc, XPathConstants.NODESET);

		//get list of projects
		List<Project> projects = new ArrayList<>();

		for (int a = 0; a < projectTitle.getLength(); ++a)
		{
			//set project
			String ptitle = ((Element) projectTitle.item(a)).getTextContent();
			int pRecs = Integer.parseInt((((Element) recordsPerImage.item(a)).getTextContent().trim()));
			int pYcoord = Integer.parseInt((((Element) firstYCoord.item(a)).getTextContent().trim()));
			int pRecH = Integer.parseInt((((Element) recordHeight.item(a)).getTextContent().trim()));
			Project project = new Project(ptitle, pYcoord, pRecH, Project.NOT_ASSIGNED, Project.NOT_COMPLETED, pRecs);
			//get fields
			NodeList fieldTitle = (NodeList) xpath.evaluate("/indexerdata/projects/project[" + (a + 1) + "]/fields/field/title", doc, XPathConstants.NODESET);
			NodeList xCoord = (NodeList) xpath.evaluate("/indexerdata/projects/project[" + (a + 1) + "]/fields/field/xcoord", doc, XPathConstants.NODESET);
			NodeList width = (NodeList) xpath.evaluate("/indexerdata/projects/project[" + (a + 1) + "]/fields/field/width", doc, XPathConstants.NODESET);
			NodeList help = (NodeList) xpath.evaluate("/indexerdata/projects/project[" + (a + 1) + "]/fields/field/helphtml", doc, XPathConstants.NODESET);
			NodeList known = (NodeList) xpath.evaluate("/indexerdata/projects/project[" + (a + 1) + "]/fields/field/knowndata", doc, XPathConstants.NODESET);
			List<Field> fields = new ArrayList<>();
			for (int b = 0; b < fieldTitle.getLength(); ++b)
			{
				String ftitle = ((Element) fieldTitle.item(b)).getTextContent().trim();
				int fxC = Integer.parseInt((((Element) xCoord.item(b)).getTextContent().trim()));
				int fwidth = Integer.parseInt((((Element) width.item(b)).getTextContent().trim()));
				String fhelp = ((Element) help.item(b)).getTextContent().trim();
				if (((Element) known.item(b)) != null)
				{
					String fknown = ((Element) known.item(b)).getTextContent().trim();
					fields.add(new Field(ftitle, fxC, fwidth, fhelp, fknown));
				}
				else
				{
					fields.add(new Field(ftitle, fxC, fwidth, fhelp, ""));
				}
			}

			//get images
			NodeList imagePath = (NodeList) xpath.evaluate("/indexerdata/projects/project[" + (a + 1) + "]/images/image/file", doc, XPathConstants.NODESET);
			List<Image> images = new ArrayList<>();

			for (int b = 0; b < imagePath.getLength(); ++b)
			{
				//get records
				NodeList recordList = (NodeList) xpath.evaluate("/indexerdata/projects/project[" + (a + 1) + "]/images/image[" + (b + 1) + "]/records/record",
						doc, XPathConstants.NODESET);
				List<Record> records = new ArrayList<>();

				boolean complete = false;
				for (int c = 0; c < recordList.getLength(); c++)
				{
					//get values
					NodeList valueList = (NodeList) xpath.evaluate("/indexerdata/projects/project[" + (a + 1) + "]/images/image[" + (b + 1)
							+ "]/records/record[" + (c + 1) + "]/values/value", doc, XPathConstants.NODESET);
					Record record = new Record();
					
					for (int d = 0; d < valueList.getLength(); d++)
					{
						if (((Element) valueList.item(d)) != null)
						{
							complete=true;
							record.getValues().add(((Element) valueList.item(d)).getTextContent().trim());
						}
						else
						{
							record.getValues().add(" ");
						}
					}
					records.add(record);
				}
				if (complete == false)
				{
					images.add(new Image(0, ((Element) imagePath.item(b)).getTextContent().trim(), Image.NO_HEIGHT, Image.NO_WIDTH, Image.NOT_ASSIGNED,
							Image.NOT_COMPLETED, records));
				}
				else
				{
					images.add(new Image(0, ((Element) imagePath.item(b)).getTextContent().trim(), Image.NO_HEIGHT, Image.NO_WIDTH, Image.NOT_ASSIGNED,
							1, records));
				}
			}

			project.setImages(images);
			project.setFields(fields);

			project.makeProjectValid();

			projects.add(project);
		}
		try
		{
			Database.recreateTables();
		}
		catch (SQLException | DatabaseException e1)
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		addProjectsToDatabase(projects);
		addUsersToDatabase(users);

	}

	/**
	 * Add projects to database
	 * 
	 * @throws IOException
	 */
	private void addProjectsToDatabase(List<Project> projects) throws IOException
	{
		try
		{
			Project.databaseAddProjects(projects);
		}
		catch (SQLException | DatabaseException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Add users to database
	 * 
	 * @throws IOException
	 */
	private void addUsersToDatabase(List<User> users) throws IOException
	{
		try
		{
			User.databaseAddUsers(users);
		}
		catch (SQLException | DatabaseException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
