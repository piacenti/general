package shared.communication;

import java.io.Serializable;

/**Holds input information for user validation
 *
 */
@SuppressWarnings("serial")
public class Validate implements Serializable
{
	private String username;
	private String password;

	/**
	 * @param string
	 * @param string2
	 */
	public Validate(String uname, String pass)
	{
		username=uname;
		password=pass;
	}

	/**
	 * @return 
	 */
	public String getUsername()
	{
		return username;
	}

	/**
	 * @param username 
	 */
	public void setUsername(String username)
	{
		this.username = username;
	}

	/**
	 * @return 
	 */
	public String getPassword()
	{
		return password;
	}

	/**
	 * @param password 
	 */
	public void setPassword(String password)
	{
		this.password = password;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "Validate [username=" + username + ", password=" + password + "]";
	}

}
