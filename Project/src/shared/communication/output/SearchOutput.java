package shared.communication.output;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Contain result information from search
 * 
 */
@SuppressWarnings("serial")
public class SearchOutput implements Serializable
{
	private List<SearchResult> results;

	public SearchOutput()
	{
		results = new ArrayList<>();
	}

	/**
	 * @return search results
	 */
	public List<SearchResult> getResults()
	{
		return results;
	}

	/**
	 * @param results
	 *            search results
	 */
	public void setResults(List<SearchResult> results)
	{
		this.results = results;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		StringBuilder string = new StringBuilder();
		for(SearchResult result:results)
		{
			string.append(result.toString());
		}
		return string.toString();
	}
	
	public void setHostPort(String host, String port)
	{
		String urlPrepend = "http:"+File.separator+File.separator+host+":"+port+File.separator;
		for(SearchResult result:results)
		{
			result.setImageUrl(urlPrepend+result.getImageUrl());
		}
	}
	
}
