package shared.communication.output;

import java.io.Serializable;
import java.util.List;
import shared.model.Project;

/**
 * Holds result information acquired after a ProjectGet request
 * 
 */
@SuppressWarnings("serial")
public class ProjectGetOutput implements Serializable
{
	private StringBuilder result;

	/**
	 * @param projects
	 */
	public ProjectGetOutput(List<Project> projects)
	{
		result = new StringBuilder();
		for (Project project : projects)
		{
			result.append(project.getId() + "\n");
			result.append(project.getTitle() + "\n");
		}
	}

	public String toString()
	{
		return result.toString();

	}

}
