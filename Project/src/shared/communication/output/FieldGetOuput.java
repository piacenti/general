package shared.communication.output;

import java.io.Serializable;
import java.util.List;

import shared.model.Field;

/**
 * Holds result information obtained after a FieldGet request
 * 
 */
@SuppressWarnings("serial")
public class FieldGetOuput implements Serializable
{
	private List<Field> fields;

	/**
	 * @param fields_
	 */
	public FieldGetOuput(List<Field> fields_)
	{
		fields = fields_;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		StringBuilder string = new StringBuilder();
		for(Field field: fields)
		{
			string.append(field.getParentId()+"\n");
			string.append(field.getId()+"\n");
			string.append(field.getTitle()+"\n");
		}
		return string.toString();
	}

	/**
	 * @return
	 */
	public List<Field> getFields()
	{
		return fields;
	}

	/**
	 * @param fields
	 */
	public void setFields(List<Field> fields)
	{
		this.fields = fields;
	}

	/**
	 * @param port 
	 * @param host 
	 * 
	 */
	public void setHostPort(String host, String port)
	{
		for(Field field:fields)
		{
			field.setHostPort(host, port);
		}
		
	}
	public static void setHostPort(String host, String port, List<Field> fields)
	{
		for(Field field:fields)
		{
			field.setHostPort(host, port);
		}
		
	}

}
