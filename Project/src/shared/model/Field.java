package shared.model;

import java.io.File;
import java.io.Serializable;

/**
 * Holds information of field as contained in the database
 * 
 */
@SuppressWarnings("serial")
public class Field implements Serializable
{
	private int id;
	private int parentId;
	private int number;
	private String title;
	private int xCoord;
	private int width;
	private File helpPath;
	private File knownDataPath;
	private String host;
	private String port;

	public Field()
	{

	}

	/**
	 * @param i
	 * @param j
	 * @param title_
	 * @param xCoord_
	 * @param width_
	 * @param helpPath_
	 * @param kDPath
	 */
	public Field(String title_, int xCoord_, int width_, String helpPath_, String kDPath)
	{
		title = title_;
		xCoord = xCoord_;
		width = width_;
		helpPath = new File(helpPath_);
		knownDataPath = new File(kDPath);
	}

	/**
	 * @return
	 */
	public int getId()
	{
		return id;
	}

	/**
	 * @return the parentId
	 */
	public int getParentId()
	{
		return parentId;
	}

	/**
	 * @param parentId
	 *            the parentId to set
	 */
	public void setParentId(int parentId)
	{
		this.parentId = parentId;
	}

	/**
	 * @param id
	 */
	public void setId(int id)
	{
		this.id = id;
	}

	/**
	 * @return
	 */
	public int getNumber()
	{
		return number;
	}

	/**
	 * @param number
	 */
	public void setNumber(int number)
	{
		this.number = number;
	}

	/**
	 * @return
	 */
	public String getTitle()
	{
		return title;
	}

	/**
	 * @param title
	 */
	public void setTitle(String title)
	{
		this.title = title;
	}

	/**
	 * @return x coordinate
	 */
	public int getxCoord()
	{
		return xCoord;
	}

	/**
	 * @param xCoord
	 *            x coordinate
	 */
	public void setxCoord(int xCoord)
	{
		this.xCoord = xCoord;
	}

	/**
	 * @return width
	 */
	public int getWidth()
	{
		return width;
	}

	/**
	 * @param width
	 *            width
	 */
	public void setWidth(int width)
	{
		this.width = width;
	}

	/**
	 * @return help file in File object
	 */
	public File getHelpPath()
	{
		return helpPath;
	}

	/**
	 * @param helpPath
	 *            help file in File object
	 */
	public void setHelpPath(File helpPath)
	{
		this.helpPath = helpPath;
	}

	/**
	 * @return known data file in File object
	 */
	public File getKnownDataPath()
	{
		return knownDataPath;
	}

	/**
	 * @param knownDataPath
	 *            File known data file File object
	 */
	public void setKnownDataPath(File knownDataPath)
	{
		this.knownDataPath = knownDataPath;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null)
		{
			return false;
		}
		if (this.getClass() != o.getClass())
		{
			return false;
		}
		Field other = (Field) o;
		if (title.equals(other.getTitle()) == false || xCoord != other.getxCoord() || width != other.getWidth()
				|| helpPath.getPath().equals(other.getHelpPath().getPath()) == false
				|| knownDataPath.getPath().equals(other.getKnownDataPath().getPath()) == false)
		{
			return false;
		}

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		String urlPrepend = "http:" + File.separator + File.separator + host + ":" + port + File.separator;
		if (knownDataPath.toString().equals("") == false)
			return id + "\n" + number + "\n" + title + "\n" + urlPrepend + helpPath + "\n" + xCoord + "\n" + width + "\n" + urlPrepend + knownDataPath + "\n";
		else
			return id + "\n" + number + "\n" + title + "\n" + urlPrepend + helpPath + "\n" + xCoord + "\n" + width + "\n";
	}

	/**
	 * @param host
	 * @param port
	 */
	public void setHostPort(String host_, String port_)
	{
		host = host_;
		port = port_;

	}

}
