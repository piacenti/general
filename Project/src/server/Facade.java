/**
 * 
 */
package server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import server.database.Database;
import server.database.DatabaseException;
import shared.communication.BatchGet;
import shared.communication.BatchSubmit;
import shared.communication.FieldGet;
import shared.communication.ProjectGet;
import shared.communication.SampleGet;
import shared.communication.Search;
import shared.communication.Validate;
import shared.communication.output.BatchGetOutput;
import shared.communication.output.SearchOutput;
import shared.model.Field;
import shared.model.Image;
import shared.model.Project;
import shared.model.User;

public class Facade
{
	/**
	 * @param input
	 * @return
	 * @throws ServerException
	 */

	public HttpHandler validateUserHandler = new HttpHandler()
	{

		@Override
		public void handle(HttpExchange exchange) throws IOException
		{
			try
			{
				Database database = new Database();

				System.out.println("Validate User Request received");

				//get Validate object
				ObjectInputStream in = new ObjectInputStream(exchange.getRequestBody());
				Validate user = (Validate) in.readObject();

				//get user object
				database.startTransaction();
				User result = database.getUserDB().getUser(user);
				database.endTransaction();
				if (result != null)
				{

					exchange.sendResponseHeaders(200, 0);
					ObjectOutputStream out = new ObjectOutputStream(exchange.getResponseBody());
					out.writeObject(result);
					out.close();
				}
				else
				{
					exchange.sendResponseHeaders(200, 0);
					ObjectOutputStream out = new ObjectOutputStream(exchange.getResponseBody());
					out.writeObject("empty");
					out.close();
				}

			}
			catch (DatabaseException | ClassNotFoundException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	};

	public HttpHandler getProjectsHandler = new HttpHandler()
	{

		@Override
		public void handle(HttpExchange exchange) throws IOException
		{

			try
			{
				Database database = new Database();

				System.out.println("Get Projects Request received");

				//get ProjectGet object
				ObjectInputStream in = new ObjectInputStream(exchange.getRequestBody());
				ProjectGet userInfo = (ProjectGet) in.readObject();

				//get list of projects object
				database.startTransaction();
				List<Project> result = database.getProjectDB().getProjects(userInfo);

				database.endTransaction();

				exchange.sendResponseHeaders(200, 0);
				if (result != null && result.size() != 0)
				{
					ObjectOutputStream out = new ObjectOutputStream(exchange.getResponseBody());
					out.writeObject(result);
					out.close();
				}
				else
				{
					ObjectOutputStream out = new ObjectOutputStream(exchange.getResponseBody());
					out.writeObject("empty");
					out.close();
				}

			}
			catch (DatabaseException | ClassNotFoundException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	};

	public HttpHandler getSampleImageHandler = new HttpHandler()
	{

		@Override
		public void handle(HttpExchange exchange) throws IOException
		{

			try
			{
				Database database = new Database();

				System.out.println("Get Sample Image Request received");

				//get SampleGet object
				ObjectInputStream in = new ObjectInputStream(exchange.getRequestBody());
				SampleGet userInfo = (SampleGet) in.readObject();

				//get Image object
				database.startTransaction();
				Image result = database.getImageDB().getSampeImage(userInfo);
				database.endTransaction();
				exchange.sendResponseHeaders(200, 0);
				if (result != null)
				{

					ObjectOutputStream out = new ObjectOutputStream(exchange.getResponseBody());
					out.writeObject(result);
					out.close();
				}
				else
				{
					ObjectOutputStream out = new ObjectOutputStream(exchange.getResponseBody());
					out.writeObject("empty");
					out.close();
				}

			}
			catch (DatabaseException | ClassNotFoundException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	};

	public HttpHandler downloadBatchHandler = new HttpHandler()
	{

		@Override
		public void handle(HttpExchange exchange) throws IOException
		{

			try
			{
				Database database = new Database();

				System.out.println("Download Batch Request received");

				//get BatchGet object
				ObjectInputStream in = new ObjectInputStream(exchange.getRequestBody());
				BatchGet userInfo = (BatchGet) in.readObject();

				//get Image object
				database.startTransaction();
				BatchGetOutput result = database.getImageDB().downloadBatch(userInfo);

				database.endTransaction();

				exchange.sendResponseHeaders(200, 0);
				if (result != null)
				{
					ObjectOutputStream out = new ObjectOutputStream(exchange.getResponseBody());
					out.writeObject(result);
					out.close();
				}
				else
				{
					ObjectOutputStream out = new ObjectOutputStream(exchange.getResponseBody());
					out.writeObject("empty");
					out.close();
				}
			}
			catch (DatabaseException | ClassNotFoundException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	};

	public HttpHandler submitBatchHandler = new HttpHandler()
	{

		@Override
		public void handle(HttpExchange exchange) throws IOException
		{

			try
			{
				Database database = new Database();

				System.out.println("Submit Batch Request received");

				//get BatchSubmit object
				ObjectInputStream in = new ObjectInputStream(exchange.getRequestBody());
				BatchSubmit userInfo = (BatchSubmit) in.readObject();

				//get Image object
				database.startTransaction();
				boolean result = database.getImageDB().submitBatch(userInfo);

				database.endTransaction();

				exchange.sendResponseHeaders(200, 0);
				ObjectOutputStream out = new ObjectOutputStream(exchange.getResponseBody());
				out.writeObject(result);
				out.close();

			}
			catch (DatabaseException | ClassNotFoundException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	};

	public HttpHandler getFieldsHandler = new HttpHandler()
	{

		@Override
		public void handle(HttpExchange exchange) throws IOException
		{

			try
			{
				Database database = new Database();

				System.out.println("Get Field Request received");

				//get FieldGet object
				ObjectInputStream in = new ObjectInputStream(exchange.getRequestBody());
				FieldGet userInfo = (FieldGet) in.readObject();

				//get Image object
				database.startTransaction();
				List<Field> result = database.getFieldsDB().getFields(userInfo);

				database.endTransaction();

				exchange.sendResponseHeaders(200, 0);
				if (result!=null && result.size() != 0)
				{
					ObjectOutputStream out = new ObjectOutputStream(exchange.getResponseBody());
					out.writeObject(result);
					out.close();
				}
				else
				{
					ObjectOutputStream out = new ObjectOutputStream(exchange.getResponseBody());
					out.writeObject("empty");
					out.close();
				}

			}
			catch (DatabaseException | ClassNotFoundException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	};

	public HttpHandler searchHandler = new HttpHandler()
	{

		@Override
		public void handle(HttpExchange exchange) throws IOException
		{

			try
			{
				Database database = new Database();

				System.out.println("Search Request received");

				//get Search object
				ObjectInputStream in = new ObjectInputStream(exchange.getRequestBody());
				Search userInfo = (Search) in.readObject();

				//get Image object
				database.startTransaction();
				SearchOutput result = database.search(userInfo);

				database.endTransaction();

				exchange.sendResponseHeaders(200, 0);
				if (result != null)
				{
					ObjectOutputStream out = new ObjectOutputStream(exchange.getResponseBody());
					out.writeObject(result);
					out.close();
				}
				else
				{
					ObjectOutputStream out = new ObjectOutputStream(exchange.getResponseBody());
					out.writeObject("empty");
					out.close();
				}

			}
			catch (DatabaseException | ClassNotFoundException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	};

	public HttpHandler downloadFileHandler = new HttpHandler()
	{

		@Override
		public void handle(HttpExchange exchange) throws IOException
		{

			System.out.println("Downloading File");
			String path = exchange.getRequestURI().toString().substring(1);

			Path filePath = FileSystems.getDefault().getPath(path);

			exchange.sendResponseHeaders(200, 0);
			Files.copy(filePath, exchange.getResponseBody());
			exchange.getResponseBody().close();

		}
	};
}
