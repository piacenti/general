package server.database;

/**Database exceptions
 *
 */
@SuppressWarnings("serial")
public class DatabaseException extends Exception
{

	/**
	 * Default database exception
	 */
	public DatabaseException()
	{
		return;
	}

	/** Exception with message
	 * @param message 
	 */
	public DatabaseException(String message)
	{
		super(message);
	}

	/**
	 * @param cause Throwable
	 */
	public DatabaseException(Throwable cause)
	{
		super(cause);

	}

	/**
	 * @param message 
	 * @param cause  
	 */
	public DatabaseException(String message, Throwable cause)
	{
		super(message, cause);
	}

}
