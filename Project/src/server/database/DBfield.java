package server.database;

import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import shared.communication.FieldGet;
import shared.model.Field;

/**
 * Perform field database operations
 * 
 */
public class DBfield
{
	private Database database;

	public DBfield(Database databaseInput)
	{
		database = databaseInput;
	}

	/**
	 * Get fields from the database
	 * 
	 * @param input
	 * @return
	 */
	public List<Field> getFields(FieldGet input)
	{

		PreparedStatement stmt = null;
		List<Field> fields = null;
		Field field = null;
		ResultSet result = null;
		if (database.getUserDB().validateUser(input.getUsername(), input.getPassword()) == true)
		{
			try
			{
				String sql;
				//FieldGet may or may not have a value for parent id, if it does not get all fields for all 
				// projects
				if (input.getProjectId() > 0)
				{
					sql = "SELECT fields.* FROM project LEFT JOIN fields ON project.id=fields.parentId WHERE project.id=?";
					stmt = database.getConnection().prepareStatement(sql);
					stmt.setInt(1, input.getProjectId());
				}
				else
				{
					sql = "SELECT * FROM fields";
					stmt = database.getConnection().prepareStatement(sql);
				}

				fields = new ArrayList<>();
				result = stmt.executeQuery();
				while (result.next())
				{
					field = new Field();
					field.setId(result.getInt("id"));
					field.setParentId(result.getInt("parentId"));
					field.setNumber(result.getInt("number"));
					field.setTitle(result.getString("title"));
					field.setxCoord(result.getInt("xCoord"));
					field.setWidth(result.getInt("width"));
					field.setHelpPath(new File(result.getString("help")));
					field.setKnownDataPath(new File(result.getString("knownData")));

					fields.add(field);
				}

			}
			catch (SQLException e)
			{

			}
			finally
			{

				if (result != null)
					try
					{
						result.close();
					}
					catch (SQLException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				if (stmt != null)
					try
					{
						stmt.close();
					}
					catch (SQLException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
		}
		return fields;
	}

	/**
	 * Get fields based on the project id to which they belong
	 * 
	 * @param parentId
	 * @return
	 */
	public List<Field> getFields(int parentId)
	{

		PreparedStatement stmt = null;
		List<Field> fields = null;
		Field field = null;
		ResultSet result = null;

		try
		{
			String sql = "SELECT fields.* FROM project LEFT JOIN fields ON project.id=fields.parentId " + "WHERE project.id=?";
			stmt = database.getConnection().prepareStatement(sql);
			stmt.setInt(1, parentId);

			result = stmt.executeQuery();
			fields = new ArrayList<>();
			while (result.next())
			{
				field = new Field();
				field.setId(result.getInt("id"));
				field.setParentId(result.getInt("parentId"));
				field.setNumber(result.getInt("number"));
				field.setTitle(result.getString("title"));
				field.setxCoord(result.getInt("xCoord"));
				field.setWidth(result.getInt("width"));
				field.setHelpPath(new File(result.getString("help")));
				field.setKnownDataPath(new File(result.getString("knownData")));

				fields.add(field);
			}

		}
		catch (SQLException e)
		{

		}
		finally
		{

			if (result != null)
				try
				{
					result.close();
				}
				catch (SQLException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			if (stmt != null)
				try
				{
					stmt.close();
				}
				catch (SQLException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		return fields;
	}

	/**
	 * Add fields to the database
	 * 
	 * @param input
	 * @return true for successful operation, false otherwise
	 */
	public boolean addFields(List<Field> input)
	{

		PreparedStatement stmt = null;
		try
		{
			String sql = "INSERT INTO fields (parentId, number, title, xCoord, width, help, knownData)" + " VALUES " + "(?, ?, ?, ?, ?, ?, ?)";

			stmt = database.getConnection().prepareStatement(sql);
			for (Field field : input)
			{
				stmt.setInt(1, field.getParentId());
				stmt.setInt(2, field.getNumber());
				stmt.setString(3, field.getTitle());
				stmt.setInt(4, field.getxCoord());
				stmt.setInt(5, field.getWidth());
				stmt.setString(6, field.getHelpPath().getPath());
				stmt.setString(7, field.getKnownDataPath().getPath());
				stmt.addBatch();
			}

			int[] test = stmt.executeBatch();
			for (int n : test)
			{
				if (n == PreparedStatement.EXECUTE_FAILED)
				{
					{
						database.setError(true);
						return false;
					}
				}
			}
			return true;
		}
		catch (SQLException e)
		{

		}
		finally
		{

			if (stmt != null)
				try
				{
					stmt.close();
				}
				catch (SQLException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}

		{
			database.setError(true);
			return false;
		}
	}

}
