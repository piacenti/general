package server.database;

import java.sql.*;
import java.util.List;

import shared.communication.Validate;
import shared.model.User;

/**
 * Perform user database operations
 * 
 */
public class DBuser
{
	private Database database;

	public DBuser(Database databaseInput) throws DatabaseException
	{
		database = databaseInput;
	}

	public boolean validateUser(String username, String password)
	{
		Validate user_ = new Validate(username, password);
		User user = getUser(user_);
		if (user == null)
			{database.setError(true); return false;}
		return true;
	}

	/**
	 * Gets user
	 * 
	 * @param input
	 * @return
	 */
	public User getUser(Validate input)
	{

		PreparedStatement stmt = null;
		User user = null;
		ResultSet result = null;
		try
		{
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM users WHERE username=? AND password=?");

			stmt = database.getConnection().prepareStatement(sql.toString());
			stmt.setString(1, input.getUsername());
			stmt.setString(2, input.getPassword());

			result = stmt.executeQuery();
			while (result.next())
			{
				user = new User();
				user.setId(result.getInt("id"));
				user.setUsername(result.getString("username"));
				user.setPassword(result.getString("password"));
				user.setFirstName(result.getString("firstName"));
				user.setLastName(result.getString("lastName"));
				user.setEmail(result.getString("email"));
				user.setCurrentImage(result.getInt("currentImage"));
				user.setIndexedRecords(result.getInt("indexedRecords"));
			}
		}
		catch (SQLException e)
		{

		}
		finally
		{

			if (result != null)
				try
				{
					result.close();
				}
				catch (SQLException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			if (stmt != null)
				try
				{
					stmt.close();
				}
				catch (SQLException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		return user;

	}

	/**
	 * Add users to database
	 * 
	 * @param input
	 * @return true for successful opeartion false otherwise
	 */
	public boolean addUsers(List<User> input)
	{

		for (User user : input)
		{
			if (addUser(user) == false)
				{database.setError(true); return false;}

		}
		return true;
	}

	public boolean addUser(User input)
	{

		PreparedStatement stmt = null;
		ResultSet result = null;
		try
		{
			String sql = "SELECT COUNT(*) FROM users WHERE username=? AND password=?";
			stmt = database.getConnection().prepareStatement(sql);
			stmt.setString(1, input.getUsername());
			stmt.setString(2, input.getPassword());

			result = stmt.executeQuery();
			result.next();
			if (result.getInt(1) == 0)
			{
				stmt.close();

				sql = "INSERT INTO users (username, password, firstName, lastName, email, currentImage, indexedRecords) VALUES " + "(?, ?, ?, ?, ?, 0, ?) ";

				stmt = database.getConnection().prepareStatement(sql);
				stmt.setString(1, input.getUsername());
				stmt.setString(2, input.getPassword());
				stmt.setString(3, input.getFirstName());
				stmt.setString(4, input.getLastName());
				stmt.setString(5, input.getEmail());
				stmt.setInt(6, input.getIndexedRecords());

				if (stmt.executeUpdate() == 1)
				{

					return true;
				}
				else
				{
					{database.setError(true); return false;}
				}
			}
		}
		catch (SQLException e)
		{

		}
		finally
		{

			if (stmt != null)
				try
				{
					stmt.close();
				}
				catch (SQLException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}

		{database.setError(true); return false;}
	}

	//likely not to be used since users are updated by getbatch and submitbatch
	/**
	 * Update users in database
	 * 
	 * @param input
	 * @return true for successful opeartion false otherwise
	 */
	public boolean updateUsers(List<User> input)
	{

		PreparedStatement stmt = null;
		PreparedStatement qstmt = null;
		ResultSet result;
		try
		{
			String sql = "UPDATE users SET username=?, password=?, firstName=?, lastName=?, email=?," + " currentImage=?, indexedRecords=? WHERE id=?";

			stmt = database.getConnection().prepareStatement(sql);
			for (User user : input)
			{
				String sqlQ = "SELECT COUNT(*) FROM users WHERE username=? AND password=?";
				qstmt = database.getConnection().prepareStatement(sqlQ);
				qstmt.setString(1, user.getUsername());
				qstmt.setString(2, user.getPassword());

				result = qstmt.executeQuery();
				//result.next();
				if (result.getInt(1) == 0)
				{
					stmt.setString(1, user.getUsername());
					stmt.setString(2, user.getPassword());
					stmt.setString(3, user.getFirstName());
					stmt.setString(4, user.getLastName());
					stmt.setString(5, user.getEmail());
					stmt.setInt(6, user.getCurrentImage());
					stmt.setInt(7, user.getIndexedRecords());
					stmt.setInt(8, user.getId());
					stmt.addBatch();
				}
				else
				{
					{database.setError(true); return false;}
				}
				result.close();
				qstmt.close();
			}
			int[] test = stmt.executeBatch();
			for (int n : test)
			{
				if (n == PreparedStatement.EXECUTE_FAILED)
				{
					{database.setError(true); return false;}
				}
			}
			return true;

		}
		catch (SQLException e)
		{

		}
		finally
		{

			if (stmt != null)
				try
				{
					stmt.close();
				}
				catch (SQLException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}

		{
			database.setError(true);
			return false;
		}
	}

}
