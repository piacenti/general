package server.database;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import shared.communication.ProjectGet;
import shared.model.Field;
import shared.model.Image;
import shared.model.Project;

/**
 * Perform project operations in the database
 * 
 */
public class DBproject
{
	private Database database;

	public DBproject(Database databaseInput)
	{
		database = databaseInput;
	}

	/**
	 * Get all projects from database
	 * 
	 * @return
	 */
	//type is 0 for only valid projects and 1 for all projects
	public List<Project> getProjects(int type)
	{

		PreparedStatement stmt = null;
		List<Project> projects = null;
		Project project = null;
		ResultSet result = null;

		try
		{
			String sql = "";
			if (type == 0)
			{
				sql = "SELECT * FROM project WHERE completed IN (0,NULL)";
			}
			else
			{
				sql = "SELECT * FROM project";
			}

			stmt = database.getConnection().prepareStatement(sql.toString());

			result = stmt.executeQuery();
			projects = new ArrayList<>();

			while (result.next())
			{
				project = new Project();

				project.setId(result.getInt("id"));
				project.setTitle(result.getString("title"));
				project.setRecordsPerImage(result.getInt("recordsPerImage"));
				project.setFirstYCoord(result.getInt("firstYCoord"));
				project.setRecordHeight(result.getInt("recordHeight"));
				project.setAssigned(result.getInt("assigned"));
				project.setCompleted(result.getInt("completed"));
				//add projects to list of projects to return
				projects.add(project);
			}

			for (Project proj : projects)
			{
				// get images by parent id
				proj.setImages(database.getImageDB().getImages(proj.getId()));
				// get fields by parent id
				proj.setFields(database.getFieldsDB().getFields(proj.getId()));
			}
		}
		catch (SQLException e)
		{

		}
		finally
		{

			if (result != null)
				try
				{
					result.close();
				}
				catch (SQLException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			if (stmt != null)
				try
				{
					stmt.close();
				}
				catch (SQLException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		return projects;
	}
/**
 * Gets all projects from database
 * @param userInfo
 * @return
 */
	public List<Project> getProjects(ProjectGet userInfo)
	{

		PreparedStatement stmt = null;
		List<Project> projects = null;
		Project project = null;
		ResultSet result = null;
		if (database.getUserDB().validateUser(userInfo.getUsername(), userInfo.getPassword()) == true)
		{
			try
			{
				String sql = "SELECT * FROM project WHERE completed IN (0,NULL)";

				stmt = database.getConnection().prepareStatement(sql.toString());

				result = stmt.executeQuery();
				projects = new ArrayList<>();

				while (result.next())
				{
					project = new Project();

					project.setId(result.getInt("id"));
					project.setTitle(result.getString("title"));
					project.setRecordsPerImage(result.getInt("recordsPerImage"));
					project.setFirstYCoord(result.getInt("firstYCoord"));
					project.setRecordHeight(result.getInt("recordHeight"));
					project.setAssigned(result.getInt("assigned"));
					project.setCompleted(result.getInt("completed"));
					//add projects to list of projects to return
					projects.add(project);
				}

				for (Project proj : projects)
				{
					// get images by parent id
					proj.setImages(database.getImageDB().getImages(proj.getId()));
					// get fields by parent id
					proj.setFields(database.getFieldsDB().getFields(proj.getId()));
				}
			}
			catch (SQLException e)
			{

			}
			finally
			{

				if (result != null)
					try
					{
						result.close();
					}
					catch (SQLException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				if (stmt != null)
					try
					{
						stmt.close();
					}
					catch (SQLException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
		}
		return projects;
	}

	/**
	 * Add a list of projects to database
	 * 
	 * @param input
	 * @return true for successful operation, false otherwise
	 */
	public boolean addProjects(List<Project> input)
	{
		for (Project project : input)
		{
			if (addProject(project) == false)
				{database.setError(true);return false;}
		}
		return true;
	}
/**
 * Add a project to the database
 * @param input
 * @return
 */
	public boolean addProject(Project input)
	{
		PreparedStatement stmt = null;
		Statement keyStmt = null;
		ResultSet keyRS = null;

		try
		{
			String sql = "INSERT INTO project (title, recordsPerImage, firstYCoord, recordHeight, assigned, completed) " + "Values (? ,? ,? ,? , 0, 0)";
			stmt = database.getConnection().prepareStatement(sql);
			stmt.setString(1, input.getTitle());
			stmt.setInt(2, input.getRecordsPerImage());
			stmt.setInt(3, input.getFirstYCoord());
			stmt.setInt(4, input.getRecordHeight());

			if (stmt.executeUpdate() == 1)
			{
				keyStmt = database.getConnection().createStatement();
				keyRS = keyStmt.executeQuery("select last_insert_rowid()");
				keyRS.next();
				//gets id of new project
				int projectId = keyRS.getInt(1);
				keyRS.close();

				//assign this id as parent id of all images and fields
				for (Image image : input.getImages())
				{
					image.setParentId(projectId);
				}
				for (Field field : input.getFields())
				{
					field.setParentId(projectId);
				}
				//set field numbers
				List<Field> fields = input.getFields();
				for (int i = 0; i < fields.size(); i++)
				{
					fields.get(i).setNumber(i + 1);
				}

				//add images
				database.getImageDB().addImages(input.getImages());
				//add fields
				database.getFieldsDB().addFields(input.getFields());
				return true;
			}
			else
			{
				{database.setError(true);return false;}
			}

		}
		catch (SQLException e)
		{

		}
		finally
		{

			if (stmt != null)
				try
				{
					stmt.close();
				}
				catch (SQLException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}

		{database.setError(true);return false;}
	}

	//this update will probably never be used since user submits one batch at a time 
	//and that operation updates projects, images and users
	/**
	 * Update projects in the database and all the relative information under
	 * them
	 * 
	 * @param projects
	 * @return true for successful operation, false otherwise
	 */
	public boolean updateProjects(List<Project> projects)
	{

		if (projects.size() == 0)
			{database.setError(true);return false;}

		PreparedStatement stmt = null;
		ResultSet result = null;
		try
		{
			//check if all project ids are valid id's (ids that are in the database)
			String sql_ = "Select COUNT(*) FROM project WHERE id=?";
			stmt = database.getConnection().prepareStatement(sql_);
			for (Project project : projects)
			{
				stmt.setInt(1, project.getId());
				result = stmt.executeQuery();
				result.next();
				if (result.getInt(1) == 0)
				{
					stmt.close();
					{database.setError(true);return false;}
				}
				result.close();
			}
			stmt.close();

			String sql = "UPDATE project SET assigned=?, completed=? WHERE id=?";

			stmt = database.getConnection().prepareStatement(sql);
			//prepare statement and check if projects have basic information such as images and fields
			for (Project project : projects)
			{
				//if project does not have fields or images return false
				if (project.getImages() == null || project.getFields() == null)
					{database.setError(true);return false;}
				if (project.getImages().size() == 0 || project.getFields().size() == 0)
					{database.setError(true);return false;}
				stmt.setInt(1, project.getAssigned());
				stmt.setInt(2, project.getCompleted());
				stmt.setInt(3, project.getId());
				stmt.addBatch();
			}

			int[] test = stmt.executeBatch();
			for (int n : test)
			{
				if (n == PreparedStatement.EXECUTE_FAILED)
				{
					{database.setError(true);return false;}
				}
			}

			for (Project project : projects)
			{
				//update images
				database.getImageDB().updateImages(project.getImages());
			}
			return true;

		}
		catch (SQLException e)
		{

		}
		finally
		{

			if (stmt != null)
				try
				{

					stmt.close();
				}
				catch (SQLException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			if (result != null)
			{
				try
				{
					result.close();
				}
				catch (SQLException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		{
			database.setError(true);
			return false;
		}
	}

}
