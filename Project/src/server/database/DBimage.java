package server.database;

import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import shared.communication.BatchGet;
import shared.communication.BatchSubmit;
import shared.communication.SampleGet;
import shared.communication.Validate;
import shared.communication.output.BatchGetOutput;
import shared.model.Field;
import shared.model.Image;
import shared.model.Record;
import shared.model.User;

/**
 * Perform image databse operations
 * 
 */
public class DBimage
{
	private Database database;

	public DBimage(Database databaseInput)
	{
		database = databaseInput;
	}
/**
 *  Retrieves image information by its id from database and create image object with it
 * @param imageId
 * @return
 */
	public Image getImageById(int imageId)
	{
		PreparedStatement stmt = null;
		Image image = null;
		ResultSet result = null;
		try
		{
			//get image from database
			String sql = "SELECT images.* FROM images LEFT JOIN project ON  images.parentId=project.id WHERE images.id=?";
	
			stmt = database.getConnection().prepareStatement(sql);
			stmt.setInt(1, imageId);
			result = stmt.executeQuery();
			result.next();
			image = new Image(result.getInt("parentId"), result.getString("file"), result.getInt("height"), result.getInt("width"), result.getInt("assigned"),
					result.getInt("completed"), new ArrayList<Record>());
			image.setId(result.getInt("id"));
		}
		catch (SQLException e)
		{
			try
			{
				stmt.close();
			}
			catch (SQLException e1)
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try
			{
				result.close();
			}
			catch (SQLException e1)
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		return image;
	}
/**
 *  Get first available image information from database based on a Project id and return a Image object with the information. The type parameters specifies if
 *  query should be able to get any image or only the ones that are not completed
 * @param parentId
 * @param type
 * @return
 */
	//type is 1 for sample image and is 0 for unassigned images
	public Image getImageByParentId(int parentId, int type)
	{
		PreparedStatement stmt = null;
		ResultSet result = null;
		Image image = null;
		try
		{
			//get first image with given parentId
			String sql = "";
			if (type == 0)
			{
				sql = "SELECT images.* FROM project LEFT JOIN images ON project.id=images.parentId "
						+ "WHERE project.id=? AND project.completed IN (0,NULL) AND images.assigned IN (0, NULL) " + "AND images.completed IN (NULL, 0)";
			}
			else if (type == 1)
			{
				sql = "SELECT images.* FROM project LEFT JOIN images ON project.id=images.parentId " + "WHERE project.id=?";
			}

			stmt = database.getConnection().prepareStatement(sql);
			stmt.setInt(1, parentId);
			result = stmt.executeQuery();

			if (result.next() == true)
			{
				image = new Image();
				image.setPath(new File(result.getString("file")));
				image.setId(result.getInt("id"));
				image.setParentId(result.getInt("parentId"));
				image.setHeight(result.getInt("height"));
				image.setWidth(result.getInt("width"));
				image.setAssigned(result.getInt("assigned"));
				image.setAssigned(result.getInt("completed"));
			}
		}
		catch (SQLException e)
		{
			try
			{
				stmt.close();
			}
			catch (SQLException e1)
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		return image;
	}
/**
 * Takes an image object and adds all the cells that belong to it into the object
 * @param image
 * @return
 */
	public boolean addCellValuesToImage(Image image)
	{
		if (image == null)
			{database.setError(true);return false;}
		PreparedStatement stmt = null;
		try
		{
			//get cell values for image
			String sql2 = "SELECT cell.* FROM cell LEFT JOIN images ON cell.parentId = images.id " + "WHERE images.id=? ORDER BY cell.row ASC, cell.column ASC";

			PreparedStatement stmt2 = database.getConnection().prepareStatement(sql2);
			stmt2.setInt(1, image.getId());
			ResultSet result2 = stmt2.executeQuery();

			Record record = new Record();
			int row = 1;
			while (result2.next())
			{
				//checks if current record is a new record (next row) and if so pushes the previous one and empties Record object
				if (result2.getInt("row") > row)
				{
					image.getRecordValues().add(record);
					record = new Record();
					row = result2.getInt("row");
				}
				record.getValues().add(result2.getString("value"));
				record.setId(result2.getInt("id"));
				record.setParentId(result2.getInt("parentId"));
				record.setRow(result2.getInt("row"));
			}
			stmt2.close();
			result2.close();
			//push the last record to the image
			image.getRecordValues().add(record);
			return true;
		}
		catch (SQLException e)
		{
			try
			{
				stmt.close();
			}
			catch (SQLException e1)
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		{database.setError(true);return false;}
	}
/**
 * Gets the number of records per image for the project to which the image belongs
 * @param imageId
 * @return
 */
	public int getRecordsPerImage(int imageId)
	{
		PreparedStatement stmt = null;
		ResultSet result = null;
		int recordsPerImage = 0;
		try
		{
			//get relevant project info from database
			String sql = "SELECT project.recordsPerImage FROM project LEFT JOIN images ON project.id=images.parentId WHERE images.id=?";
	
			stmt = database.getConnection().prepareStatement(sql);
			stmt.setInt(1, imageId);
			result = stmt.executeQuery();
			if (result.next() == true)
				recordsPerImage = result.getInt("recordsPerImage");
		}
		catch (SQLException e)
		{
			try
			{
				stmt.close();
			}
			catch (SQLException e1)
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try
			{
				result.close();
			}
			catch (SQLException e1)
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		return recordsPerImage;
	}
	/**
	 * Get images from database
	 * 
	 * @param input
	 * @return
	 */
	//TODO this has to be heavily changed
	public Image getSampeImage(SampleGet input)
	{
		Image image = null;
		if (database.getUserDB().validateUser(input.getUsername(), input.getPassword()) == true)
		{
			image = getImageByParentId(input.getProjectId(), 1);
			addCellValuesToImage(image);
		}

		return image;
	}
/**
 * Download a image
 * @param input
 * @return
 */
	public BatchGetOutput downloadBatch(BatchGet input)
	{
		PreparedStatement stmt = null;
		BatchGetOutput output = null;
		ResultSet result = null;
		if (database.getUserDB().validateUser(input.getUsername(), input.getPassword()) == true)
		{
			try
			{
				//get image information
				Image image = getImageByParentId(input.getProjectId(), 0);
				//check if user already has an image assignment
				User user = database.getUserDB().getUser(new Validate(input.getUsername(), input.getPassword()));

				String sql = "SELECT * FROM project WHERE id=?";
				if (image != null && user.getCurrentImage() == 0)
				{
					//get relevant project information such as recordHeight, number of Records per image, firstYCoord
					sql = "SELECT * FROM project WHERE id=?";

					stmt = database.getConnection().prepareStatement(sql);
					stmt.setInt(1, image.getParentId());

					result = stmt.executeQuery();
					result.next();
					int recordHeight = result.getInt("recordHeight");
					int recordsPerImage = result.getInt("recordsPerImage");
					int firstYCoord = result.getInt("firstYCoord");

					stmt.close();
					result.close();
					sql = "SELECT COUNT(*) FROM fields WHERE parentId=?";

					stmt = database.getConnection().prepareStatement(sql);
					stmt.setInt(1, image.getParentId());

					result = stmt.executeQuery();
					result.next();
					int numberOfFields = result.getInt(1);

					stmt.close();
					result.close();

					//get fields information

					List<Field> fields = database.getFieldsDB().getFields(image.getParentId());

					output = new BatchGetOutput(image.getId(), image.getParentId(), image.getPath().getPath(), firstYCoord, recordHeight,
							recordsPerImage, numberOfFields, fields);

					//set image as user's current image
					sql = "UPDATE users SET currentImage=? WHERE username=? AND password=?";

					stmt = database.getConnection().prepareStatement(sql);
					stmt.setInt(1, image.getId());
					stmt.setString(2, input.getUsername());
					stmt.setString(3, input.getPassword());
					if(stmt.executeUpdate()==0)
						return null;

					stmt.close();

					//assign image to user
					sql = "UPDATE images SET assigned=(SELECT id FROM users WHERE username=? AND password=?) WHERE id=?";

					stmt = database.getConnection().prepareStatement(sql);
					stmt.setString(1, input.getUsername());
					stmt.setString(2, input.getPassword());
					stmt.setInt(3, image.getId());

					if(stmt.executeUpdate()==0)
						return null;
					

					stmt.close();
				}

			}
			catch (SQLException e)
			{

			}
			finally
			{
				if (result != null)
					try
					{
						result.close();
					}
					catch (SQLException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				if (stmt != null)
					try
					{
						stmt.close();
					}
					catch (SQLException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
		}
		return output;
	}
/**
 * Get a list of images associated to given project id
 * @param parentId
 * @return
 */
	public List<Image> getImages(int parentId)
	{

		PreparedStatement stmt = null;
		List<Image> images = null;
		Image image = null;
		ResultSet result = null;

		try
		{
			//get images with given parentId
			String sql = "SELECT images.* FROM project LEFT JOIN images ON project.id=images.parentId "
					+ "WHERE project.id=? AND project.completed IN (0,NULL) AND images.assigned IN (NULL, 0) " + "AND images.completed IN (NULL, 0)";

			stmt = database.getConnection().prepareStatement(sql);
			stmt.setInt(1, parentId);
			result = stmt.executeQuery();

			images = new ArrayList<>();
			while (result.next())
			{
				image = new Image();
				image.setPath(new File(result.getString("file")));
				image.setId(result.getInt("id"));
				image.setParentId(result.getInt("parentId"));
				image.setHeight(result.getInt("height"));
				image.setWidth(result.getInt("width"));
				image.setAssigned(result.getInt("assigned"));
				image.setAssigned(result.getInt("completed"));

				addCellValuesToImage(image);
				images.add(image);
			}

		}
		catch (SQLException e)
		{

		}
		finally
		{
			if (result != null)
				try
				{
					result.close();
				}
				catch (SQLException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			if (stmt != null)
				try
				{
					stmt.close();
				}
				catch (SQLException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		return images;

	}

	/**
	 * Add a list of images to the database
	 * 
	 * @param input
	 * @return true for successful operation, false otherwise
	 */
	public boolean addImages(List<Image> input)
	{
		if (input.size() == 0)
			{database.setError(true);return false;}
		for (Image image : input)
		{
			if (addImage(image) == false)
			{
				//rollback so that you cannot add an image with no records or other errors
				database.setError(true);
				{database.setError(true);return false;}
			}
		}
		return true;
	}
//this assumes image already has the right number of values per record and the right number of records even if they are filled with blank spaces
	/**Add a single image to the database
	 * @param x
	 */
	private boolean addImage(Image input)
	{
		if (input == null)
			{database.setError(true);return false;}
		PreparedStatement stmt = null;
		Statement keyStmt = null;
		ResultSet keyRS = null;
		try
		{
			String sql = "INSERT INTO images (parentId, file, height, width, assigned, completed) " + "VALUES (?, ?, ?, ?, 0, ?)";

			stmt = database.getConnection().prepareStatement(sql);

			stmt.setInt(1, input.getParentId());
			stmt.setString(2, input.getPath().getPath());
			stmt.setInt(3, input.getHeight());
			stmt.setInt(4, input.getWidth());
			stmt.setInt(5, input.getCompleted());

			if (stmt.executeUpdate() == 1)
			{
				keyStmt = database.getConnection().createStatement();
				keyRS = keyStmt.executeQuery("select last_insert_rowid()");
				keyRS.next();
				int imageId = keyRS.getInt(1);

				keyRS.close();
				//add cells and associate them to image
				stmt.close();
				//if there are no records create records with empty strings
				if (input.getRecordValues() == null)
				{
					input.setRecordValues(new ArrayList<Record>());
				}
				if (input.getRecordValues().size() == 0)
				{
					//get the number of fields for project
					sql = "SELECT COUNT(*) FROM project LEFT JOIN fields on project.id=field.parentId" + "WHERE project.id=?";
					stmt = database.getConnection().prepareStatement(sql);
					stmt.setInt(1, input.getId());

					keyRS = stmt.executeQuery();
					keyRS.next();
					//if there is no associated project and fields in the database return false
					if (keyRS.getInt(1) == 0)
						{database.setError(true);return false;}
					int numberOffields = keyRS.getInt(1);

					keyRS.close();
					stmt.close();

					int recordsPerImage = getRecordsPerImage(input.getId());

					//create empty records
					for (int a = 0; a < recordsPerImage; a++)
					{
						Record record = new Record();
						List<String> strings = new ArrayList<>();
						for (int b = 0; b < numberOffields; b++)
						{
							strings.add("");
						}
						record.setValues(strings);
						input.getRecordValues().add(record);
					}

				}
				String sql_ = "INSERT INTO cell (parentId, row, column, value) VALUES (?, ?, ?, ?)";

				stmt = database.getConnection().prepareStatement(sql_);

				int row = 1;
				int column = 1;
				for (Record record : input.getRecordValues())
				{
					for (String value : record.getValues())
					{
						stmt.setInt(1, imageId);
						//set row
						stmt.setInt(2, row);
						//column as the index of b +1
						stmt.setInt(3, column);
						stmt.setString(4, value);
						stmt.addBatch();
						column++;
					}
					column = 1;
					row++;
				}
				int[] test = stmt.executeBatch();
				for (int n : test)
				{
					if (n == PreparedStatement.EXECUTE_FAILED)
					{
						{database.setError(true);return false;}
					}
				}
				return true;
			}
			else
			{
				{database.setError(true);return false;}
			}
		}
		catch (SQLException e)
		{

		}
		finally
		{
			if (stmt != null)
				try
				{
					stmt.close();
				}
				catch (SQLException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}

		{database.setError(true);return false;}

	}
/**
 * Submits image to the database
 * @param input
 * @return
 */
	public boolean submitBatch(BatchSubmit input)
	{

		PreparedStatement stmt = null;
		ResultSet result = null;
		Image image = null;
		if (database.getUserDB().validateUser(input.getUsername(), input.getPassword()) == false)
			{database.setError(true);return false;}
		try
		{
			//get image from database
			image = getImageById(input.getImageId());
			if (image == null)
				{database.setError(true);return false;}

			//get recordsPerImage
			int recordsPerImage = getRecordsPerImage(input.getImageId());
			if (recordsPerImage == 0)
				{database.setError(true);return false;}

			//add input values to image object
			String recordList = input.getRecordValues();
			String[] records = recordList.split(";");
			//for each record break it up into pieces and add them to Record object and add to image
			for (String record_ : records)
			{
				String[] values = record_.split("\\,", -1);
				Record record = new Record();

				for (String str : values)
				{
					if (str == "")
					{
						record.getValues().add("e");
					}
					else
					{
						record.getValues().add(str.trim());
					}
				}
				image.getRecordValues().add(record);
			}
			//check if number of values per record match the number of fields for project
			String sql = "SELECT COUNT(*) FROM project LEFT JOIN fields ON project.id=fields.parentID WHERE project.id=?";

			stmt = database.getConnection().prepareStatement(sql);
			stmt.setInt(1, image.getParentId());
			result = stmt.executeQuery();
			result.next();
			int valuesPerRecord = result.getInt(1);
			for (Record record : image.getRecordValues())
			{
				if (valuesPerRecord != record.getValues().size())
					{database.setError(true);return false;}
			}
			stmt.close();
			result.close();

			//check if user has image assigned to him
			User user= database.getUserDB().getUser(new Validate(input.getUsername(), input.getPassword()));
			int assigned = user.getCurrentImage();
			if (assigned != image.getId())
				{database.setError(true);return false;}
			stmt.close();
			result.close();
			//update image in database with new record values 
			if (updateImage(image) == false)
				{database.setError(true);return false;}

			//give user credit for submitting all the records in picture and clear his/her current image, does not update if user does not own the image
			sql = "UPDATE users SET indexedRecords=indexedRecords+?, currentImage=0 WHERE username=? AND password=?";

			stmt = database.getConnection().prepareStatement(sql);
			stmt.setInt(1, recordsPerImage);
			stmt.setString(2, input.getUsername());
			stmt.setString(3, input.getPassword());
			if (stmt.executeUpdate() == 0)
				{database.setError(true);return false;}

			stmt.close();
			//mark image as completed and unassigned
			sql = "UPDATE images SET completed=1, assigned=0 WHERE id=?";

			stmt = database.getConnection().prepareStatement(sql);
			stmt.setInt(1, input.getImageId());

			if (stmt.executeUpdate() == 0)
				{database.setError(true);return false;}

			stmt.close();

			//if after the update all images are completed mark project as completed
			sql = "UPDATE project SET completed=1 WHERE (SELECT COUNT(completed) FROM images WHERE completed=1 AND parentId=?)"
					+ "=(SELECT COUNT(completed) FROM images WHERE parentId=?) AND id=?";
			stmt = database.getConnection().prepareStatement(sql);
			stmt.setInt(1, image.getParentId());
			stmt.setInt(2, image.getParentId());
			stmt.setInt(3, image.getParentId());
			stmt.executeUpdate();

			return true;

		}
		catch (SQLException e)
		{

		}
		finally
		{
			if (stmt != null)
				try
				{
					stmt.close();
				}
				catch (SQLException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			try
			{
				result.close();
			}
			catch (SQLException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		{database.setError(true);return false;}
	}

	/**
	 * Update a list of images in the database
	 * 
	 * @param input
	 * @return true for successful operation, false otherwise
	 */
	public boolean updateImages(List<Image> input)
	{
		for (Image image : input)
		{
			if (updateImage(image) == false)
			{
				{database.setError(true);return false;}
			}
		}
		return true;
	}
/**
 * Update a single image in the database
 * @param input
 * @return
 */
	public boolean updateImage(Image input)
	{

		PreparedStatement stmt = null;

		try
		{
			String sql = "UPDATE images SET  assigned=?, completed=? WHERE id=?";

			stmt = database.getConnection().prepareStatement(sql);

			stmt.setInt(1, input.getAssigned());
			stmt.setInt(2, input.getCompleted());
			stmt.setInt(3, input.getId());

			if (stmt.executeUpdate() == 1)
			{

				//update cells
				stmt.close();
				String sql_ = "UPDATE cell SET value=? WHERE row=? AND column=? AND parentId=?";

				stmt = database.getConnection().prepareStatement(sql_);

				int row = 1;
				int column = 1;
				for (Record record : input.getRecordValues())
				{
					for (String value : record.getValues())
					{
						stmt.setString(1, value);
						stmt.setInt(2, row);
						//column as the index of value +1
						stmt.setInt(3, column);
						stmt.setInt(4, input.getId());
						stmt.addBatch();
						column++;
					}
					column = 1;
					row++;
				}
				int[] test = stmt.executeBatch();
				for (int n : test)
				{
					if (n == PreparedStatement.EXECUTE_FAILED)
					{
						{database.setError(true);return false;}
					}
				}
				return true;
			}
			else
			{
				{database.setError(true);return false;}
			}
		}
		catch (SQLException e)
		{

		}
		finally
		{
			if (stmt != null)
				try
				{
					stmt.close();
				}
				catch (SQLException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}

		{
			database.setError(true);
			return false;
		}
	}

}
