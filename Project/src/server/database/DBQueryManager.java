/**
 * 
 */
package server.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Automatically creates prepared statements for query or update based on a string with the character @ marking all the spots where the data will be entered. "@" is 
 * substituted by the number of parameters for each field in case of IN(x,x...) SQL statements and the statement is built based on the parameters passed in the 
 * second argument of the methods
 * 
 */
public class DBQueryManager
{

	public DBQueryManager()
	{

	}
/**
 * 
 * @param queryInput query string
 * @param parameters values to replace "@" in the query string
 * @return the result from the query
 */
	//list containing list of strings with values separated by commas
	public static ResultSet performQuery(String queryInput, List<String> parameters)
	{
		Database database;
		try
		{
			database = new Database();
		}
		catch (DatabaseException e1)
		{
			e1.printStackTrace();
			return null;
		}
		PreparedStatement stmt = null;
		ResultSet result = null;
		try
		{
			//split query around wild card '@'
			String[] splitQuery = queryInput.split("@");
			new ArrayList<>();

			List<String> listsOfQuestionMarks = new ArrayList<>();
			for (String string : parameters)
			{
				//break string into array
				String[] temp = string.split("\\,", -1);
				//for each value in array add a question mark to questions string builder
				StringBuilder questions = new StringBuilder();
				for (int i = 0; i < temp.length; i++)
				{
					questions.append("?,");
				}
				//remove last comma
				questions.deleteCharAt(questions.length() - 1);
				//add resulted string to listofQuestionMarks
				listsOfQuestionMarks.add(questions.toString());
			}

			//build final query string
			StringBuilder query = new StringBuilder();
			//concatenate corresponding question after each piece of splitQuery
			//there should be 1 less item in list of questionMarks than in the split string, string "aa@aaa@aa" woudl be split into "aa", "aaa", "aa", 
			//and question marks should be just "?*","?*"
			assert listsOfQuestionMarks.size() == splitQuery.length - 1;

			int counter = 0;
			for (int i = 0; i < listsOfQuestionMarks.size(); i++, counter++)
			{
				query.append(splitQuery[i]);
				query.append(listsOfQuestionMarks.get(i));
			}
			//append last part of splitQuery
			query.append(splitQuery[counter]);

			stmt = database.getConnection().prepareStatement(query.toString());

			//set statement values
			counter = 0;
			for (String string : parameters)
			{
				String[] values = string.split("\\,", -1);
				for (int i = 0; i < values.length; i++, counter++)
				{
					try
					{
						stmt.setInt(counter, Integer.parseInt(values[i].trim()));
					}
					catch (NumberFormatException e)
					{
						stmt.setString(counter, values[i].trim());
					}
				}
			}
			result = stmt.executeQuery();
		}
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally
		{
			if (result != null)
				try
				{
					result.close();
				}
				catch (SQLException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			if (stmt != null)
				try
				{
					stmt.close();
				}
				catch (SQLException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}

		return result;
	}
	/**
	 * 
	 * @param queryInput query string
	 * @param parameters values to replace "@" in the query string
	 * @return true for successful operation and false otherwise
	 */
	//list containing list of strings with values separated by commas
	public static boolean performInsertUpdate(String queryInput, List<String> parameters)
	{
		Database database;
		try
		{
			database = new Database();
		}
		catch (DatabaseException e1)
		{
			e1.printStackTrace();
			return false;
		}
		PreparedStatement stmt = null;
		ResultSet result = null;
		try
		{
			//split query around wild card '@'
			String[] splitQuery = queryInput.split("@");
			new ArrayList<>();

			List<String> listsOfQuestionMarks = new ArrayList<>();
			for (String string : parameters)
			{
				//break string into array
				String[] temp = string.split("\\,", -1);
				//for each value in array add a question mark to questions string builder
				StringBuilder questions = new StringBuilder();
				for (int i = 0; i < temp.length; i++)
				{
					questions.append("?,");
				}
				//remove last comma
				questions.deleteCharAt(questions.length() - 1);
				//add resulted string to listofQuestionMarks
				listsOfQuestionMarks.add(questions.toString());
			}

			//build final query string
			StringBuilder query = new StringBuilder();
			//concatenate corresponding question after each piece of splitQuery
			//there should be 1 less item in list of questionMarks than in the split string, string "aa@aaa@aa" woudl be split into "aa", "aaa", "aa", 
			//and question marks should be just "?*","?*"
			assert listsOfQuestionMarks.size() == splitQuery.length - 1;

			int counter = 0;
			for (int i = 0; i < listsOfQuestionMarks.size(); i++, counter++)
			{
				query.append(splitQuery[i]);
				query.append(listsOfQuestionMarks.get(i));
			}
			//append last part of splitQuery
			query.append(splitQuery[counter]);

			stmt = database.getConnection().prepareStatement(query.toString());

			//set statement values
			counter = 0;
			for (String string : parameters)
			{
				String[] values = string.split("\\,", -1);
				for (int i = 0; i < values.length; i++, counter++)
				{
					try
					{
						stmt.setInt(counter, Integer.parseInt(values[i].trim()));
					}
					catch (NumberFormatException e)
					{
						stmt.setString(counter, values[i].trim());
					}
				}
			}
			if (stmt.executeUpdate() == 1)
				return true;

		}
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally
		{
			if (result != null)
				try
				{
					result.close();
				}
				catch (SQLException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			if (stmt != null)
				try
				{
					stmt.close();
				}
				catch (SQLException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}

		return false;
	}
}
