package listem;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class FileSearcher
{
	protected String filePattern;
	protected boolean recursive;
	protected List<File> files;
	protected StringBuilder output;

	protected void search(File input, String fPattern, boolean r) throws FileNotFoundException
	{
		filePattern = fPattern;
		if (input.getPath().equals("."))
			input = new File(System.getProperty("user.dir"));

		recursive = r;
		output = new StringBuilder();
		files = new ArrayList<File>();
		search(input);
	}

	private void search(File currentFile) throws FileNotFoundException
	{
		Pattern p = Pattern.compile(filePattern);
		String[] filesFolders = currentFile.list();
		for (String i : filesFolders)
		{
			//System.out.println(i);
			Matcher m = p.matcher(i);
			File test = new File(currentFile.getAbsolutePath() + "\\" + i);
			if (m.matches())
			{
				

				if (test.isFile())
				{
					files.add(new File(currentFile.getAbsolutePath() + "\\" + i));
				}
				
			}
			else if (test.isDirectory() && recursive)
			{
				search(new File(currentFile.getAbsolutePath() + "\\" + i));
			}
		}

	}

	protected abstract void processFile();

}
