package listem;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class MyLineCount extends FileSearcher implements LineCounter
{
	private Map<File, Integer> result;

	public MyLineCount()
	{

	}

	@Override
	public Map<File, Integer> countLines(File directory, String fileSelectionPattern, boolean recursive)
	{
		try
		{
			result = new HashMap<File, Integer>();
			super.search(directory, fileSelectionPattern, recursive);
			processFile();
		}
		catch (FileNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;
	}

	@Override
	protected void processFile()
	{
		for (File i : files)
		{
			try
			{
				Scanner text = new Scanner(i);
				int counter = 0;

				while (text.hasNextLine())
				{
					text.nextLine();
					counter++;
				}

				//output.append(counter + " " + i.getAbsolutePath() + "\n");
				result.put(i, counter);

				text.close();

			}
			catch (FileNotFoundException e)
			{
				e.printStackTrace();
			}

		}

	}

}
