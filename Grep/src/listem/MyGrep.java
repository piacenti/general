package listem;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MyGrep extends FileSearcher implements Grep
{
	private String searchPattern;
	private Map<File, List<String>> result;

	public MyGrep()
	{

	}

	@Override
	public Map<File, List<String>> grep(File directory, String fileSelectionPattern, String substringSelectionPattern, boolean recursive)
	{
		searchPattern = substringSelectionPattern;
		try
		{
			result = new HashMap<File, List<String>>();
			super.search(directory, fileSelectionPattern, recursive);
			processFile();
			//test();
		}
		catch (FileNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;
	}

	@Override
	protected void processFile()
	{
		for (File i : files)
		{
			//System.out.println(i.getAbsolutePath());

			try
			{
				List<String> current = new ArrayList<String>();
				Scanner text = new Scanner(i);
				String line = "";
				while (text.hasNextLine())
				{
					line = text.nextLine();
					Pattern p = Pattern.compile(searchPattern);
					Matcher m = p.matcher(line);

					if (m.find())
					{
						current.add(line);
					}
				}
				//System.out.println();
				if (current.size() > 0)
					result.put(i, current);
				text.close();
			}
			catch (FileNotFoundException e)
			{
				e.printStackTrace();
			}

		}

	}
	private void test()
	{
		int totalMatches = 0;
		for (Map.Entry<File, List<String>> singleFileResult : result.entrySet())
		{
			System.out.println("FILE: " + singleFileResult.getKey().getPath());

			List<String> lineMatches = singleFileResult.getValue();
			for (String lineMatch : lineMatches)
			{
				System.out.println(lineMatch);
			}

			int matches = lineMatches.size();
			System.out.println("MATCHES: " + matches);
			totalMatches += matches;

			System.out.println();
		}

		System.out.println("TOTAL MATCHES: " + totalMatches);
	}

}
