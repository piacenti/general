package imageEditor;

import java.lang.String;

public class Image
{
	private pixel[][] pixels;
	private String header;
	private int width;
	private int height;
	private int maxColorValue;
	private String ppmImage;
	private int index;
	public Image(String ppmImageInput)
	{
		ppmImage = ppmImageInput;
		header="";
		maxColorValue=255;
		
		int counter =0;
		boolean comment=false;
		boolean pixelText=false;
		String value="";
		while(index<ppmImage.length())
		{
			//System.out.println(comment);
			if(counter<4)
			{
				header+=ppmImage.charAt(index);
			}
			
			if(ppmImage.charAt(index)=='#')
			{
				comment=true;
			}
			else if(comment==true && ppmImage.charAt(index)=='\n')
			{
				comment = false;
				//goes through all white spaces after the line break
				while(Character.isWhitespace(ppmImage.charAt(index))==true)
				{
					index++;
				}
				header+=ppmImage.charAt(index);
			}
			
			if(Character.isWhitespace(ppmImage.charAt(index))==true && pixelText==false && comment==false && counter<4)
			{
				if(counter==1 && value!="")
				{
					width=Integer.parseInt(value);
				}
				else if(counter==2 && value!="")
				{
					height=Integer.parseInt(value);
				}
				counter++;
				value="";
			}
			
			if((counter==1 || counter==2) && comment==false && Character.isWhitespace(ppmImage.charAt(index))==false)
			{
				value+=ppmImage.charAt(index);
			}
			if(counter==4)
			{
				break;
			}
			
			index++;
		}
		createPixels();
		
		
	}
	
	private void createPixels()
	{
		pixels = new pixel[height][width];
		int row=0;
		int column=0;
		String red="";
		String green="";
		String blue="";
		while(index<ppmImage.length())
		{
			if(Character.isWhitespace(ppmImage.charAt(index))==false)
			{
				while(Character.isWhitespace(ppmImage.charAt(index))==false)
				{
					red+=ppmImage.charAt(index);
					index++;
				}
				
				index++;
				while(Character.isWhitespace(ppmImage.charAt(index))==false)
				{
					green+=ppmImage.charAt(index);
					index++;
				}
				
				index++;
				while(Character.isWhitespace(ppmImage.charAt(index))==false)
				{
					blue+=ppmImage.charAt(index);
					index++;
				}
				//System.out.println(red+" "+green+" "+blue +" column="+column);
				
				pixels[row][column]=new pixel(Integer.parseInt(red),Integer.parseInt(green),Integer.parseInt(blue));;
				
				if(column<width-1)
				{
					column++;
				}
				else
				{
					column=0;
					if(row<height-1)
					{
						row++;
					}
				}
				if(row==height-1 && column==width)
				{
					break;
				}
				
				red="";
				green="";
				blue="";
			}
			index++;
		}
		
	}

	public pixel[][] getPixels()
	{
		return pixels;
	}

	public void setPixels(pixel[][] pixels)
	{
		this.pixels = pixels;
	}

	public String getHeader()
	{
		return header;
	}

	public void setHeader(String header)
	{
		this.header = header;
	}

	public int getWidth()
	{
		return width;
	}

	public void setWidth(int width)
	{
		this.width = width;
	}

	public int getHeight()
	{
		return height;
	}

	public void setHeight(int height)
	{
		this.height = height;
	}
	
}
