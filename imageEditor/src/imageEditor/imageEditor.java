package imageEditor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class imageEditor
{
	static Image image;
	static int motionblur;
	static String changedImage;

	public static void main(String[] args) throws IOException
	{
		loadFile(args[0]);
		if (args[2].equals("motionblur") && args.length > 3)
		{
			System.out.println(args[3]);
			motionblur = Integer.parseInt(args[3]);
		} else if (args[2].equals("motionblur"))
		{
			Error error = new Error(
					"USAGE: java ImageEditor in-file out-file (grayscale|invert|emboss|motionblur motion-blur-length)");
			throw error;
		}
		System.out.println(args[2]);
		action(args[2]);
		ImageToString();
		saveFile(args[1]);

	}

	public static void loadFile(String filePath) throws IOException
	{
		BufferedReader text = null;
		try
		{

			text = new BufferedReader(new FileReader(filePath));

		} catch (IOException e)
		{
			e.printStackTrace();
		}

		String fullText = "";
		StringBuilder str = new StringBuilder();
		String line = "";
		while ((line = text.readLine()) != null)
		{
			str.append(line + "\n");
		}
		fullText = str.toString();

		// System.out.println();
		image = new Image(fullText);

	}

	private static void saveFile(String filePath) throws FileNotFoundException
	{

		PrintWriter text = new PrintWriter(filePath);
		text.write(changedImage);
		text.close();

	}

	private static void action(String action)
	{
		if (action.equals("invert"))
		{
			invert();
		} else if (action.equals("grayscale"))
		{
			grayscale();
		} else if (action.equals("emboss"))
		{
			emboss();
		} else if (action.equals("motionblur"))
		{
			motionblur();
		}

	}

	private static void motionblur()
	{
		int maxRows = image.getHeight();
		int maxColumns = image.getWidth();
		pixel[][] pixels = image.getPixels();
		pixel[][] blurredPixels = new pixel[maxRows][maxColumns];
		for (int row = 0; row < maxRows; row++)
		{
			for (int column = 0; column < maxColumns; column++)
			{
				int red = 0;
				int green = 0;
				int blue = 0;
				if (column + motionblur < maxColumns)
				{
					for (int i = column; i < (column + motionblur); i++)
					{
						red += pixels[row][i].getRed();
						green += pixels[row][i].getGreen();
						blue += pixels[row][i].getBlue();
					}
					red /= motionblur;
					green /= motionblur;
					blue /= motionblur;
				} else if (column + motionblur >= maxColumns)
				{
					int tillEnd = maxColumns - column;
					for (int i = column; i < (column + tillEnd); i++)
					{
						red += pixels[row][i].getRed();
						green += pixels[row][i].getGreen();
						blue += pixels[row][i].getBlue();
					}
					red /= tillEnd;
					green /= tillEnd;
					blue /= tillEnd;
				}

				

				System.out.println(pixels[row][column].getRed() + " " + red);
				blurredPixels[row][column] = new pixel(red, green, blue);
			}
		}

		image.setPixels(blurredPixels);
	}

	private static void emboss()
	{
		int maxRows = image.getHeight();
		int maxColumns = image.getWidth();
		pixel[][] pixels = image.getPixels();
		for (int row = maxRows - 1; row > -1; row--)
		{
			for (int column = maxColumns - 1; column > -1; column--)
			{

				int redDiff = 0;
				int greenDiff = 0;
				int blueDiff = 0;
				if (row > 0 && column > 0)
				{
					redDiff = pixels[row][column].getRed()
							- pixels[row - 1][column - 1].getRed();
					greenDiff = pixels[row][column].getGreen()
							- pixels[row - 1][column - 1].getGreen();
					blueDiff = pixels[row][column].getBlue()
							- pixels[row - 1][column - 1].getBlue();
				}

				int maxDifference = redDiff;
				if (Math.abs(maxDifference) < Math.abs(greenDiff))
					maxDifference = greenDiff;
				if (Math.abs(maxDifference) < Math.abs(blueDiff))
					maxDifference = blueDiff;

				int value = 128 + maxDifference;
				if (value < 0)
				{
					value = 0;
				} else if (value > 255)
				{
					value = 255;
				}
				pixels[row][column].setRed(value);
				pixels[row][column].setGreen(value);
				pixels[row][column].setBlue(value);
			}
		}

	}

	private static void grayscale()
	{
		int maxRows = image.getHeight();
		int maxColumns = image.getWidth();
		pixel[][] pixels = image.getPixels();
		for (int row = 0; row < maxRows; row++)
		{
			for (int column = 0; column < maxColumns; column++)
			{
				int average = (pixels[row][column].getRed()
						+ pixels[row][column].getGreen() + pixels[row][column]
						.getBlue()) / 3;
				pixels[row][column].setRed(average);
				pixels[row][column].setGreen(average);
				pixels[row][column].setBlue(average);
			}
		}

	}

	private static void invert()
	{
		int maxRows = image.getHeight();
		int maxColumns = image.getWidth();
		pixel[][] pixels = image.getPixels();
		for (int row = 0; row < maxRows; row++)
		{
			for (int column = 0; column < maxColumns; column++)
			{
				pixels[row][column].setRed(Math.abs(pixels[row][column]
						.getRed() - 255));
				pixels[row][column].setGreen(Math.abs(pixels[row][column]
						.getGreen() - 255));
				pixels[row][column].setBlue(Math.abs(pixels[row][column]
						.getBlue() - 255));
			}
		}

	}

	private static void ImageToString()
	{
		StringBuilder str = new StringBuilder();
		str.append(image.getHeader());
		int maxRows = image.getHeight();
		int maxColumns = image.getWidth();
		pixel[][] pixels = image.getPixels();
		for (int row = 0; row < maxRows; row++)
		{
			for (int column = 0; column < maxColumns; column++)
			{
				// System.out.println(column);
				str.append(Integer.toString(pixels[row][column].getRed()) + " "
						+ Integer.toString(pixels[row][column].getGreen())
						+ " " + Integer.toString(pixels[row][column].getBlue())
						+ " ");

			}
		}
		changedImage = str.toString();

	}
}
