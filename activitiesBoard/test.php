<?php
 require('../includes/includeme.php');
	?>
<script src="https://<?php echo $_SERVER['SERVER_NAME']; ?>/includes/template/js/libs/jquery-1.8.2.min.js"></script>
<script src="https://<?php echo $_SERVER['SERVER_NAME']; ?>/includes/template/js/libs/jquery-ui-1.9.0.custom.min.js"></script>
<script src="https://<?php echo $_SERVER['SERVER_NAME']; ?>/includes/template/js/libs/jquery-ui-1.9.0.custom.min.js"></script>
<link rel="stylesheet" href="https://<?php echo $_SERVER['SERVER_NAME']; ?>/includes/template/css/jquery-ui-1.9.0.custom.css" />
<style>
td.highlight a
{
background: #A8C7E3 50% 50% repeat-x !important;  
border: 1px #A8C7E3 solid !important;
color:#070B73 !important;
}
</style>

<script>
function calendar(selection, curDate)
{
	var dayInMilliseconds=86400000;
	var days=Math.floor(curDate/dayInMilliseconds);
	var range=((selection=="3days")?2:6);
	var defaultTime=new Date();
	defaultTime.setTime(curDate+dayInMilliseconds);
	$('#selectedDate').datepicker('destroy');
	$('#selectedDate').datepicker({defaultDate:defaultTime, dateFormat:"yy-mm-dd", onSelect:function(date){calendar(selection, Date.parse(date));}, beforeShowDay:function(date)
	{
		if(Math.floor(date.valueOf()/dayInMilliseconds) >= days && Math.floor(date.valueOf()/dayInMilliseconds) <= days+range)
		{
			console.log(date);
			return[true, "highlight", ""];
		}
		return[true, "ui-datepicker-days-cell", ""];
	}
	});

}
</script>

<div id="selectedDate"></div>
<input type="button" id="3days" Value="3 days" onclick="calendar('3days', '<?php echo strtotime(date('Y-m-d'))*1000; ?>')">
<input type="button" id="week" Value="week" onclick="calendar('week', '<?php echo strtotime(date('Y-m-d'))*1000; ?>')">


<?php
 require('../includes/includeAtEnd.php');
?>
