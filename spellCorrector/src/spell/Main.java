package spell;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

import spell.SpellCorrector.NoSimilarWordFoundException;

/**
 * A simple main class for running the spelling corrector
 */
public class Main
{

	/**
	 * Give the dictionary file name as the first argument and the word to
	 * correct as the second argument.
	 */
	public static void main(String[] args) throws NoSimilarWordFoundException,
			IOException
	{

		String dictionaryFileName = args[0];
		String inputWord = args[1];

		Corrector corrector = new Corrector();
		corrector.useDictionary(dictionaryFileName);
		ArrayList<String> suggestion = corrector.suggestSimilarWord(inputWord);
		//corrector.runCorrector();

		System.out.println("Suggestion is: " + suggestion);
		
		//test
		Corrector test1=new Corrector();
		Corrector test2=new Corrector();
		test1.useDictionary(dictionaryFileName);
		test2.useDictionary(dictionaryFileName);
		

		
		//System.out.println(test1.getDictionary().toString() +test1.getDictionary().toString());
		//System.out.println(test1.getDictionary().toString().equals(test2.getDictionary().toString()));
	}

}
